package com.plusot.bluwatch

import android.content.Context
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import com.plusot.bluelib.log.LLog
import android.hardware.SensorManager
import androidx.wear.ambient.AmbientModeSupport
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.LibOption
import com.plusot.bluelib.sensor.*
import com.plusot.bluelib.sensor.bluetooth.ble.BikePowerBlePeripheral
import com.plusot.bluelib.sensor.bluetooth.ble.BlePeripheralServer
import com.plusot.bluelib.sensor.bluetooth.ble.HeartRateBlePeripheral
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.sensor.data.DeviceDataType
import com.plusot.bluelib.util.*
import com.plusot.bluelib.util.TimeUtil.MINUTE
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.concurrent.timer
import kotlin.coroutines.CoroutineContext
import kotlin.math.max

//TODO: Peripheral for accelerations

class BluWatchMain : WearableActivity(), CoroutineScope {
    private lateinit var job: Job
    private lateinit var sensorManager: SensorManager

    private val deviceDataTypes = mutableListOf<DeviceDataType>()
    private val newDeviceDataTypes = mutableListOf<DeviceDataType>()
    private val currentData = Data()
    private val currentAccuracy = mutableMapOf<String, SensorAccuracy>()
    private var currentDeviceDataType: DeviceDataType? = null
    private var switchTime = 0L
    private var periServer: BlePeripheralServer? = null
    private var ambient: Boolean = false

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val handler = CoroutineExceptionHandler { _, throwable ->
        LLog.e("Exception: $throwable")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        job = Job()
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // Enables Always-on
        setAmbientEnabled()

        setAutoResumeEnabled(true)

        launch(handler) {
            delay(1000)
            LLog.d("Inet Address: " + this@BluWatchMain.getIpAddress())
            BlueLib.start(
                this@BluWatchMain,
                LibOption.USE_INTERNAL_SENSORS,
                LibOption.USE_BODY_SENSORS,
                LibOption.USE_BLE_SENSORS
            )
            BlueLib.listeners.add(object: BlueLib.Listener {
                override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {
                    LLog.d("$device - ${device.manufacturer} scanned", checkHistory)
                    device.isSelected = when (device.type) {
                        DeviceType.BLUETOOTH_LE,
                        DeviceType.BLUETOOTH_DUAL -> false //device.name.equals("Mio-FUSE", true)
                        DeviceType.INTERNAL_LINEAR_ACCELERATION,
                        DeviceType.INTERNAL_ACCELEROMETER,
                        DeviceType.INTERNAL_GPS -> false
                        DeviceType.INTERNAL_CLOCK,
                        DeviceType.INTERNAL_STEP_COUNTER,
                        DeviceType.INTERNAL_TEMPERATURE,
                        DeviceType.INTERNAL_AMBIENT_TEMPERATURE,
                        DeviceType.INTERNAL_HEART_RATE_MONITOR -> true
                        else -> false
                    }
                }

                override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo, data: Data?) {
                    when (state) {
                        Device.StateInfo.CONNECTED -> {
                            val accuracy = data?.getInt(device.address, DataType.SENSOR_ACCURACY)
                                ?.toEnum<SensorAccuracy>()
                            if (accuracy != null) {
                                LLog.d("$device state= $state, accuracy= $accuracy", checkHistory)
                                currentAccuracy[device.address] = accuracy
                            } else
                                LLog.d("$device state= $state")
                        }
                        else -> LLog.d("$device state= $state")
                    }
                }

                override fun onScanning(scanType: ScanType, on: Boolean) {
                    LLog.d("Scan type $scanType = $on", checkHistory)
                }

                override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
                    //LLog.d("$device data $data")
                    currentData += data

                    data.dataTypes
                        .filter {
                            when (it) {
                                DataType.TIME_CLOCK,
                                DataType.HEART_RATE,
//                                DataType.ACCELERATION,
                                DataType.TEMPERATURE,
//                                DataType.STEPS,
                                DataType.STEPS_TOTAL -> true
                                else -> false
                            }
                        }
                        .forEach {
                            val ddt = DeviceDataType(device, it)
                            deviceDataTypes.addIfNew(ddt)
                            if (ddt == currentDeviceDataType) showValue(ddt)
                            if (it == DataType.HEART_RATE) periServer?.setValue(data)
                        }

                    if (isNew) data.dataTypes.forEach {
                        newDeviceDataTypes.addIfNew(DeviceDataType(device, it)) { type -> LLog.d("New device - datatype: $type") }
                    }
                }
            })

            timer(name ="ScreenWriter",
                daemon = true,
                period = 1000) {
                val now = System.currentTimeMillis()
                if (currentDeviceDataType == null)
                    currentDeviceDataType = deviceDataTypes.firstOrNull()
                else
                    currentDeviceDataType?.let {
                        if (now - switchTime > 5000L) {
                            currentDeviceDataType = deviceDataTypes.nextElement(it)
                            switchTime = now
                        }
                    }
                currentDeviceDataType?.let { showValue(it) }
            }
            timer(name = "Beater",
                daemon = true,
                period = 1000) {

                val hr = if (currentData.hasDataType(DataType.HEART_RATE))
                    currentData.getInt(null, DataType.HEART_RATE, 0)
                else
                    (25 + Math.random() * 25).toInt()

                val power = max(maxPower * (hr - hrRest) / (hrMax - hrRest), 0.0)

                //LLog.d("Sending $hr bpm and $power W")
                periServer?.setValue(Data()
                    .add(DataType.HEART_RATE, hr)
                    .add(DataType.POWER, power)
                )
            }
            periServer = BlePeripheralServer(listOf(HeartRateBlePeripheral(), BikePowerBlePeripheral()))
        }
    }

    private fun showValue(ddt: DeviceDataType) {
//        if (!ambient)
        runInMain {
            val address = ddt.device.address
            currentData.toValuePair(address, ddt.dataType)?.let { (value, unit) ->
                valueView.setTextColor(currentAccuracy[address]?.color ?: resources.getColored(R.color.unknownColor))
                sensorNameView.text = ddt.dataType.toProperString()
                valueView.text = value
                unitView.text = unit
            }
        }
    }

    override fun onEnterAmbient(ambientDetails: Bundle?) {
        ambient = true
        LLog.d("Enter ambient")
        super.onEnterAmbient(ambientDetails)
    }

    override fun onExitAmbient() {
        ambient = false
        LLog.d("Exit ambient")
        super.onExitAmbient()
    }

    override fun onUpdateAmbient() {
        LLog.d("Exit ambient")
        super.onUpdateAmbient()
    }

    override fun onResume() {
        BlueLib.resume()
        super.onResume()
    }

//    override fun onPause() {
//        //BlueLib.pause()
//        super.onPause()
//    }

    override fun onDestroy() {
        BlueLib.close()
        periServer?.close()
        job.cancel()
        super.onDestroy()
    }

    companion object {
        const val checkHistory = 10 * MINUTE
        const val hrMax = 200.0
        const val hrRest = 60
        const val maxPower = 500
    }
}
