package com.plusot.bluwatch

import android.app.Application
import com.plusot.bluelib.Globals

/**
 * Package: com.plusot.hellowatch
 * Project: HelloWatch
 *
 * Created by Peter Bruinink on 2019-05-03.
 * Copyright © 2019 Plusot. All rights reserved.
 */

class BluWatchApp: Application() {

    override fun onCreate() {
        super.onCreate()
        Globals.init(this, javaClass.simpleName)
    }

}