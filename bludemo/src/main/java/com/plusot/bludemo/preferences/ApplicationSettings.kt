package com.plusot.bludemo.preferences

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.plusot.bludemo.R

class ApplicationSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.application_preferences, rootKey)
    }
}

