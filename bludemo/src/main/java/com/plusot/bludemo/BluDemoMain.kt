package com.plusot.bludemo

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.plusot.bludemo.preferences.ApplicationSettingsActivity
import com.plusot.bludemo.preferences.PreferenceKey
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.Globals
import com.plusot.bluelib.LibOption
import com.plusot.bluelib.dialog.Alerts
import com.plusot.bluelib.dialog.ClickResult
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceHelper
import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.ScanType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.SensorAccuracy
import com.plusot.bluelib.sensor.bluetooth.ble.BlePeripheralServer
import com.plusot.bluelib.sensor.bluetooth.ble.HeartRateBlePeripheral
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.sensor.data.DeviceDataType
import com.plusot.bluelib.util.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.timer
import kotlin.math.max

class BluDemoMain : Activity() {
    private val types = mutableListOf<DeviceDataType>()
    private val validTypes = mutableListOf<DeviceDataType>()
    private val newDeviceDataTypes = mutableListOf<DeviceDataType>()
    private val currentData = Data()
    private val currentAccuracy = mutableMapOf<String, SensorAccuracy>()
    private var currentDeviceDataType: DeviceDataType? = null
    private var switchTime = 0L
    private var periServer: BlePeripheralServer? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars() or
                    WindowInsets.Type.navigationBars() or
                    WindowInsets.Type.systemBars() or
                    WindowInsets.Type.captionBar() or
                    WindowInsets.Type.ime() or
                    WindowInsets.Type.systemGestures() or
                    WindowInsets.Type.mandatorySystemGestures() or
                    WindowInsets.Type.tappableElement() or
                    WindowInsets.Type.displayCutout()
            )
        } else {
            @Suppress("DEPRECATION")
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            this.setShowWhenLocked(true)
        }

        setContentView(R.layout.activity_main)
        Globals.verboseDevices = false

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        runInMain(100) {
            init()
            hideUI()
        }
    }

    private fun hideUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars() or
                    WindowInsets.Type.navigationBars() or
                    WindowInsets.Type.systemBars() or
                    WindowInsets.Type.captionBar() or
                    WindowInsets.Type.ime() or
                    WindowInsets.Type.systemGestures() or
                    WindowInsets.Type.mandatorySystemGestures() or
                    WindowInsets.Type.tappableElement() or
                    WindowInsets.Type.displayCutout()
            )
        } else {
            @Suppress("DEPRECATION")
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                    // Set the content to appear under the system bars so that the
                    // content doesn't resize when the system bars hide and show.
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    // Hide the nav bar and status bar
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)
        }
    }

    val listener = object: BlueLib.Listener {
        override fun onDeviceScanned(device: ScannedDevice, isNew: Boolean) {
            LLog.d("$device - ${device.manufacturer} - ${device.address} scanned", checkHistory)
            device.isSelected = PreferenceHelper["Selected_${device.name}", false]
            runInMain { scanned.text = getString(R.string.scanned, device.name) }
        }

        override fun onDeviceState(device: ScannedDevice, state: Device.StateInfo, data: Data?) {
            when (state) {
                Device.StateInfo.CONNECTED -> {
                    val accuracy = data?.getInt(device.address, DataType.SENSOR_ACCURACY)
                        ?.toEnum<SensorAccuracy>()
                    if (accuracy != null) {
                        LLog.d("$device ${device.address} state= $state, accuracy= $accuracy", checkHistory)
                        currentAccuracy[device.address] = accuracy
                    } else
                        LLog.d("$device ${device.address} state= $state")
                }
                else -> LLog.d("$device ${device.address} state= $state")
            }
        }

        override fun onScanning(scanType: ScanType, on: Boolean) {
            LLog.d("Scan type $scanType = $on", checkHistory)
            runInMain {scanning.text = getString(R.string.scanning, scanType.toString())}
        }

        override fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean) {
//            LLog.d("$device data ${device.address}: $data")
            currentData += data
//            if (data.hasDataType(DataType.VOLTAGE)) (device as ScannedBluetoothDevice)
//                .write(
//                    GattAttribute.BLUNO_SERIAL_PORT,
//                    Data(device.address).add(DataType.INFO, "IPeter")) { success, answer ->
//                        LLog.d("Write is $success = $answer")
//                    }

            data.dataTypes
                .filter {
                    it.visible
                }
                .forEach {
                    val ddt = DeviceDataType(device, it)
                    if (types.addIfNew(ddt)) {
                        if (PreferenceHelper["Valid_$ddt", false]) validTypes.add(ddt)
                    }
                    if (validTypes.contains(ddt) && ddt == currentDeviceDataType) showValue(ddt)

                }

            if (isNew) data.dataTypes.forEach {
                newDeviceDataTypes.addIfNew(DeviceDataType(device, it)) { ddt -> LLog.d("New device - datatype for $device: $ddt") }
            }

        }
    }


    private fun init() {
        BlueLib.start(
            this@BluDemoMain,
            LibOption.USE_INTERNAL_SENSORS,
            LibOption.USE_BODY_SENSORS,
            LibOption.USE_NMEA,
            LibOption.USE_BLE_SENSORS
        )
        BlueLib.listeners.add(listener)
        hamburger.setOnClickListener {
            val menuItems = mutableListOf(
                R.string.sensors.getString(),
                R.string.types.getString(),
                R.string.settings.getString()
            )

            Alerts.showItemsDialog(this, R.string.menu, menuItems.toTypedArray())  { _, whichString, _ ->
                when (whichString) {
                    R.string.sensors.getString() -> {
                        val devices = BlueLib.getScannedDevices().sortedBy { it.name }
                        val selected = devices.map { it.isSelected }
                        Alerts.showItemsDialog(this, R.string.sensors, devices.map { it.name }.toTypedArray(), selected.toBooleanArray()) { result, whichStr, which ->
                            if (which >= 0) {
                                devices[which].isSelected = result == ClickResult.YES
                                PreferenceHelper["Selected_$whichStr"] = result == ClickResult.YES
                                logEvent( "Selected_$whichStr", "Result" to result.toString())

                            }
                        }
                    }
                    R.string.types.getString() -> {
                        val sortedTypes = types.sortedBy { it.toNiceString() }

                        val typeList = sortedTypes.map { it.toNiceString() }
                        val selected = sortedTypes.map { validTypes.contains(it)}
                        Alerts.showItemsDialog(this, R.string.types, typeList.toTypedArray(), selected.toBooleanArray()) { result, _, which ->
                            if (which >= 0) {
                                val type = sortedTypes[which]
                                if (result == ClickResult.YES) {
                                    validTypes.add(type)
                                    PreferenceHelper["Valid_$type"] = true
                                } else {
                                    validTypes.remove(type)
                                    PreferenceHelper["Valid_$type"] = false
                                }
                                logEvent( "ValidType_$type", "Result" to result.toString())
                            }
                        }
                    }
                    R.string.settings.getString() -> {
                        startActivity<ApplicationSettingsActivity>()
                    }
                }
            }
        }

        timer(name ="ScreenWriter",
            daemon = true,
            period = 1000) {
            val now = System.currentTimeMillis()
            if (currentDeviceDataType == null)
                currentDeviceDataType = validTypes.firstOrNull()
            else
                currentDeviceDataType?.let {
                    if (now - switchTime > 5000L) {
                        currentDeviceDataType = validTypes.nextElement(it)
                        switchTime = now
                    }
                }
            currentDeviceDataType?.let { showValue(it) }
        }

        val info = packageManager.getPackageInfo(packageName, 0)
        @Suppress("DEPRECATION")
        version.text = R.string.version.getString(
            R.string.app_name.getString(),
            info.versionName,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                info.longVersionCode
            else
                info.versionCode,
            TimeUtil.formatTime(BuildConfig.BUILD_TIME),
            BuildConfig.FLAVOR
        )

        if (PreferenceKey.PERIPHERAL.isTrue) {
            timer(
                name = "Beater",
                daemon = true,
                period = 1000
            ) {
                val hr = if (currentData.hasDataType(DataType.HEART_RATE))
                    currentData.getInt(null, DataType.HEART_RATE, 0)
                else
                    (25 + Math.random() * 25).toInt()

                val power = max(maxPower * (hr - hrRest) / (hrMax - hrRest), 0.0)

                //LLog.d("Sending $hr bpm and $power W")
                periServer?.setValue(Data()
                    .add(DataType.HEART_RATE, hr)
                    .add(DataType.POWER, power)
                )
            }

            periServer = BlePeripheralServer(listOf(HeartRateBlePeripheral()))
        }

//        runInMain(5000) {
//            Crashlytics.getInstance().crash()
//        }
    }


    private fun showValue(ddt: DeviceDataType) {
        runInMain {
            val address = ddt.device.address
            currentData.toValuePair(address, ddt.dataType)?.let { (value, unit) ->
                valueView.setTextColor(currentAccuracy[address]?.color ?: resources.getColored(R.color.unknownColor))
                sensorNameView.text = ddt.device.name
                fieldNameView.text = ddt.dataType.toProperString()
                valueView.text = value
                unitView.text = unit
                scanCount.text = if (ddt.device.scanCount > 0) {
                    scanCount.visible = true
                    R.string.times.getString(ddt.device.scanCount.format(0), ddt.device.rssi)
                } else {
                    scanCount.visible = false
                    ""
                }
            }

        }
    }

    override fun onResume() {

        logEvent( "BluDemo_onResume")
        //firebaseAnalytics.logEvent("Resumed ${R.string.app_name.getString()}", null)
        BlueLib.resume()
        super.onResume()
    }

    override fun onPause() {
////        BlueLib.pause()
        if (isFinishing) {
            BlueLib.close()
            periServer?.close()
        }
        super.onPause()

    }

    override fun onDestroy() {
//        BlueLib.close()
//        periServer?.close()
        BlueLib.listeners.remove(listener)
        super.onDestroy()
    }

    companion object {
        const val checkHistory = 10 * TimeUtil.MINUTE
        const val hrMax = 200.0
        const val hrRest = 65
        const val maxPower = 400
    }
}
