package com.plusot.bludemo

import android.app.Activity
import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog

/**
 * Package: com.plusot.bludemo
 * Project: blu
 *
 * Created by Peter Bruinink on 2019-10-28.
 * Copyright © 2019 Plusot. All rights reserved.
 */

class BluDemoApp: Application() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate() {
        super.onCreate()
        Globals.init(this, javaClass.simpleName)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    fun logEvent(activity: Activity, name: String,  params: Array<out Pair<String, String>>) {
        val underScoredname = name
            .replace(" / ", "_")
            .replace(".", "_")
            .replace(' ', '_')

        LLog.i("LogEvent: $underScoredname")
        firebaseAnalytics.logEvent(underScoredname) {
            params.forEach {
                param(it.first, it.second)
            }
        }
        firebaseAnalytics.setCurrentScreen(activity, name, null)
    }
}

fun Activity.logEvent(name: String, vararg params: Pair<String, String>) {
    val app = this.application
    if (app is BluDemoApp) app.logEvent(this, name, params)
}