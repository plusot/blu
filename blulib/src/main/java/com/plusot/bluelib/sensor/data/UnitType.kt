package com.plusot.bluelib.sensor.data


enum class UnitType(val unitClass: Class<*>) {
    DENSITY_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.KG_PER_CUBIC_METER
    },
    DIGITAL_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.BYTE
    },
    VOLTAGE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.VOLT
    },
    PRESSURE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.MBAR
    },
    TEMPERATURE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.CELSIUS
    },
    EFFICIENCY_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.JOULEPERBEAT
    },
    DATETIME_UNIT(Long::class.java) {
        override val isNumeric: Boolean
            get() = false

        override fun defaultUnit(): Unit = Unit.DATETIME

        override fun metricUnit(): Unit = Unit.MILLISECOND
    },
    PULSE_WIDTH_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.MILLISECOND
    },
    SECOND_TIME_UNIT(Long::class.java) {
        override fun defaultUnit(): Unit = Unit.SECOND

        override fun metricUnit(): Unit = Unit.MILLISECOND
    },
    WIND_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.KPH

        override fun metricUnit(): Unit = Unit.MPS
    },
    SPEED_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.KPH

        override fun metricUnit(): Unit = Unit.MPS
    },
    MAGNETIC_FIELD_UNIT(Float::class.java) {
        override val dimensions: Int
            get() = 3

        override fun defaultUnit(): Unit = Unit.MICROTESLA
    },
    ACCELERATION_UNIT(Float::class.java) {
        override val dimensions: Int
            get() = 3

        override fun defaultUnit(): Unit = Unit.MPSS
    },
    DISTANCE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.METER
    },
    ANGLE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.DEGREE
    },
    ANGLE3D_UNIT(Float::class.java) {
        override val dimensions: Int
            get() = 3

        override fun defaultUnit(): Unit = Unit.DEGREE
    },
    CYCLES_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.RPM
    },
    BEATS_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.BPM
    },
    WEIGHT_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.KG
    },
    COUNT_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.COUNT
    },
    HEXCOUNT_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.NONE
    },
    REFERENCED_POWER_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.DBM
    },
    POWER_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.WATT
    },
    FORCE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.N
    },
    ENERGY_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.JOULE
    },
    TORQUE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.NM
    },
    RATIO_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.RATIO
    },
    STEP_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.COUNT
    },
    INT_WITH_NO_UNIT(Int::class.java) {
        override fun defaultUnit(): Unit = Unit.NONE
    },
    DOUBLE_WITH_NO_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.NONE
    },
    TEXT_UNIT(String::class.java) {
        override val isNumeric: Boolean
            get() = false

        override fun defaultUnit(): Unit = Unit.TEXT
    },
    BYTE_ARRAY_UNIT(Byte::class.java) {
        override val dimensions: Int
            get() = -1

        override fun defaultUnit(): Unit = Unit.NONE
    },
    INT_ARRAY_UNIT(Int::class.java) {
        override val dimensions: Int
            get() = -1

        override fun defaultUnit(): Unit = Unit.NONE
    },
    LIGHT_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.LUX
    },
    VOLUME_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.MILLILITER

        override fun metricUnit(): Unit = Unit.LITER
    },
    ROTATIONSPEED_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.DEGREESPERSECOND
    },
    VECTOR_3D_UNIT(Float::class.java) {
        override val dimensions: Int
            get() = 3
        override fun defaultUnit(): Unit = Unit.NONE
    },
    VECTOR_2D_UNIT(Float::class.java) {
        override val dimensions: Int
            get() = 2
        override fun defaultUnit(): Unit = Unit.NONE
    },
    INSULIN_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit= Unit.INSULIN_PULSES
    },
    GLUCOSE_UNIT(Double::class.java) {
        override fun defaultUnit(): Unit = Unit.MMOLPL
    },
    ;


    open val isNumeric: Boolean
        get() = true

    open val dimensions: Int
        get() = 1

    open fun defaultUnit(): Unit = Unit.NONE


    open fun metricUnit(): Unit = defaultUnit()

}
