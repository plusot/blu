package com.plusot.bluelib.sensor.data

import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey
import com.plusot.bluelib.sensor.SensorAccuracy
import com.plusot.bluelib.sensor.bluetooth.ble.*
import com.plusot.bluelib.util.*
import com.plusot.bluelib.util.json.JSoNField
import com.plusot.bluelib.util.json.JSoNHelper
import org.json.JSONException
import org.json.JSONObject

import java.util.Locale

enum class DataType constructor(
    val label: Int,
    val voiceLabel: Int,
    val decimals: Int = 0,
    private val unitType: UnitType = UnitType.INT_WITH_NO_UNIT
) {
    ACCELERATION(R.string.acceleration, 2, UnitType.ACCELERATION_UNIT),
    ACCELERATION_XYZ(R.string.acceleration_xyz, 2, UnitType.ACCELERATION_UNIT),
    AEROBIC_HEART_RATE_LOWER_LIMIT(R.string.aerobic_heart_rate_lower_limit, 0, UnitType.BEATS_UNIT),
    AEROBIC_HEART_RATE_UPPER_LIMIT(R.string.aerobic_heart_rate_upper_limit, UnitType.BEATS_UNIT),
    AEROBIC_THRESHOLD(R.string.aerobic_threshold, UnitType.BEATS_UNIT),
    AGE(R.string.age, 0, UnitType.INT_WITH_NO_UNIT),
    AGGREGATE(R.string.aggregate, 0),
    AIR_PRESSURE(R.string.airpressure, UnitType.PRESSURE_UNIT), // Pa
    ALERT_CATEGORY_ID(R.string.alert_category_id, 0),
    ALERT_CATEGORY_ID_BIT_MASK(R.string.alert_category_id_bit_mask, 0),
    ALERT_LEVEL(R.string.alert_level, 0),
    ALERT_NOTIFICATION_CONTROL_POINT(R.string.alert_notification_control_point, 0),
    ALERT_STATUS(R.string.alert_status, 0),
    ALTITUDE(R.string.altitude, UnitType.DISTANCE_UNIT) {
        override fun hasAvg(): Boolean {
            return false
        }

        override fun hasMin(): Boolean {
            return hasStats()
        }
    }, // m
    ALTITUDE_NMEA(R.string.altitudeNmea, UnitType.DISTANCE_UNIT) {
        override fun hasAvg(): Boolean {
            return false
        }

        override fun hasMin(): Boolean {
            return hasStats()
        }
    }, // m
    ALTITUDE_RAW(R.string.altitudeRaw, UnitType.DISTANCE_UNIT) {
        override fun hasAvg(): Boolean {
            return false
        }

        override fun hasMin(): Boolean {
            return hasStats()
        }
    }, // m
    ANAEROBIC_HEART_RATE_LOWER_LIMIT(R.string.anaerobic_heart_rate_lower_limit, UnitType.BEATS_UNIT),
    ANAEROBIC_HEART_RATE_UPPER_LIMIT(R.string.anaerobic_heart_rate_upper_limit, UnitType.BEATS_UNIT),
    ANAEROBIC_THRESHOLD(R.string.anaerobic_threshold, 0),
    ANALOG(R.string.analog, 0),
    ANDROID_VERSION(R.string.android_version, UnitType.TEXT_UNIT),
    APPARENT_WIND_DIRECTION_(R.string.apparent_wind_direction, 0, UnitType.ANGLE_UNIT),
    APPARENT_WIND_SPEED(R.string.apparent_wind_speed, 0, UnitType.SPEED_UNIT),
    APPEARANCE(R.string.appearance, UnitType.TEXT_UNIT),
    ASCENT(R.string.ascent_total, UnitType.DISTANCE_UNIT) {
        override val isAggregate: Boolean
            get() = true

        override fun hasStats(): Boolean {
            return false
        }
    }, // m
    BAROMETRIC_PRESSURE_TREND(R.string.barometric_pressure_trend, 0),
    BATTERY_LEVEL(R.string.battery_level, 0, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    },
    BATTERY_VOLTAGE(R.string.battery_level, 1, UnitType.VOLTAGE_UNIT) {
        override val unit: Unit
            get() = Unit.VOLT
    },
    BATTERY_INFO(R.string.battery_info, UnitType.TEXT_UNIT),
    BEARING(R.string.bearing, UnitType.ANGLE_UNIT), // degrees
    BLOOD_PRESSURE_FEATURE(R.string.blood_pressure_feature, 0),
    BLOOD_PRESSURE_SYSTOLIC(R.string.blood_pressure_systolic, 0, UnitType.PRESSURE_UNIT) {
        override val unit: Unit
            get() = Unit.MMHG
    },
    BLOOD_PRESSURE_DIASTOLIC(R.string.blood_pressure_diastolic, 0, UnitType.PRESSURE_UNIT) {
        override val unit: Unit
            get() = Unit.MMHG
    },
    BLOOD_PRESSURE_MEAN(R.string.blood_pressure_mean, 0, UnitType.PRESSURE_UNIT) {
        override val unit: Unit
            get() = Unit.MMHG
    },
    BODY_COMPOSITION_FEATURE(R.string.body_composition_feature, 0),
    BODY_COMPOSITION_MEASUREMENT(R.string.body_composition_measurement, 0),
    BODY_MASS_INDEX(R.string.bmi, 2, UnitType.DOUBLE_WITH_NO_UNIT),
    BODY_SENSOR_LOCATION(R.string.body_sensor_location, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    BOND_MANAGEMENT_CONTROL_POINT(R.string.bond_management_control_point, 0),
    BOND_MANAGEMENT_FEATURE(R.string.bond_management_feature, 0),
    BOOT_KEYBOARD_INPUT_REPORT(R.string.boot_keyboard_input_report, 0),
    BOOT_KEYBOARD_OUTPUT_REPORT(R.string.boot_keyboard_output_report, 0),
    BOOT_MOUSE_INPUT_REPORT(R.string.boot_mouse_input_report, 0),
    CADENCE(R.string.cadence, UnitType.CYCLES_UNIT), // rpm
    CADENCE_INSTANT(R.string.cadence_instant, UnitType.CYCLES_UNIT), // rpm
    CENTRAL_ADDRESS_RESOLUTION(R.string.central_address_resolution, 0),
    CGM_FEATURE(R.string.cgm_feature, 0),
    CGM_MEASUREMENT(R.string.cgm_measurement, 0),
    CGM_SESSION_RUN_TIME(R.string.cgm_session_run_time, 0),
    CGM_SESSION_START_TIME(R.string.cgm_session_start_time, 0),
    CGM_SPECIFIC_OPS_CONTROL_POINT(R.string.cgm_specific_ops_control_point, 0),
    CGM_STATUS(R.string.cgm_status, 0),
    COMPASS(R.string.compass, UnitType.ANGLE_UNIT), // degrees
    CPU(R.string.cpu, 0, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    },
    CRANK_TORQUE(R.string.crank_torque, UnitType.TORQUE_UNIT),
    CSC_FEATURE(R.string.csc_feature, 0),
    CSC_MEASUREMENT(R.string.csc_measurement, 0),
    CYCLING_POWER_CONTROL_POINT(R.string.cycling_power_control_point, 0),
    CYCLING_POWER_FEATURE(R.string.cycling_power_feature, 0),
    CYCLING_POWER_MEASUREMENT(R.string.cycling_power_measurement, 0),
    CYCLING_POWER_VECTOR(R.string.cycling_power_vector, 0),
    DATABASE_CHANGE_INCREMENT(R.string.database_change_increment, 0),
    DATA_SOURCE(R.string.datasource, UnitType.TEXT_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    DATE_OF_BIRTH(R.string.date_of_birth, 0),
    DATE_OF_THRESHOLD_ASSESSMENT(R.string.date_of_threshold_assessment, 0),
    DATE_TIME(R.string.date_time, UnitType.DATETIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SHORTTIME
    },
    DAY_DATE_TIME(R.string.day_date_time, UnitType.COUNT_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SHORTTIME
    },
    DAY_OF_WEEK(R.string.day_of_week, UnitType.COUNT_UNIT),
    DESCENT(R.string.descent_total, UnitType.DISTANCE_UNIT) {
        override val isAggregate: Boolean
            get() = true

        override fun hasStats(): Boolean {
            return false
        }
    }, // m
    DESCRIPTOR_VALUE_CHANGED(R.string.descriptor_value_changed, 0),
    DEVICE_NAME(R.string.device_name, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    DIGITAL(R.string.digital, 0),
    DISTANCE(R.string.distance, 1, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM
        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 5000.0
        }
    },
    DISTANCE_TOTAL(R.string.distance_total, 0, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM
        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 10000.0
        }
    },
    DISTANCE_START(R.string.start_distance, 1, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM
        override val isSharable: Boolean
            get() = false

        override fun minInterval(): Double {
            return 10000.0
        }

        override fun hasStats(): Boolean {
            return false
        }
    },
    DISTANCE_POI(R.string.distance_poi, 1, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM
        override val isSharable: Boolean
            get() = false

        override fun minInterval(): Double {
            return 10000.0
        }

        override fun hasStats(): Boolean {
            return false
        }
    },
    DEW_POINT(R.string.dew_point, 0),
    DST_OFFSET(R.string.dst_offset, 0),
    EFFICIENCY(R.string.efficiency, UnitType.EFFICIENCY_UNIT), // ratio
    ELEVATION(R.string.elevation, 0),
    EMAIL_ADDRESS(R.string.email_address, UnitType.TEXT_UNIT),
    ENERGY(R.string.energy, 1, UnitType.ENERGY_UNIT) {
        override val unit: Unit
            get() = Unit.KILOJOULE
    },
    EOF(R.string.eof, 0, UnitType.INT_WITH_NO_UNIT),
    EXACT_TIME_256(R.string.exact_time_256, 0),
    FAT_RATIO(R.string.fat, 2, UnitType.DOUBLE_WITH_NO_UNIT),
    FAT_BURN_HEART_RATE_LOWER_LIMIT(R.string.fat_burn_heart_rate_lower_limit, 0),
    FAT_BURN_HEART_RATE_UPPER_LIMIT(R.string.fat_burn_heart_rate_upper_limit, 0),
    FIRMWARE_REVISION_STRING(R.string.firmware_revision_string, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    FIRST_NAME(R.string.first_name, 0),
    FIVE_ZONE_HEART_RATE_LIMITS(R.string.five_zone_heart_rate_limits, 0),
    GATT_STATUS(R.string.gattState) {
        override fun toValueString(isSpeech: Boolean, value: Int): Array<String> {
            return arrayOf(GattState.fromId(value).toString())
        }
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    GATT_ATTRIBUTE_READ(R.string.gattAttributeRead) {
        override fun toValueString(isSpeech: Boolean, value: Int): Array<String> {
            return arrayOf(GattAttribute.values()[value].toString())
        }
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    GATT_ATTRIBUTE_DISCOVERED(R.string.gattAttributeDiscovered) {
        override fun toValueString(isSpeech: Boolean, value: Int): Array<String> {
            return arrayOf(GattAttribute.values()[value].toString())
        }
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    GAIN_RATIO(R.string.gearratio, UnitType.RATIO_UNIT), // ratio
    GEOID_HEIGHT(R.string.geoidHeight, UnitType.DISTANCE_UNIT),
    GENDER(R.string.gender, 0),
    GLUCOSE_FEATURE(R.string.glucose_feature, 0),
    GLUCOSE_MEASUREMENT(R.string.glucose_measurement, 0),
    GLUCOSE_MEASUREMENT_CONTEXT(R.string.glucose_measurement_context, 0),
    GPX_DISTANCE(R.string.gpx_distance, 1, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM
    }, // m
    GPX_DISTANCE_TO_DO(R.string.gpx_distance_end_real, 1, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.KM

        override val isSharable: Boolean
            get() = false

        override fun hasStats(): Boolean {
            return false
        }

    }, // m
    GUST_FACTOR(R.string.gust_factor, 0),
    GYROSCOPE(R.string.gyroscope, UnitType.ANGLE3D_UNIT),
    HARDWARE_REVISION_STRING(R.string.hardware_revision_string, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices


    },
    HDOP(R.string.hdop, UnitType.RATIO_UNIT),
    HEART_RATE(R.string.heart_rate, UnitType.BEATS_UNIT) {
        override fun minInterval(): Double {
            return 10.0
        }
    }, // bpm
    HEART_RATE_CONTROL_POINT(R.string.heart_rate_control_point, 0),
    HEART_RATE_MAX(R.string.heart_rate_max, 0),
    HEART_RATE_MEASUREMENT(R.string.heart_rate_measurement, 0),
    HEART_RATE_ZONE(R.string.zone, UnitType.BEATS_UNIT),
    HEAT_INDEX(R.string.heat_index, 0),
    HEIGHT(R.string.height, 2, UnitType.DISTANCE_UNIT),
    HID_CONTROL_POINT(R.string.hid_control_point, 0),
    HID_INFORMATION(R.string.hid_information, 0),
    HIP_CIRCUMFERENCE(R.string.hip_circumference, 0),
    HUMIDITY(R.string.humidity, R.string.humidity, 0, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    }, // ratio
    IBEACON_ADVERTISEMENT(R.string.ibeacon_advertisment, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices


    },
    IBEACON_MAJOR(R.string.ibeacon_major, 0, UnitType.INT_WITH_NO_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices


    },
    IBEACON_MINOR(R.string.ibeacon_minor, 0, UnitType.INT_WITH_NO_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices


    },
    IBEACON_UUID(R.string.ibeacon_uuid, 0, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    IBEACON_POWER(R.string.ibeacon_power, 0, UnitType.REFERENCED_POWER_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    IEEE_CERTIFICATION(R.string.ieee_11073_20601_regulatory_certification_data_list, 0),
    INFO(R.string.textInfo, 0, UnitType.TEXT_UNIT),
    INTERMEDIATE_CUFF_PRESSURE(R.string.intermediate_cuff_pressure, 0),
    IRRADIANCE(R.string.irradiance, 0),
    KEY(R.string.key, UnitType.COUNT_UNIT),
    KIEN_ARRAY(R.string.kienArray, UnitType.BYTE_ARRAY_UNIT),
    LANGUAGE(R.string.language, 0),
    LAST_NAME(R.string.last_name, 0),
    LEVEL(R.string.level, UnitType.COUNT_UNIT),
    LIGHT(R.string.light, UnitType.LIGHT_UNIT),
    LN_CONTROL_POINT(R.string.ln_control_point, 0),
    LN_FEATURE(R.string.ln_feature, 0),
    LOCAL_TIME_INFORMATION(R.string.local_time_information, 0),
    LOCATION(R.string.location, 4, UnitType.ANGLE_UNIT) {
        override val isMapType: Boolean get() = true
    }, // degrees
    LOCATION_NMEA(R.string.locationNmea, 4, UnitType.ANGLE_UNIT) {
        override val isMapType: Boolean get() = true
    }, // degrees
    LOCATION_AND_SPEED(R.string.location_and_speed, 0),
    MAGNETIC_DECLINATION(R.string.magnetic_declination, 0),
    MAGNETIC_FLUX_DENSITY_2D(R.string.magnetic_flux_density_2d, 0),
    MAGNETIC_FLUX_DENSITY_3D(R.string.magnetic_flux_density_3d, 0, UnitType.MAGNETIC_FIELD_UNIT),
    MANUFACTURER_NAME_STRING(R.string.manufacturer_name_string, 0, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },

    MANUFACTURER_ID(R.string.manufacturer_id, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    MAXIMUM_RECOMMENDED_HEART_RATE(R.string.maximum_recommended_heart_rate, 0),
    MEASUREMENT_INTERVAL(R.string.measurement_interval, 0),
    MEMORY(R.string.allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        override val unit: Unit get() = Unit.MEGA_BYTE
    },
    MEMORY_FREE(R.string.free_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean get() = Globals.verboseDevices
        override val unit: Unit get() = Unit.MEGA_BYTE
    },
    MEMORY_GC_ALLOCATED(R.string.gc_allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean get() = Globals.verboseDevices
        override val unit: Unit get() = Unit.MEGA_BYTE
    },
    MEMORY_GC_FREED(R.string.gc_freed_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_GC(R.string.gc_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_HEAP_ALLOCATED(R.string.heap_allocated_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_TOTAL(R.string.total_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_MAX(R.string.max_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_PSS(R.string.pss_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MEMORY_RSS(R.string.rss_memory, 0, UnitType.DIGITAL_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MEGA_BYTE
    },
    MODEL_NUMBER_STRING(R.string.model_number_string, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    MUSCLE_RATIO(R.string.muscle_ratio, 2, UnitType.DOUBLE_WITH_NO_UNIT),
    NAVIGATION(R.string.navigation, 0),
    NEW_ALERT(R.string.new_alert, 0),
    NMEA(R.string.nmea, UnitType.TEXT_UNIT),
    ORIENTATION(R.string.orientation, 1, UnitType.ANGLE3D_UNIT),
    PEDAL_SMOOTHNESS(R.string.pedal_smoothness, 2, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    }, // ratio
    PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS(R.string.peripheral_preferred_connection_parameters, 0) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    PERIPHERAL_PRIVACY_FLAG(R.string.peripheral_privacy_flag, 0),
    PITCH(R.string.pitch, UnitType.ANGLE_UNIT),
    PNP_ID(R.string.pnp_id, 0),
    POLLEN_CONCENTRATION(R.string.pollen_concentration, 0),
    POSITION_QUALITY(R.string.position_quality, UnitType.COUNT_UNIT),
    POWER(R.string.power, UnitType.POWER_UNIT), // W
    POWER_INSTANT(R.string.power_instant, UnitType.POWER_UNIT),
    POWER_ACCUMULATED(R.string.power_accumulated, UnitType.POWER_UNIT) {
        override val isGraphable: Boolean
            get() = false

        override val visible: Boolean
            get() = Globals.verboseDevices

    }, // W
    POWER_PEDAL(R.string.power_pedal, 2, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    }, // ratio,                        // ratio
    PRESSURE(R.string.pressure, 0, UnitType.PRESSURE_UNIT),
    PROTEIN(R.string.protein, 2, UnitType.DOUBLE_WITH_NO_UNIT),
    PROTOCOL_MODE(R.string.protocol_mode, 0),
    PROXIMITY(R.string.proximity, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.CM

    },
    PTS_SERIAL(R.string.ptsSerial, UnitType.COUNT_UNIT),
    PTS_ANSWER(R.string.ptsAnswer, UnitType.BYTE_ARRAY_UNIT),
    PTS_HBA1C(R.string.ptsHbA1c, UnitType.RATIO_UNIT),
    PTS_TEST_RESULT(R.string.ptsTestResult, UnitType.TEXT_UNIT),
    PULSE_DATA(R.string.pulseData, 0, UnitType.INT_WITH_NO_UNIT),
    PULSE_WIDTH(R.string.pulsewidth, UnitType.PULSE_WIDTH_UNIT),
    RADBEACON_DEVICE_NAME(R.string.radbeacon_device_name, UnitType.TEXT_UNIT),
    RAINFALL(R.string.rainfall, 0),
    RECONNECTION_ADDRESS(R.string.reconnection_address, 0),
    RECORD_ACCESS_CONTROL_POINT(R.string.record_access_control_point, 0),
    REFERENCE_TIME_INFORMATION(R.string.reference_time_information, 0),
    REPORT(R.string.report, 0),
    REPORT_MAP(R.string.report_map, 0),
    RESTING_HEART_RATE(R.string.resting_heart_rate, 0),
    RHO(R.string.rho, 3, UnitType.DENSITY_UNIT),
    RINGER_CONTROL_POINT(R.string.ringer_control_point, 0),
    RINGER_SETTING(R.string.ringer_setting, 0),
    ROLL(R.string.roll, UnitType.ANGLE_UNIT),
    ROTATION(R.string.rotation, 1, UnitType.ANGLE_UNIT),
    ROTATION_MAX(R.string.rotation_max, 1, UnitType.ANGLE_UNIT),
    ROTATION_MIN(R.string.rotation_min, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO(R.string.rotation_accelero, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO_MAX(R.string.rotation_accelero_max, 1, UnitType.ANGLE_UNIT),
    ROTATION_ACCELERO_MIN(R.string.rotation_accelero_min, 1, UnitType.ANGLE_UNIT),
    ROTATION_RAW(R.string.rotation_raw, 1, UnitType.ANGLE_UNIT),
    ROTATION_SPEED(R.string.rotation_speed, 1, UnitType.ROTATIONSPEED_UNIT),
    ROTATION_TOTAL(R.string.rotationtotal, 0, UnitType.ANGLE_UNIT),
    RSC_FEATURE(R.string.rsc_feature, 0),
    RSC_MEASUREMENT(R.string.rsc_measurement, 0),
    RSSI(R.string.rssi, UnitType.REFERENCED_POWER_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    RR_INTERVAL(R.string.interval, UnitType.SECOND_TIME_UNIT) {
        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    SATELLITES_USED(R.string.satellites_used, UnitType.INT_WITH_NO_UNIT) {
        override val isSharable: Boolean
            get() = false
    }, // #
    SATELLITES_FIX(R.string.satellites_fix, UnitType.INT_WITH_NO_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isSharable: Boolean
            get() = false
    }, // #
    SATELLITES_EVENT(R.string.satellites_event, UnitType.INT_WITH_NO_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isSharable: Boolean
            get() = false
    }, // #,                        // #
    SATELLITES_VISIBLE(R.string.satellites_visible, UnitType.INT_WITH_NO_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isSharable: Boolean
            get() = false
    }, // #,                        // #
    SATELLITES_STATUS(R.string.satellites, UnitType.INT_WITH_NO_UNIT) {
        override val isSharable: Boolean
            get() = false
    }, // #
    SC_CONTROL_POINT(R.string.sc_control_point, 0),
    SCAN_INTERVAL_WINDOW(R.string.scan_interval_window, 0),
    SCAN_REFRESH(R.string.scan_refresh, 0),
    SENSOR_ACCURACY(R.string.sensorAccuracy) {
        override fun toValueString(isSpeech: Boolean, value: Int): Array<String> {
            return arrayOf(SensorAccuracy.values()[value].toString())
        }
    },
    SENSOR_LOCATION(R.string.sensor_location, 0),

    SERIAL_NUMBER_STRING(R.string.serial_number_string, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    SERVICE_CHANGED(R.string.service_changed, 0),
    SLOPE(R.string.slope, UnitType.RATIO_UNIT) {
        override val unit: Unit
            get() = Unit.PERCENT
    },
    SOFTWARE_REVISION_STRING(R.string.software_revision, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    SPEED(R.string.speed, UnitType.SPEED_UNIT) {
        override fun minInterval(): Double {
            return 5.0 / 3.6
        }
    }, // m/s
    SPEED_AVG(R.string.speed_avg, UnitType.SPEED_UNIT) {
        override fun hasStats(): Boolean {
            return false
        }

    }, // m/s
    SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS(R.string.sport_type_for_aerobic_and_anaerobic_thresholds, 0),
    STANDARD_DEVIATION(R.string.standardDeviation, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    STANDARD_DEVIATION_2(R.string.standardDeviation2, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    STATE(R.string.state, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices
    },
    STEPS(R.string.steps, UnitType.STEP_UNIT) {

        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 500.0
        }

        override fun hasStats(): Boolean {
            return false
        }
    },
    STEPS_TOTAL(R.string.steps_total, UnitType.STEP_UNIT) {
        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 1000.0
        }

        override fun hasStats(): Boolean {
            return false
        }
    },
    STEP_SPEED(R.string.step_speed, UnitType.CYCLES_UNIT),
    SUPPORTED_NEW_ALERT_CATEGORY(R.string.supported_new_alert_category, 0),
    SUPPORTED_UNREAD_ALERT_CATEGORY(R.string.supported_unread_alert_category, 0),
    SYSTEM_ID(R.string.system_id, 0),
    TEMPERATURE(R.string.temperature, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_2(R.string.temperature2, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_INTERMEDIATE(R.string.temperature_intermediate, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_IR(R.string.temperature_ir, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_OBJECT(R.string.temperature_object, 1, UnitType.TEMPERATURE_UNIT),
    TEMPERATURE_TYPE(R.string.temperature_type, 0, UnitType.TEMPERATURE_UNIT),
    THREE_ZONE_HEART_RATE_LIMITS(R.string.three_zone_heart_rate_limits, 0),
    TI_TEMPERATURE_CONFIG(R.string.ti_temperature_config, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isGraphable: Boolean
            get() = false
    },
    TI_TEMPERATURE_PERIOD(R.string.ti_temperature_period, UnitType.SECOND_TIME_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isGraphable: Boolean
            get() = false

        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    TI_LIGHT_CONFIG(R.string.ti_light_config, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val isGraphable: Boolean
            get() = false
    },
    TI_LIGHT_PERIOD(R.string.ti_light_period, UnitType.SECOND_TIME_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    TI_HUMIDITY_CONFIG(R.string.ti_humidity_config, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_HUMIDITY_PERIOD(R.string.ti_humidity_period, UnitType.SECOND_TIME_UNIT) {

        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    TI_IO_DATA(R.string.ti_io, UnitType.HEXCOUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_IO_CONFIG(R.string.ti_io_config, UnitType.TEXT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_LEFT_KEY(R.string.ti_left_key, UnitType.HEXCOUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_RIGHT_KEY(R.string.ti_right_key, UnitType.HEXCOUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_REED_RELAY(R.string.ti_reed_relay, UnitType.HEXCOUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_BAROMETER_CONFIG(R.string.ti_barometer_config, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_BAROMETER_PERIOD(R.string.ti_barometer_period, UnitType.SECOND_TIME_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    TI_MOVEMENT_CONFIG_GYRO_ON(R.string.ti_movement_config_gyro, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_MOVEMENT_CONFIG_ACCELEROMETER_ON(R.string.ti_movement_config_accelerometer, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_MOVEMENT_CONFIG_MAGNETOMETER_ON(R.string.ti_movement_config_magnetometer, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_MOVEMENT_CONFIG_WAKE_ON_MOTION(R.string.ti_movement_config_wake_on_motion, UnitType.COUNT_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE(
        R.string.ti_movement_config_accelerometer_range,
        UnitType.ACCELERATION_UNIT
    ) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    TI_MOVEMENT_PERIOD(R.string.ti_movement_period, UnitType.SECOND_TIME_UNIT) {
        override val visible: Boolean
            get() = Globals.verboseDevices
        override val unit: Unit
            get() = Unit.MILLISECOND
    },
    TIME_CLOCK(R.string.time, UnitType.DATETIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.TIME
    }, // milliseconds
    TIME_GPS(R.string.time_gps, UnitType.DATETIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SHORTTIME
    }, // milliseconds
    TIME_TOTAL(R.string.time_total, UnitType.SECOND_TIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SECOND
        override val isAggregate: Boolean
            get() = true
    },
    TIME_ACCURACY(R.string.time_accuracy, 0),
    TIME_ACTIVITY(R.string.time_active, UnitType.SECOND_TIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SECOND
        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 300000.0
        }
    },
    TIME_ACTIVE(R.string.time_moving, UnitType.SECOND_TIME_UNIT) {
        override val isGraphable: Boolean
            get() = false
        override val unit: Unit
            get() = Unit.SECOND
        override val isAggregate: Boolean
            get() = true

        override fun minInterval(): Double {
            return 300000.0
        }
    },
    TIME_SOURCE(R.string.time_source, 0),
    TIME_UPDATE_CONTROL_POINT(R.string.time_update_control_point, 0),
    TIME_UPDATE_STATE(R.string.time_update_state, 0),
    TIME_WITH_DST(R.string.time_with_dst, 0),
    TIME_ZONE(R.string.time_zone, 0),
    TRUE_WIND_DIRECTION(R.string.true_wind_direction, 0),
    TRUE_WIND_SPEED(R.string.true_wind_speed, 0),
    TWO_ZONE_HEART_RATE_LIMIT(R.string.two_zone_heart_rate_limit, 0),
    TX_POWER_LEVEL(R.string.tx_power_level, 0),
    TYPE_MAJOR(R.string.type_major) {
        override val visible: Boolean
            get() = Globals.verboseDevices

    },
    UNREAD_ALERT_STATUS(R.string.unread_alert_status, 0),
    USER_CONTROL_POINT(R.string.user_control_point, 0),
    USER_INDEX(R.string.user_index, 0),
    USER_NAME(R.string.user_name, UnitType.TEXT_UNIT),
    UV_INDEX(R.string.uv_index, 0),
    VECTOR(R.string.vector, 1, UnitType.VECTOR_3D_UNIT),
    VERTICAL_SPEED(R.string.vertspeed, UnitType.SPEED_UNIT),
    VISCERAL_FAT_AREA(R.string.vfa, 1, UnitType.DOUBLE_WITH_NO_UNIT),
    VISCERAL_FAT_INDEX(R.string.vfi, UnitType.RATIO_UNIT),
    VO2_MAX(R.string.vo2_max, 0),
    VOLTAGE(R.string.voltage, 2, UnitType.VOLTAGE_UNIT), // V
    WAIST_CIRCUMFERENCE(R.string.waist_circumference, 0),
    WAIST_HIP_RATIO(R.string.waist_hip_ratio, 2, UnitType.RATIO_UNIT),
    WEIGHT(R.string.weight, 2, UnitType.WEIGHT_UNIT),
    WEIGHT_MEASUREMENT(R.string.weight_measurement, 0),
    WEIGHT_SCALE_FEATURE(R.string.weight_scale_feature, 0),
    WHEEL_REVOLUTIONS(R.string.wheelrevolutions, 0, UnitType.CYCLES_UNIT),
    WHEEL_TORQUE(R.string.wheel_torque, UnitType.TORQUE_UNIT),
    WHEEL_CIRCUMFERENCE(R.string.wheel_circumference, 3, UnitType.DISTANCE_UNIT) {
        override val unit: Unit
            get() = Unit.METER
    },
    WIND_CHILL(R.string.wind_chill, 0),
    WIND_DIRECTION(R.string.winddirection, UnitType.ANGLE_UNIT), // degrees
    WIND_SPEED(R.string.windspeed, 1, UnitType.WIND_UNIT), // m/s
    UNKNOWN(R.string.unknown, 0);

    open val unit: Unit

    open val isGraphable: Boolean
        get() {
            when (unitType) {
                UnitType.DATETIME_UNIT, UnitType.SECOND_TIME_UNIT, UnitType.TEXT_UNIT -> return false
                else -> return true
            }

        }

    open val isShareAlways: Boolean
        get() = false

    open val isSharable: Boolean
        get() = true

    open val isAggregate: Boolean
        get() = false

    open val visible: Boolean
        get() = true

    open val isMapType: Boolean
        get() = false
    //    static EnumMap<DataType, Unit> units = new EnumMap<>(DataType.class);

    constructor(label: Int, decimals: Int = 0) : this(label, label, decimals)

    constructor(label: Int, unitType: UnitType) : this(label, label, 0, unitType)

    constructor(label: Int, decimals: Int, unitType: UnitType) : this(label, label, decimals, unitType)

    init {
        unit = unitType.defaultUnit()
    }

    fun toProperString(): String = "${label.getString()}"
    override fun toString(): String = "${label.getString()}"

    open fun toString(value: Double): String {
        val appContext = Globals.appContext
        return if (appContext != null) appContext.getString(label) + " = " + toValueString(value, true, false) else ""
    }

    open fun toString(value: Int): String = when (this.unitType) {
        UnitType.HEXCOUNT_UNIT -> "${label.getString()} = ${StringUtil.toHexString(value)}"
        else -> "${label.getString()} = ${toValueString(value, true, false)}"
    }

    fun toString(value: Boolean): String = "${label.getString()} = ${toValueString(value, true, false)}"


    fun toString(value: String): String = "${label.getString()} = $value"

    fun toString(value: FloatArray): String {
        val v = MathVector(value, 3)
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(label) + " = " + v.toString(decimals) + " " + unit.toString()
    }

    fun toValueString(value: FloatArray): String {
        val v = MathVector(value, 3)
        return v.toString(decimals)
    }

    fun toValueString(value: IntArray): String {
        return value.fold("") { acc, intValue -> "${acc.addIfNotEmpty(", ")}$intValue" }
    }

    fun toValueString(value: ByteArray): String {
        return value.toHex()
    }

    fun toString(value: DoubleArray): String {
        val v = MathVector(value)
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(label) + " = " + v.toString(decimals) + " " + unit.toString()
    }

    open fun toString(value: ByteArray, delim: String): String = "${label.getString()} = ${value.toHex()} $unit"
    open fun toString(value: IntArray, delim: String): String = "${label.getString()} = ${StringUtil.toString(value, delim)} $unit"


    fun toValueString(value: DoubleArray): String {
        val v = MathVector(value)
        return v.toString(decimals)
    }

    fun toString(value: Long): String {
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(label) + " = " + toValueString(value, true, false)
    }

    fun toVoiceString(value: Double): String {
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(voiceLabel) + " " + toValueString(value, true, true)
    }

    fun toVoiceString(value: Int): String {
        val appContext = Globals.appContext ?: return ""
        when (this.unitType) {
            UnitType.HEXCOUNT_UNIT -> return appContext.getString(voiceLabel) + " " + StringUtil.toHexString(value)
            else -> return appContext.getString(voiceLabel) + " " + toValueString(value, true, true)
        }
    }

    fun toVoiceString(value: Boolean): String {
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(voiceLabel) + " " + toValueString(value, true, true)
    }

    fun toVoiceString(value: String): String {
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(voiceLabel) + " " + value
    }

    fun toVoiceString(value: FloatArray): String {
        val appContext = Globals.appContext ?: return ""
        val v = MathVector(value)
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) + " " + unit.toVoiceString()
    }

    fun toVoiceString(value: IntArray): String {
        val appContext = Globals.appContext ?: return ""
        val v = MathVector(value)
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) + " " + unit.toVoiceString()
    }

    fun toVoiceString(value: ByteArray): String {
        val appContext = Globals.appContext ?: return ""
        val v = MathVector(value)
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) + " " + unit.toVoiceString()
    }

    fun toVoiceString(value: DoubleArray): String {
        val v = MathVector(value)
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(voiceLabel) + " " + v.toString(decimals) + " " + unit.toVoiceString()
    }

    fun toVoiceString(value: Long): String {
        val appContext = Globals.appContext ?: return ""
        return appContext.getString(voiceLabel) + " " + toValueString(value, true, true)
    }


    fun toValueString(value: Double, withUnit: Boolean, isSpeech: Boolean): String {
        val ret = toValueString(isSpeech, value)
        if (ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueString(value: Float, withUnit: Boolean, isSpeech: Boolean): String {
        val ret = toValueString(isSpeech, value)
        if (ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueString(value: Long, withUnit: Boolean, isSpeech: Boolean): String {
        val ret = toValueString(isSpeech, value)
        if (ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueString(value: Int, withUnit: Boolean, isSpeech: Boolean): String {
        val ret = toValueString(isSpeech, value)
        if (ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueString(value: Boolean, withUnit: Boolean, isSpeech: Boolean): String {
        val ret = toValueString(value)
        if (ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueString(isSpeech: Boolean, value: Double): Array<String> {
        return if (isSpeech) arrayOf(
            Format.format(unit.scale(value), decimals, Locale.getDefault()),
            unit.toVoiceString()
        ) else arrayOf(
            Format.format(unit.scale(value), decimals),
            unit.toString()
        )
    }

    fun toValueString(isSpeech: Boolean, value: Float): Array<String> {
        return if (isSpeech) arrayOf(
            Format.format(unit.scale(value.toDouble()), decimals, Locale.getDefault()),
            unit.toVoiceString()
        ) else arrayOf(Format.format(unit.scale(value.toDouble()), decimals), unit.toString())
    }

    fun toValueString(isSpeech: Boolean, value: Long): Array<String> {
        val unit = unit
        when (unit.unitType) {
            UnitType.SECOND_TIME_UNIT -> when (unit) {
                Unit.MILLISECOND -> {
                    return if (isSpeech) arrayOf(
                        TimeUtil.formatElapsedMilliSpoken(
                            value,
                            true
                        )
                    ) else arrayOf(TimeUtil.formatElapsedMilli(value, false))
                }
                Unit.TENTHSECOND -> {
                    return if (isSpeech) arrayOf(
                        TimeUtil.formatElapsedTenthsSpoken(
                            value,
                            true
                        )
                    ) else arrayOf(TimeUtil.formatElapsedTenths(value, false))
                }
                Unit.SECOND -> {
                    return if (isSpeech) arrayOf(
                        TimeUtil.formatElapsedSecondsSpoken(
                            value,
                            true
                        )
                    ) else arrayOf(TimeUtil.formatElapsedSeconds(value, false))
                }
                Unit.HOURSECOND -> {
                    return if (isSpeech) arrayOf(TimeUtil.formatElapsedHourSecondsSpoken(value, true)) else arrayOf(
                        TimeUtil.formatElapsedHourSeconds(value, false)
                    )
                }
                else -> {}
            }
            UnitType.DATETIME_UNIT -> when (unit) {
                Unit.DATETIME, Unit.LONGDATE -> return arrayOf(TimeUtil.formatTime(value, "dd MMM yyyy HH:mm"))
                Unit.TIME -> return arrayOf(TimeUtil.formatTime(value, "HH:mm:ss"))
                Unit.SHORTTIME -> return arrayOf(TimeUtil.formatTime(value, "HH:mm"))
                Unit.SHORTDATE -> {
                    if (Math.abs(value - System.currentTimeMillis()) > 31557600000L)
                        return arrayOf(TimeUtil.formatTime(value, "MMM yy"))
                    else if (Math.abs(value - System.currentTimeMillis()) > 2 * 86400000L)
                        return arrayOf(TimeUtil.formatTime(value, "dd MMM"))
                    return arrayOf(TimeUtil.formatTime(value, "EEE HH:mm"))
                }
                else -> return arrayOf(TimeUtil.formatTime(value, "dd MMM yyyy HH:mm"))
            }
            else -> {
            }
        }
        return if (isSpeech) arrayOf("" + unit.scale(value), unit.toVoiceString()) else arrayOf(
            "" + unit.scale(value),
            unit.toString()
        )

    }

    open fun toValueString(isSpeech: Boolean, value: Int): Array<String> {
        when (this.unitType) {
            UnitType.HEXCOUNT_UNIT -> return arrayOf(StringUtil.toHexString(value))
            else -> {
                return if (isSpeech) arrayOf(
                    "" + unit.scale(value.toLong()),
                    unit.toVoiceString()
                ) else arrayOf("" + unit.scale(value.toLong()), unit.toString())
            }
        }
    }

    open fun toValueString(value: Boolean): Array<String> {
        return arrayOf(if (value) R.string.trueValue.getString() else R.string.falseValue.getString())
    }

    fun toJSON(value: Any, sensorId: String?): JSONObject {
        val jValue = JSONObject()
        try {
            jValue.put(JSoNField.DATATYPE.toString(), name.toLowerCase())
            JSoNHelper[jValue, JSoNField.VALUE] = value
            jValue.put(JSoNField.CLASS.toString(), value.javaClass.simpleName)
            jValue.put(JSoNField.UNIT.toString(), unitType.metricUnit())
            if (sensorId != null) jValue.put(JSoNField.SENSORID.toString(), sensorId)

        } catch (e: JSONException) {
            LLog.e("Could not create JSON", e)
        }

        return jValue
    }

    open fun hasMin(): Boolean {
        return false
    }

    open fun hasAvg(): Boolean {
        return hasStats()
    }

    fun hasMax(): Boolean {
        return hasStats()
    }

    open fun hasStats(): Boolean {
        return isGraphable && !isAggregate && PreferenceKey.STATS.isTrue
    }

    fun floatAvgFactor(): Double {
        return 0.2
    }

    open fun minInterval(): Double {
        return 1.0
    }

    companion object {

        fun fromString(value: String): DataType? {
            for (type in DataType.values())
                if (type.name == value) return type
            return null
        }

        fun fromInt(value: Int): DataType? {
            return if (value < 0 || value >= DataType.values().size) null else DataType.values()[value]
        }

        //    public void toShortJSON(JSONObject jObj, Object value, String sensorId) {
        //        if (sensorId != null && sensorId.length() > 0)
        //            JSoNHelper.set(jObj, JSoNField.DATATYPE.name() + "@" + sensorId, value);
        //        else
        //            JSoNHelper.set(jObj, JSoNField.DATATYPE, value);
        //    }

        fun fromJSONString(value: String?): DataType? {
            if (value == null) return null
            for (type in DataType.values())
                if (type.name.equals(value, ignoreCase = true)) return type
            return null
        }
    }

}
