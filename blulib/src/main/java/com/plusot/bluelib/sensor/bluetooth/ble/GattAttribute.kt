package com.plusot.bluelib.sensor.bluetooth.ble

import android.annotation.TargetApi
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.os.Build
import com.google.gson.GsonBuilder
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey
import com.plusot.bluelib.sensor.bluetooth.util.BTHelper
import com.plusot.bluelib.sensor.bluetooth.util.getUInt16
import com.plusot.bluelib.sensor.bluetooth.util.getUInt32
import com.plusot.bluelib.sensor.bluetooth.util.getUInt8
import com.plusot.bluelib.sensor.data.CommandType
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.sensor.data.PTSConnectPacketType
import com.plusot.bluelib.util.*
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*

enum class GattAttribute( val type:GattAttributeType, sUUID:String,  val dataTypes:Array<DataType>? = null) {
    ALERT_NOTIFICATION_SERVICE(GattAttributeType.SERVICE, "00001811-0000-1000-8000-00805f9b34fb"),
    AEROBIC_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a7e-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_HEART_RATE_LOWER_LIMIT),
    AEROBIC_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a84-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_HEART_RATE_UPPER_LIMIT),
    AEROBIC_THRESHOLD(GattAttributeType.CHARACTERISTIC, "00002a7f-0000-1000-8000-00805f9b34fb", DataType.AEROBIC_THRESHOLD),
    AGE(GattAttributeType.CHARACTERISTIC, "00002a80-0000-1000-8000-00805f9b34fb", DataType.AGE),
    AGGREGATE(GattAttributeType.CHARACTERISTIC, "00002a5a-0000-1000-8000-00805f9b34fb", DataType.AGGREGATE),
    ANALOG(GattAttributeType.CHARACTERISTIC, "00002a58-0000-1000-8000-00805f9b34fb", DataType.ANALOG),
    ALERT_CATEGORY_ID(GattAttributeType.CHARACTERISTIC, "00002a43-0000-1000-8000-00805f9b34fb", DataType.ALERT_CATEGORY_ID),
    ALERT_CATEGORY_ID_BIT_MASK(GattAttributeType.CHARACTERISTIC, "00002a42-0000-1000-8000-00805f9b34fb", DataType.ALERT_CATEGORY_ID_BIT_MASK),
    ALERT_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a06-0000-1000-8000-00805f9b34fb", DataType.ALERT_LEVEL),
    ALERT_NOTIFICATION_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a44-0000-1000-8000-00805f9b34fb", DataType.ALERT_NOTIFICATION_CONTROL_POINT),
    ALERT_STATUS(GattAttributeType.CHARACTERISTIC, "00002a3f-0000-1000-8000-00805f9b34fb", DataType.ALERT_STATUS),
    ANAEROBIC_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a81-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_HEART_RATE_LOWER_LIMIT),
    ANAEROBIC_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a82-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_HEART_RATE_UPPER_LIMIT),
    ANAEROBIC_THRESHOLD(GattAttributeType.CHARACTERISTIC, "00002a83-0000-1000-8000-00805f9b34fb", DataType.ANAEROBIC_THRESHOLD),
    APPARENT_WIND_DIRECTION_(GattAttributeType.CHARACTERISTIC, "00002a73-0000-1000-8000-00805f9b34fb", DataType.APPARENT_WIND_DIRECTION_) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val value = characteristic.getUInt16(0)
            return if (value != 0) Data().add(gatt.device.address, DataType.WIND_DIRECTION, 0.01 * value) else null
        }
    },
    APPARENT_WIND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a72-0000-1000-8000-00805f9b34fb", DataType.APPARENT_WIND_SPEED) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val value = characteristic.getUInt16(0)
            return if (value != 0) Data().add(gatt.device.address, DataType.WIND_SPEED, 0.01 * value) else null
        }
    },
    APPEARANCE(GattAttributeType.CHARACTERISTIC, "00002a01-0000-1000-8000-00805f9b34fb", arrayOf<DataType>(DataType.APPEARANCE)) {
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val value = characteristic.getUInt16(0)
            return if (value != 0) Data().add(gatt.device.address, DataType.APPEARANCE, BTHelper.getAppearance(value)) else null
        }
    },
    AUTOMATION_IO_SERVICE(GattAttributeType.SERVICE, "00001815-0000-1000-8000-00805f9b34fb"),
    BAROMETRIC_PRESSURE_TREND(GattAttributeType.CHARACTERISTIC, "00002aa3-0000-1000-8000-00805f9b34fb", DataType.BAROMETRIC_PRESSURE_TREND),
    BATTERY_SERVICE(GattAttributeType.SERVICE, "0000180f-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    BATTERY_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a19-0000-1000-8000-00805f9b34fb") {

        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val batteryLevel = characteristic.getUInt8(0)
            return Data(gatt.device.address)
                .add(DataType.GATT_ATTRIBUTE_READ, this.ordinal)
                .add(DataType.BATTERY_LEVEL, 0.01 * batteryLevel)
        }
    },
    BATTERY_LEVEL2(GattAttributeType.CHARACTERISTIC, "00002a1b-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            //            byte[] bytes = characteristic.getValue();
            //            int batteryLevel;
            //            int batteryState = 0;
            //            if (bytes.length >= 2) {
            //                batteryLevel = characteristic.getUInt8(0);
            //                batteryState = characteristic.getUInt8(1);
            //            } else {
            val batteryLevel = characteristic.getUInt8(0)
            //            }
            //LLog.INSTANCE.i("Battery level = " + StringUtil.toHexString(bytes) + ", " + batteryLevel + ", " + batteryState);
            return Data().add(gatt.device.address, DataType.BATTERY_LEVEL, 0.01 * batteryLevel.toDouble())
        }
    },
    BATTERY_STATE(GattAttributeType.CHARACTERISTIC, "00002a1a-0000-1000-8000-00805f9b34fb", arrayOf<DataType>()) {

        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val bytes = characteristic.value
            val state = characteristic.getUInt8(0)

            LLog.i("Battery state = " + StringUtil.toHexString(bytes) + " = " + state)

            return null
        }
    },
    BLOOD_PRESSURE_SERVICE(GattAttributeType.SERVICE, "00001810-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    BLOOD_PRESSURE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a49-0000-1000-8000-00805f9b34fb", DataType.BLOOD_PRESSURE_FEATURE) {

        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val flags = characteristic.getUInt16(0)
            if (flags and 0x1 == 0x1) LLog.i("Movement detection feature supported")
            if (flags and 0x2 == 0x2) LLog.i("Cuff fit detection feature supported")
            if (flags and 0x4 == 0x4) LLog.i("Irregular pulse detection feature supported")
            if (flags and 0x8 == 0x8) LLog.i("Pulse rate range detection feature supported")
            if (flags and 0x10 == 0x10) LLog.i("Measurement position detection feature supported")
            if (flags and 0x20 == 0x20) LLog.i("Multiple bonds supported")
            LLog.i("Blood pressure feature: " + StringUtil.toHexString(characteristic.value))
            return null
        }
    },
    BLOOD_PRESSURE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a35-0000-1000-8000-00805f9b34fb",
        arrayOf<DataType>(DataType.HEART_RATE, DataType.BLOOD_PRESSURE_DIASTOLIC, DataType.BLOOD_PRESSURE_SYSTOLIC, DataType.BLOOD_PRESSURE_MEAN)
    ) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val bytes = characteristic.value
            val flags = characteristic.getUInt8(0)
            var systolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 1).toDouble()
            var diastolic = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 3).toDouble()
            var mean = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, 5).toDouble()
            if (flags and 0x1 == 0x0)
            {
                // measurement in mmHg. Convert to kPa
                systolic *= 133.322387415 // /= 0.00750061561303;
                diastolic *= 133.322387415
                mean *= 133.322387415
            }
            val data = Data()
                .add(gatt.device.address, DataType.BLOOD_PRESSURE_SYSTOLIC, systolic)
                .add(gatt.device.address, DataType.BLOOD_PRESSURE_DIASTOLIC, diastolic)
                .add(gatt.device.address, DataType.BLOOD_PRESSURE_MEAN, mean)
            var index = 7
            if (flags and 0x2 == 0x2)
            {
                val year = characteristic.getUInt16(index)
                index += 2
                val month = characteristic.getUInt8(index++)
                val day = characteristic.getUInt8(index++)
                val hour = characteristic.getUInt8(index++)
                val minutes = characteristic.getUInt8(index++)
                val seconds = characteristic.getUInt8(index++)
                val cal = Calendar.getInstance()
                cal.set(year, month, day, hour, minutes, seconds)
                LLog.i("Time stamp present: " + TimeUtil.formatTime(cal.timeInMillis))
            }
            if (flags and 0x4 == 0x4)
            {
                val pulseRate = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, index).toDouble()
                LLog.i("Pulse rate present: " + Format.format(pulseRate, 1, Locale.US))
                index += 2
                data.add(gatt.device.address, DataType.HEART_RATE, pulseRate)

            }
            if (flags and 0x8 == 0x8)
            {
                LLog.i("User ID present:" + characteristic.getUInt8(index++))
            }
            if (flags and 0x10 == 0x10)
            {
                LLog.i("Measurement status present")
                val status = characteristic.getUInt8(index /*index++*/)
                if (Numbers.checkBit(status, 0x1)) LLog.i("Body movement detected")
                if (Numbers.checkBit(status, 0x2)) LLog.i("Cuff too loose")
                if (Numbers.checkBit(status, 0x4)) LLog.i("Irregular pulse")
                val pulseRateStatus = Numbers.getBits(status, 3, 2)
                when (pulseRateStatus) {
                    0 -> {
                        LLog.i("Pulse rate within range")
                        LLog.i("Pulse rate exceeds upper limit")
                        LLog.i("Pulse rate exceeds lower limit")
                        LLog.i("Pulse rate future use")
                    }
                    1 -> {
                        LLog.i("Pulse rate exceeds upper limit")
                        LLog.i("Pulse rate exceeds lower limit")
                        LLog.i("Pulse rate future use")
                    }
                    2 -> {
                        LLog.i("Pulse rate exceeds lower limit")
                        LLog.i("Pulse rate future use")
                    }
                    3 -> LLog.i("Pulse rate future use")
                }
                if (Numbers.checkBit(status, 0x20)) LLog.i("Improper measurement position")
            }
            LLog.i("Blood pressure measurement: " + StringUtil.toHexString(bytes))
            return data

        }
    },

    BLUNO_SERVICE(GattAttributeType.SERVICE, "0000dfb0-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    BLUNO_SERIAL_PORT(GattAttributeType.CHARACTERISTIC, "0000dfb1-0000-1000-8000-00805f9b34fb", arrayOf<DataType>(
        DataType.INFO, DataType.TIME_ACTIVE, DataType.VOLTAGE
    )) {
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.value
            if (value.size == 1 && value[0] == 'c'.toByte()) {
                LLog.d("Single ${value[0].toChar()} received")
                return null
            }
            val readable = StringUtil.toReadableString(value)
                .replace("[0D][0A]", "")
                .trimEnd('\n','\r')
            LLog.d("Bluno received: $readable")

            val data = Data(gatt.device.address) //.add(DataType.INFO, readable)
            val blunoObj = gson.fromJson<BlunoObj>(readable)
            blunoObj?.alive?.let { data.add(DataType.TIME_ACTIVE, it) }
            blunoObj?.info?.let { data.add(DataType.INFO, it) }
            blunoObj?.voltage?.let { data.add(DataType.VOLTAGE, it / 1023.0) }

            return data
        }

        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? {
            val inits = mutableListOf<Tuple<ByteArray, String>>()
            when (cmdType) {
                CommandType.INIT -> {
                    inits.add(Tuple(("hello\r\n").toByteArray(), "Attention"))
                    return inits
                }
                CommandType.START -> {
                    inits.add(Tuple(("start\r\n").toByteArray(), "Start"))
                    return inits
                }
                CommandType.STOP -> { inits.add(Tuple(("stop\r\n").toByteArray(), "Stop"))
                    return inits
                }
                else -> {}
            }
            return null
        }

        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {

            val info = data.getString(gatt?.device?.address, DataType.INFO) + "\r\n"
            return info.toByteArray()

        }

        override val isSupported:Boolean
            get() = true
    },
    BLUNO_COMMAND(GattAttributeType.CHARACTERISTIC, "0000dfb2-0000-1000-8000-00805f9b34fb") {
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) return null
            val bytes = characteristic.value
            LLog.d("Bluno command received: " + bytes.toReadable())
            return null
        }

        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? {
            val inits = mutableListOf<Tuple<ByteArray, String>>()
            return if (cmdType == CommandType.INIT) {
                inits.add(Tuple("AT+PASSWORD=DFRobot\r\n".toByteArray(), "Password"))
                inits.add(Tuple("AT+UART=115200\r\n".toByteArray(), "Set baudrate"))
                inits.add(Tuple("AT+BLUNODEBUG=ON\r\n".toByteArray(), "Bluno Debug on"))
                inits.add(Tuple("AT+USBDEBUG=OFF\r\n".toByteArray(), "USB Debug on"))
                /*
                AT+FSM=FSM_TRANS_USB_COM_BLE
                AT+ROLE=ROLE_CENTRAL
                AT+MIN_INTERVAL=20
                AT+MAX_INTERVAL=40
                AT+UART=115200
                AT+BIND=0x000000000000
                AT+CMODE=ANYONE
                AT+BLUNODEBUG=ON
                AT+USBDEBUG=OFF
                AT+TXPOWER=000
                AT+IBEACONS=ON
                AT+PASSWORD=DFRobot
                 */
                inits
            } else
                null
        }
        override val isSupported:Boolean
            get() = true
    },

    BODY_COMPOSITION_SERVICE(GattAttributeType.SERVICE, "0000181B-0000-1000-8000-00805f9b34fb"),
    BODY_COMPOSITION_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a9b-0000-1000-8000-00805f9b34fb", DataType.BODY_COMPOSITION_FEATURE),
    BODY_COMPOSITION_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a9c-0000-1000-8000-00805f9b34fb", DataType.BODY_COMPOSITION_MEASUREMENT),
    BODY_SENSOR_LOCATION(GattAttributeType.CHARACTERISTIC, "00002a38-0000-1000-8000-00805f9b34fb", DataType.BODY_SENSOR_LOCATION),
    BOND_MANAGEMENT(GattAttributeType.SERVICE, "0000181E-0000-1000-8000-00805f9b34fb"),
    BOND_MANAGEMENT_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002aa4-0000-1000-8000-00805f9b34fb", DataType.BOND_MANAGEMENT_CONTROL_POINT),
    BOND_MANAGEMENT_FEATURE(GattAttributeType.CHARACTERISTIC, "00002aa5-0000-1000-8000-00805f9b34fb", DataType.BOND_MANAGEMENT_FEATURE),
    BOOT_KEYBOARD_INPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a22-0000-1000-8000-00805f9b34fb", DataType.BOOT_KEYBOARD_INPUT_REPORT),
    BOOT_KEYBOARD_OUTPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a32-0000-1000-8000-00805f9b34fb", DataType.BOOT_KEYBOARD_OUTPUT_REPORT),
    BOOT_MOUSE_INPUT_REPORT(GattAttributeType.CHARACTERISTIC, "00002a33-0000-1000-8000-00805f9b34fb", DataType.BOOT_MOUSE_INPUT_REPORT),
    CENTRAL_ADDRESS_RESOLUTION(GattAttributeType.CHARACTERISTIC, "00002aa6-0000-1000-8000-00805f9b34fb", DataType.CENTRAL_ADDRESS_RESOLUTION),
    CGM_FEATURE(GattAttributeType.CHARACTERISTIC, "00002aa8-0000-1000-8000-00805f9b34fb", DataType.CGM_FEATURE),
    CGM_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002aa7-0000-1000-8000-00805f9b34fb", DataType.CGM_MEASUREMENT),
    CGM_SESSION_RUN_TIME(GattAttributeType.CHARACTERISTIC, "00002aab-0000-1000-8000-00805f9b34fb", DataType.CGM_SESSION_RUN_TIME),
    CGM_SESSION_START_TIME(GattAttributeType.CHARACTERISTIC, "00002aaa-0000-1000-8000-00805f9b34fb", DataType.CGM_SESSION_START_TIME),
    CGM_SPECIFIC_OPS_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002aac-0000-1000-8000-00805f9b34fb", DataType.CGM_SPECIFIC_OPS_CONTROL_POINT),
    CGM_STATUS(GattAttributeType.CHARACTERISTIC, "00002aa9-0000-1000-8000-00805f9b34fb", DataType.CGM_STATUS),
    CHARACTERISTIC_VALUE_CHANGED(GattAttributeType.CHARACTERISTIC, "00002a7d-0000-1000-8000-00805f9b34fb", DataType.DESCRIPTOR_VALUE_CHANGED),
    CHARACTERISTIC_EXTENDED_PROPERTIES(GattAttributeType.DESCRIPTOR, "00002900-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_USER_DESCRIPTION(GattAttributeType.DESCRIPTOR, "00002901-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_PRESENTATION_FORMAT(GattAttributeType.DESCRIPTOR, "00002904-0000-1000-8000-00805f9b34fb"),
    CHARACTERISTIC_AGGREGATE_FORMAT(GattAttributeType.DESCRIPTOR, "00002905-0000-1000-8000-00805f9b34fb"),
    CLIENT_CHARACTERISTIC_CONFIGURATION(GattAttributeType.DESCRIPTOR, "00002902-0000-1000-8000-00805f9b34fb"),
    CONTINUOUS_GLUCOSE_SERVICE(GattAttributeType.SERVICE, "0000181F-0000-1000-8000-00805f9b34fb"),
    CSC_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a5c-0000-1000-8000-00805f9b34fb", DataType.CSC_FEATURE),
    CSC_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a5b-0000-1000-8000-00805f9b34fb",
        arrayOf<DataType>(DataType.WHEEL_REVOLUTIONS, DataType.SPEED, DataType.DISTANCE)//DataType.WHEEL_CIRCUMFERENCE
    ) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            var refDataItem:RefData? = refData[gatt.device.address]
            if (refDataItem == null || refDataItem !is WheelDataItem)
            {
                refDataItem = WheelDataItem()
                refData[gatt.device.address] = refDataItem
            }
            val now = System.currentTimeMillis()
            val wdi = refDataItem as WheelDataItem? ?: return null
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000)
            {
                wdi.reset()
            }
            wdi.prevTimeCalled = now

            if (dataTypes == null || dataTypes.size == 0)
            {
                LLog.e("No datatypes defined for " + toString())
                return null
            }
            val flag = characteristic.getUInt8(0)
            var hasWheelRevolutions = false
            var hasCrankRevolutions = false
            if (flag and 0x01 != 0) hasWheelRevolutions = true
            if (flag and 0x02 != 0) hasCrankRevolutions = true
            var index = 1
            val data = Data()
            if (hasWheelRevolutions)
            {
                val wheelRevolutions = characteristic.getUInt32(index)
                index += 4
                val wheelEventTime = characteristic.getUInt16(index)
                index += 2
                if (wdi.prevWheelRevolutions != -1 && wdi.prevWheelEventTime != wheelEventTime)
                {
                    val divRevolutions = ((UINT32_LIMIT + wheelRevolutions - wdi.prevWheelRevolutions) % UINT32_LIMIT).toInt()
                    val divTime = 1.0 / 1024 * ((UINT16_LIMIT + wheelEventTime - wdi.prevWheelEventTime) % UINT16_LIMIT)
                    val wheelCircumference = PreferenceKey.WHEEL_CIRCUMFERENCE.double

                    wdi.wheelRpm *= 0.8
                    wdi.wheelRpm += 0.2 * 60.0 * divRevolutions.toDouble() / divTime

                    //                    if (BlueLib.isShowNew()) {
                    //                    if (wdi.distance == -1) {
                    //                        wdi.distance = BlueLib.getDouble(gatt.getDevice().getAddress(), DataType.DISTANCE);
                    //                        if (Double.isNaN(wdi.distance)) wdi.distance = 0;
                    //                    }
                    wdi.distance += wheelCircumference * divRevolutions // + wdi.dummyDistance;
                    //                        wdi.dummyDistance = 0;
                    data.add(gatt.device.address, DataType.DISTANCE, wdi.distance)
                    //                    } else {
                    //                        wdi.dummyDistance += wheelCircumference * divRevolutions;
                    //                    }

                    data.add(gatt.device.address, DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm)
                    data.add(gatt.device.address, DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60)
                    //deviceData.add(gatt.getDevice().getAddress(), DataType.WHEEL_CIRCUMFERENCE, wheelCircumference);

                }
                else if (wdi.prevWheelEventTime == wheelEventTime && wdi.prevWheelRevolutions == wheelRevolutions)
                {
                    data.add(gatt.device.address, DataType.WHEEL_REVOLUTIONS, 0.0)
                    data.add(gatt.device.address, DataType.SPEED, 0.0)
                }
                wdi.prevWheelEventTime = wheelEventTime
                wdi.prevWheelRevolutions = wheelRevolutions
            }
            if (hasCrankRevolutions)
            {
                val crankRevolutions = characteristic.getUInt16(index)
                index += 2
                val crankEventTime = characteristic.getUInt16(index)
                //			Log.d("Received crank revolutions: " + crankRevolutions + ", " + Format.format(crankEventTime, 3));
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime)
                {
                    val divRevolutions = (UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT
                    val divTime = 1.0 / 1024 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT)
                    wdi.crankRpm *= 0.8
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions.toDouble() / divTime
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.device.address, DataType.CADENCE, wdi.crankRpm)
                }
                else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions)
                {
                    data.add(gatt.device.address, DataType.CADENCE, 0.0)
                }
                wdi.prevCrankEventTime = crankEventTime
                wdi.prevCrankRevolutions = crankRevolutions

            }
            return data
        }
    },
    CURRENT_TIME_SERVICE(GattAttributeType.SERVICE, "00001805-0000-1000-8000-00805f9b34fb"),
    CURRENT_TIME(GattAttributeType.CHARACTERISTIC, "00002a2b-0000-1000-8000-00805f9b34fb", DataType.TIME_CLOCK),
    CYCLING_SPEED_AND_CADENCE_SERVICE(GattAttributeType.SERVICE, "00001816-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    CYCLING_POWER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a66-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_CONTROL_POINT) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            var refDataItem:RefData? = refData[gatt.device.address + "_CONTROL"]
            var isNew = false
            if (refDataItem == null || refDataItem !is WheelDataItem) {
                isNew = true
                refDataItem = WheelDataItem()
                refData[gatt.device.address + "_CONTROL"] = refDataItem
            }
            if (isNew) {
                LLog.i("Power control point deviceData")
            }
            return null
        }
    },
    CYCLING_POWER_SERVICE(GattAttributeType.SERVICE, "00001818-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    CYCLING_POWER_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a65-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_FEATURE) {
        override val isSupported:Boolean
            get() = false
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            var refDataItem: RefData? = refData[gatt.device.address + "_FEATURE"]
            var isNew = false
            if (refDataItem == null || refDataItem !is WheelDataItem)
            {
                isNew = true
                refDataItem = WheelDataItem()
                refData[gatt.device.address + "_FEATURE"] = refDataItem
            }
            if (isNew)
            {
                val feature = characteristic.getUInt32(0)
                if (feature and 0x1 == 0x1) LLog.i("Pedal power balance supported")
                if (feature and 0x2 == 0x2) LLog.i("Accumulated torque supported")
                if (feature and 0x4 == 0x4) LLog.i("Wheel revolution deviceData supported")
                if (feature and 0x8 == 0x8) LLog.i("Crank revolution deviceData supported")
                if (feature and 0x10 == 0x10) LLog.i("Extreme magnitudes supported")
                if (feature and 0x20 == 0x20) LLog.i("Extreme angles supported")
                if (feature and 0x40 == 0x40) LLog.i("Top and bottom dead spot angles supported")
                if (feature and 0x80 == 0x80) LLog.i("Accumulated energy supported")
                if (feature and 0x100 == 0x100) LLog.i("Offset indication indicator supported")
                if (feature and 0x200 == 0x200) LLog.i("Offset indication supported")
                if (feature and 0x400 == 0x400) LLog.i("Measurement charteristic masking supported")
                if (feature and 0x800 == 0x800) LLog.i("Multiple sensor locations supported")
                if (feature and 0x1000 == 0x1000) LLog.i("Crank length adjustment supported")
                if (feature and 0x2000 == 0x2000) LLog.i("Chain length adjustment supported")
                if (feature and 0x4000 == 0x4000) LLog.i("Chain weight adjustment supported")
                if (feature and 0x8000 == 0x8000) LLog.i("Span length adjustment supported")
                if (feature and 0x10000 == 0x10000) LLog.i("Sensor measurement context")
                if (feature and 0x20000 == 0x20000) LLog.i("Instantaneous measurement direction supported")
                if (feature and 0x40000 == 0x40000) LLog.i("Factory calibration supported")
                if (feature and 0x80000 == 0x80000) LLog.i("Enhanced offset compensation supported")
                if (feature and 0x300000 == 0x100000) LLog.i("Not for use in distributed system")
                if (feature and 0x300000 == 0x200000) LLog.i("Can be used in distributed system")
                if (feature and 0x300000 == 0x300000) LLog.i("RFU")
                if (feature and 0x400000 == 0x400000) LLog.i("Future use")
            }
            return null
        }
    },
    CYCLING_POWER_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a63-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_MEASUREMENT) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            var refDataItem:RefData? = refData[gatt.device.address]
            var isNew = false
            if (refDataItem == null || refDataItem !is WheelDataItem)
            {
                isNew = true
                refDataItem = WheelDataItem()
                refData[gatt.device.address] = refDataItem
            }
            val now = System.currentTimeMillis()
            val wdi = refDataItem as WheelDataItem? ?: return null
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000) {
                wdi.reset()
            }
            wdi.prevTimeCalled = now

            var offset = 0
            val flags = characteristic.getUInt16(offset)
            offset += 2

            val power = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
            offset += 2
            wdi.powerAvg = wdi.powerAvg * 0.9 + power * 0.1

            val data = Data()
                .add(gatt.device.address, DataType.POWER_INSTANT, java.lang.Double.valueOf(power.toDouble()))
                .add(gatt.device.address, DataType.POWER, wdi.powerAvg)

            if (flags and 0x1 == 0x1)
            { // bit 0
                if (isNew) LLog.i("Pedal power balance present")
                val value = characteristic.getUInt8(offset)
                offset += 1
                var valueD = Math.min(value / 200.0, 1.0)
                if (flags and 0x2 == 0x2)
                { // bit 1
                    if (isNew) LLog.i("Pedal power balance reference = left")
                    valueD = 1.0 - valueD
                }
                data.add(gatt.device.address, DataType.POWER_PEDAL, valueD)
            }
            if (flags and 0x4 == 0x4)
            { // bit 2
                if (isNew) LLog.i("Accumulated torque present")
                val value = characteristic.getUInt16(offset)
                offset += 2
                val valueD = value / 32.0
                if (flags and 0x8 == 0x8)
                { // bit 3
                    if (isNew) LLog.i("Accumulated torque source = crank")
                    data.add(gatt.device.address, DataType.CRANK_TORQUE, valueD)
                }
                else
                {
                    data.add(gatt.device.address, DataType.WHEEL_TORQUE, valueD)
                }
            }
            if (flags and 0x10 == 0x10)
            { // bit 4
                if (isNew) LLog.i("Wheel revolution deviceData present")
                val wheelRevolutions = characteristic.getUInt32(offset)
                offset += 4
                val wheelEventTime = characteristic.getUInt16(offset) // / 2048.0;
                offset += 2
                //deviceData.add(gatt.getDevice().getAddress(), DataType.WHEEL_REVOLUTIONS, valueD);
                val wheelCircumference = PreferenceKey.WHEEL_CIRCUMFERENCE.double
                if (wdi.prevWheelRevolutions != -1 && wdi.prevWheelEventTime != wheelEventTime)
                {
                    val divRevolutions = ((UINT32_LIMIT + wheelRevolutions - wdi.prevWheelRevolutions) % UINT32_LIMIT).toInt()
                    val divTime = (UINT16_LIMIT + wheelEventTime - wdi.prevWheelEventTime) % UINT16_LIMIT / 2048.0

                    wdi.wheelRpm *= 0.8
                    wdi.wheelRpm += 0.2 * 60.0 * divRevolutions.toDouble() / divTime

                    //                    if (BlueLib.isShowNew()) {
                    //                        if (wdi.distance == -1) {
                    //                            wdi.distance = BlueLib.getDouble(gatt.getDevice().getAddress(), DataType.DISTANCE);
                    //                            if (Double.isNaN(wdi.distance)) wdi.distance = 0;
                    //                        }
                    wdi.distance += wheelCircumference * divRevolutions // + wdi.dummyDistance;
                    //                        wdi.dummyDistance = 0;
                    data.add(gatt.device.address, DataType.DISTANCE, wdi.distance)
                    //                    } else {
                    //                        wdi.dummyDistance += wheelCircumference * divRevolutions;
                    //                    }

                    data.add(gatt.device.address, DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm)
                    data.add(gatt.device.address, DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60)
                    //deviceData.add(gatt.getDevice().getAddress(), DataType.WHEEL_CIRCUMFERENCE, wheelCircumference);

                }
                else if (wdi.prevWheelEventTime == wheelEventTime && wdi.prevWheelRevolutions == wheelRevolutions)
                {
                    wdi.wheelRpm *= 0.8
                    data.add(gatt.device.address, DataType.WHEEL_REVOLUTIONS, wdi.wheelRpm)
                    data.add(gatt.device.address, DataType.SPEED, wheelCircumference * wdi.wheelRpm / 60)
                }
                wdi.prevWheelEventTime = wheelEventTime
                wdi.prevWheelRevolutions = wheelRevolutions
            }
            if (flags and 0x20 == 0x20)
            { // bit 5
                if (isNew) LLog.i("Crank revolution deviceData present")
                val crankRevolutions = characteristic.getUInt16(offset)
                offset += 2
                val crankEventTime = characteristic.getUInt16(offset)
                offset += 2
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime)
                {
                    val divRevolutions = (UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT
                    val divTime = 1.0 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT) / 1024.0
                    wdi.crankRpm *= 0.8
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions.toDouble() / divTime
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.device.address, DataType.CADENCE, wdi.crankRpm)
                }
                else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions)
                {
                    wdi.crankRpm *= 0.8
                    data.add(gatt.device.address, DataType.CADENCE, wdi.crankRpm)
                }
                wdi.prevCrankEventTime = crankEventTime
                wdi.prevCrankRevolutions = crankRevolutions
            }
            if (flags and 0x40 == 0x40)
            { // bit 6
                val maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                val minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                if (isNew) LLog.i("Extreme force magnitudes present: max = $maxForce N, min = $minForce N")
            }
            if (flags and 0x80 == 0x80)
            { // bit 7
                val maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                val minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                if (isNew)
                    LLog.i("Extreme torque magnitudes present: max = " + Format.format(maxForce / 32.0, 2, Locale.US) + " Nm, min = " +
                            Format.format(minForce / 32.0, 2, Locale.US) + " Nm")
            }
            if (flags and 0x100 == 0x100)
            { // bit 8
                offset += 3
                if (isNew) LLog.i("Extreme angles present")
            }
            if (flags and 0x200 == 0x200)
            { // bit 9
                val value = characteristic.getUInt16(offset)
                offset += 2
                if (isNew) LLog.i("Top dead spot angle present: $value degrees")
            }
            if (flags and 0x400 == 0x400)
            { // bit 10
                val value = characteristic.getUInt16(offset)
                offset += 2
                if (isNew) LLog.i("Bottom dead spot angle present: $value degrees")
            }
            if (flags and 0x800 == 0x800)
            { // bit 11
                val value = characteristic.getUInt16(offset)
                if (wdi.energyAtStart == 0) wdi.energyAtStart = value
                data.add(gatt.device.address, DataType.ENERGY, (UINT16_LIMIT + value - wdi.energyAtStart) % UINT16_LIMIT * 1000.0)
                if (isNew) LLog.i("Accumulated energy present")
            }
            if (flags and 0x1000 == 0x1000) if (isNew) LLog.i("Offset compensation indicator") // bit 12
            if (flags and 0x2000 == 0x2000) if (isNew) LLog.i("Future use") // bit 13

            return data
        }
        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {

            val power = data.getDouble(gatt?.device?.address, DataType.POWER)
            val crankRevolutions = data.getInt(gatt?.device?.address, DataType.STEPS_TOTAL, -1)
            val wheelRevolutions = data.getInt(gatt?.device?.address, DataType.WHEEL_REVOLUTIONS, -1)
            val pedalPower = data.getDoubleNullable(gatt?.device?.address, DataType.POWER_PEDAL) ?: -1.0
            val now = System.currentTimeMillis()
            var offset = 0
            var flags = 0
            var byteArrayLength = 4
            if (pedalPower >= 0) {
                flags = flags or 0x1
                byteArrayLength++
            }
            if (wheelRevolutions >= 0) {
                flags = flags or 0x10
                byteArrayLength += 6
            }
            if (crankRevolutions >= 0) {
                flags = flags or 0x20
                byteArrayLength += 4
            }
            val value = ByteArray(byteArrayLength)

            value.setUInt16(offset, flags)
            offset += 2

            value.setSInt16(offset, power.toInt())
            offset += 2

            if (flags and 0x1 == 0x1)
            { // bit 0
                LLog.i("Pedal power balance present")
                value.setUInt8(offset,( pedalPower * 100).toInt())
                offset++

            }
            if (flags and 0x4 == 0x4)
            { // bit 2
                LLog.i("Accumulated torque present")
                offset += 2
            }
            if (flags and 0x10 == 0x10)
            { // bit 4
                LLog.i("Wheel revolution deviceData present")
                value.setUInt32(offset, wheelRevolutions)
                offset += 4
                value.setUInt16(offset,((2048.0 * now / 1000.0).toLong() and 0xFFFF).toInt())
                offset += 2
            }
            if (flags and 0x20 == 0x20)
            { // bit 5
                LLog.i("Crank revolution deviceData present")
                value.setUInt16(offset, crankRevolutions)
                offset += 2
                value.setUInt16(offset,((1024.0 * now / 1000.0).toLong() and 0xFFFF).toInt())
                offset += 2
            }
            if (flags and 0x40 == 0x40)
            { // bit 6
                //val maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                //val minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                LLog.i("Extreme force magnitudes present: max = maxForce N, min = minForce N")
            }
            if (flags and 0x80 == 0x80)
            { // bit 7
//                val maxForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
//                val minForce = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                LLog.i("Extreme torque magnitudes present: max = maxForce / 32.0  Nm, min =minForce Nm")
            }
            if (flags and 0x100 == 0x100)
            { // bit 8
                offset += 3
                LLog.i("Extreme angles present")
            }
            if (flags and 0x200 == 0x200)
            { // bit 9
//                val value = characteristic.getUInt16(offset)
                offset += 2
                LLog.i("Top dead spot angle present: value degrees")
            }
            if (flags and 0x400 == 0x400)
            { // bit 10
//                val value = characteristic.getUInt16(offset)
                offset += 2
                LLog.i("Bottom dead spot angle present: value degrees")
            }
            if (flags and 0x800 == 0x800)
            { // bit 11
//                val value = characteristic.getUInt16(offset)
//                if (wdi.energyAtStart == 0) wdi.energyAtStart = value
//                data.add(gatt.device.address, DataType.ENERGY, (UINT16_LIMIT + value - wdi.energyAtStart) % UINT16_LIMIT * 1000.0)
                LLog.i("Accumulated energy present")
            }
            if (flags and 0x1000 == 0x1000)  LLog.i("Offset compensation indicator") // bit 12
            if (flags and 0x2000 == 0x2000)  LLog.i("Future use") // bit 13
            return value
        }
    },
    CYCLING_POWER_VECTOR(GattAttributeType.CHARACTERISTIC, "00002a64-0000-1000-8000-00805f9b34fb", DataType.CYCLING_POWER_VECTOR) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            var refDataItem:RefData? = refData[gatt.device.address + "_VECTOR"]
            var isNew = false
            if (refDataItem == null || refDataItem !is WheelDataItem) {
                isNew = true
                refDataItem = WheelDataItem()
                refData[gatt.device.address + "_VECTOR"] = refDataItem
            }
            val now = System.currentTimeMillis()
            val wdi = refDataItem as WheelDataItem? ?: return null
            if (wdi.prevTimeCalled > 0 && now - wdi.prevTimeCalled > 20000) {
                wdi.reset()
            }
            wdi.prevTimeCalled = now
            var offset = 0
            val flags = characteristic.getUInt8(offset)
            offset++

            val data = Data()
            if (flags and 0x1 == 0x1)
            { // bit 0
                if (isNew) LLog.i("Crank revolution deviceData present")
                val crankRevolutions = characteristic.getUInt16(offset)
                offset += 2
                val crankEventTime = characteristic.getUInt16(offset)
                offset += 2
                if (wdi.prevCrankRevolutions != -1 && wdi.prevCrankEventTime != crankEventTime)
                {
                    val divRevolutions = (UINT16_LIMIT + crankRevolutions - wdi.prevCrankRevolutions) % UINT16_LIMIT
                    val divTime = 1.0 * ((UINT16_LIMIT + crankEventTime - wdi.prevCrankEventTime) % UINT16_LIMIT) / 1024.0
                    wdi.crankRpm *= 0.8
                    wdi.crankRpm += 0.2 * 60.0 * divRevolutions.toDouble() / divTime
                    if (wdi.crankRpm > 0 && wdi.crankRpm < 200) data.add(gatt.device.address, DataType.CADENCE, wdi.crankRpm)
                }
                else if (wdi.prevCrankEventTime == crankEventTime && wdi.prevCrankRevolutions == crankRevolutions)
                {
                    data.add(gatt.device.address, DataType.CADENCE, 0.0)
                }
                wdi.prevCrankEventTime = crankEventTime
                wdi.prevCrankRevolutions = crankRevolutions

            }
            if (flags and 0x2 == 0x2)
            { // bit 1
                val value = characteristic.getUInt16(offset)
                offset += 2
                if (isNew) LLog.i("First crank measurement angle present: $value degrees")
            }
            if (flags and 0x4 == 0x4)
            { // bit 2
                val value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                offset += 2
                if (isNew) LLog.i("Instant force magnitude array present. First value: $value N")
            }
            if (flags and 0x8 == 0x8)
            { // bit 3
                val value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, offset)
                //                offset += 2;
                if (isNew) LLog.i("Instant torque magnitude array present. First value: $value N")
            }
            if (flags and 0x10 == 0x10)
            { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Tangential component")
            }
            if (flags and 0x20 == 0x20)
            { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Radial component")
            }
            if (flags and 0x30 == 0x30)
            { // bit 4 & 5
                if (isNew) LLog.i("Instance measurement direction: Lateral component")
            }
            if (flags and 0x40 == 0x40)
            { // bit 6
                if (isNew) LLog.i("Future use")
            }

            return null
        }

    },
    DATABASE_CHANGE_INCREMENT(GattAttributeType.CHARACTERISTIC, "00002a99-0000-1000-8000-00805f9b34fb", DataType.DATABASE_CHANGE_INCREMENT),
    DATE_OF_BIRTH(GattAttributeType.CHARACTERISTIC, "00002a85-0000-1000-8000-00805f9b34fb", DataType.DATE_OF_BIRTH),
    DATE_OF_THRESHOLD_ASSESSMENT(GattAttributeType.CHARACTERISTIC, "00002a86-0000-1000-8000-00805f9b34fb", DataType.DATE_OF_THRESHOLD_ASSESSMENT),
    DATE_TIME(GattAttributeType.CHARACTERISTIC, "00002a08-0000-1000-8000-00805f9b34fb", DataType.DATE_TIME),
    DAY_DATE_TIME(GattAttributeType.CHARACTERISTIC, "00002a0a-0000-1000-8000-00805f9b34fb", DataType.DAY_DATE_TIME),
    DAY_OF_WEEK(GattAttributeType.CHARACTERISTIC, "00002a09-0000-1000-8000-00805f9b34fb", DataType.DAY_OF_WEEK),
    DEVICE_INFORMATION_SERVICE(GattAttributeType.SERVICE, "0000180a-0000-1000-8000-00805f9b34fb"),
    DEVICE_NAME(GattAttributeType.CHARACTERISTIC, "00002a00-0000-1000-8000-00805f9b34fb", DataType.DEVICE_NAME),
    DEW_POINT(GattAttributeType.CHARACTERISTIC, "00002a7b-0000-1000-8000-00805f9b34fb", DataType.DEW_POINT),
    DIGITAL(GattAttributeType.CHARACTERISTIC, "00002a56-0000-1000-8000-00805f9b34fb", DataType.DIGITAL),
    DST_OFFSET(GattAttributeType.CHARACTERISTIC, "00002a0d-0000-1000-8000-00805f9b34fb", DataType.DST_OFFSET),
    ENVIRONMENTAL_SENSING_SERVICE(GattAttributeType.SERVICE, "0000181A-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_CONFIGURATION(GattAttributeType.DESCRIPTOR, "0000290b-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_MEASUREMENT(GattAttributeType.DESCRIPTOR, "0000290c-0000-1000-8000-00805f9b34fb"),
    ENVIRONMENTAL_SENSING_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290d-0000-1000-8000-00805f9b34fb"),
    ELEVATION(GattAttributeType.CHARACTERISTIC, "00002a6c-0000-1000-8000-00805f9b34fb", DataType.ELEVATION),
    EMAIL_ADDRESS(GattAttributeType.CHARACTERISTIC, "00002a87-0000-1000-8000-00805f9b34fb", DataType.EMAIL_ADDRESS),
    ESTIMOTE_SERVICE(GattAttributeType.SERVICE, "b9403000-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_AUTHENTICATION_SERVICE(GattAttributeType.SERVICE, "b9402000-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_VERSION_SERVICE(GattAttributeType.SERVICE, "b9404000-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_UUID(GattAttributeType.CHARACTERISTIC, "b9403003-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_MAJOR(GattAttributeType.CHARACTERISTIC, "b9403001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_MINOR(GattAttributeType.CHARACTERISTIC, "b9403002-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_BATTERY(GattAttributeType.CHARACTERISTIC, "b9403041-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_TEMPERATURE(GattAttributeType.CHARACTERISTIC, "b9403021-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_POWER(GattAttributeType.CHARACTERISTIC, "b9403011-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_INTERVAL(GattAttributeType.CHARACTERISTIC, "b9403012-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_SOFTWARE_VERSION(GattAttributeType.CHARACTERISTIC, "b9404001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_HARDWARE_VERSION(GattAttributeType.CHARACTERISTIC, "b9404002-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_SEED(GattAttributeType.CHARACTERISTIC, "b9402001-f5f8-466e-aff9-25556b57fe6d"),
    ESTIMOTE_ADVERTISING_VECTOR(GattAttributeType.CHARACTERISTIC, "b9402002-f5f8-466e-aff9-25556b57fe6d"),
    EXACT_TIME_256(GattAttributeType.CHARACTERISTIC, "00002a0c-0000-1000-8000-00805f9b34fb", DataType.EXACT_TIME_256),
    EXTERNAL_REPORT_REFERENCE(GattAttributeType.DESCRIPTOR, "00002907-0000-1000-8000-00805f9b34fb"),
    FAT_BURN_HEART_RATE_LOWER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a88-0000-1000-8000-00805f9b34fb", DataType.FAT_BURN_HEART_RATE_LOWER_LIMIT),
    FAT_BURN_HEART_RATE_UPPER_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a89-0000-1000-8000-00805f9b34fb", DataType.FAT_BURN_HEART_RATE_UPPER_LIMIT),
    FIRMWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a26-0000-1000-8000-00805f9b34fb", DataType.FIRMWARE_REVISION_STRING),
    FIRST_NAME(GattAttributeType.CHARACTERISTIC, "00002a8a-0000-1000-8000-00805f9b34fb", DataType.FIRST_NAME),
    FIVE_ZONE_HEART_RATE_LIMITS(GattAttributeType.CHARACTERISTIC, "00002a8b-0000-1000-8000-00805f9b34fb", DataType.FIVE_ZONE_HEART_RATE_LIMITS),
    GENERIC_ACCESS_SERVICE(GattAttributeType.SERVICE, "00001800-0000-1000-8000-00805f9b34fb"),
    GENERIC_ATTRIBUTE_SERVICE(GattAttributeType.SERVICE, "00001801-0000-1000-8000-00805f9b34fb"),
    GENDER(GattAttributeType.CHARACTERISTIC, "00002a8c-0000-1000-8000-00805f9b34fb", DataType.GENDER),
    GLUCOSE_SERVICE(GattAttributeType.SERVICE, "00001808-0000-1000-8000-00805f9b34fb"),
    GLUCOSE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a51-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_FEATURE),
    GLUCOSE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a18-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_MEASUREMENT),
    GLUCOSE_MEASUREMENT_CONTEXT(GattAttributeType.CHARACTERISTIC, "00002a34-0000-1000-8000-00805f9b34fb", DataType.GLUCOSE_MEASUREMENT_CONTEXT),
    GUST_FACTOR(GattAttributeType.CHARACTERISTIC, "00002a74-0000-1000-8000-00805f9b34fb", DataType.GUST_FACTOR),
    HARDWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a27-0000-1000-8000-00805f9b34fb", DataType.HARDWARE_REVISION_STRING),
    HEALTH_THERMOMETER_SERVICE(GattAttributeType.SERVICE, "00001809-0000-1000-8000-00805f9b34fb"),
    HEART_RATE_SERVICE(GattAttributeType.SERVICE, "0000180d-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    HEART_RATE_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a39-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE_CONTROL_POINT),
    HEART_RATE_MAX(GattAttributeType.CHARACTERISTIC, "00002a8d-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE_MAX),
    HEART_RATE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a37-0000-1000-8000-00805f9b34fb", DataType.HEART_RATE
    ) {
        override val isSupported:Boolean
            get() = true
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val flag = characteristic.properties
            val format:Int
            if (flag and 0x01 != 0)
            {
                format = BluetoothGattCharacteristic.FORMAT_UINT16
                //Log.d("Heart rate format UINT16.");
            }
            else
            {
                format = BluetoothGattCharacteristic.FORMAT_UINT8
                //Log.d("Heart rate format UINT8.");
            }
            val heartRate = characteristic.getIntValue(format, 1)
            return Data().add(gatt.device.address, DataType.HEART_RATE, java.lang.Double.valueOf(heartRate.toDouble()))
        }

        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {
            val value = byteArrayOf(0b00000001, 0, 0, 0)
            val hr = data.getInt(gatt?.device?.address, DataType.HEART_RATE)
            value.setUInt16(1, hr)
            return value
        }
    },
    HEAT_INDEX(GattAttributeType.CHARACTERISTIC, "00002a7a-0000-1000-8000-00805f9b34fb", DataType.HEAT_INDEX),
    HEIGHT(GattAttributeType.CHARACTERISTIC, "00002a8e-0000-1000-8000-00805f9b34fb", DataType.HEIGHT),
    HID_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a4c-0000-1000-8000-00805f9b34fb", DataType.HID_CONTROL_POINT),
    HID_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a4a-0000-1000-8000-00805f9b34fb", DataType.HID_INFORMATION),
    HIP_CIRCUMFERENCE(GattAttributeType.CHARACTERISTIC, "00002a8f-0000-1000-8000-00805f9b34fb", DataType.HIP_CIRCUMFERENCE),
    HUMAN_INTERFACE_DEVICE_SERVICE(GattAttributeType.SERVICE, "00001812-0000-1000-8000-00805f9b34fb"),
    HUMIDITY(GattAttributeType.CHARACTERISTIC, "00002a6f-0000-1000-8000-00805f9b34fb", DataType.HUMIDITY),
    IEEE_CERTIFICATION(GattAttributeType.CHARACTERISTIC, "00002a2a-0000-1000-8000-00805f9b34fb", DataType.IEEE_CERTIFICATION),
    IMMEDIATE_ALERT_SERVICE(GattAttributeType.SERVICE, "00001802-0000-1000-8000-00805f9b34fb"),
    INTERMEDIATE_CUFF_PRESSURE(GattAttributeType.CHARACTERISTIC, "00002a36-0000-1000-8000-00805f9b34fb", DataType.INTERMEDIATE_CUFF_PRESSURE),
    INTERMEDIATE_TEMPERATURE(GattAttributeType.CHARACTERISTIC, "00002a1e-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE_INTERMEDIATE),
    INTERNET_PROTOCOL_SUPPORT_SERVICE(GattAttributeType.SERVICE, "00001820-0000-1000-8000-00805f9b34fb"),
    IR_RADIANCE(GattAttributeType.CHARACTERISTIC, "00002a77-0000-1000-8000-00805f9b34fb", DataType.IRRADIANCE),

    KIEN_SERVICE(GattAttributeType.SERVICE, "6e400001-b5a3-f393-e0a9-e50e24dcca9e") {
        override val isSupported:Boolean
            get() = true
    },
    KIEN_RX(GattAttributeType.CHARACTERISTIC, "6e400002-b5a3-f393-e0a9-e50e24dcca9e") {
        override val isSupported:Boolean
            get() = true

        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val bytes = characteristic.value
            LLog.d("Bytes ${bytes.toHex()}")
            return null
        }

        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {
            return data.getBytes(gatt?.device?.address, DataType.KIEN_ARRAY)
        }
    },
    KIEN_TX(GattAttributeType.CHARACTERISTIC, "6e400003-b5a3-f393-e0a9-e50e24dcca9e") {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val bytes = characteristic.value
            LLog.d("Bytes ${bytes.toHex()}")
            return null
        }

        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {
            return data.getBytes(gatt?.device?.address, DataType.KIEN_ARRAY)
        }
    },
    LANGUAGE(GattAttributeType.CHARACTERISTIC, "00002aa2-0000-1000-8000-00805f9b34fb", DataType.LANGUAGE),
    LAST_NAME(GattAttributeType.CHARACTERISTIC, "00002a90-0000-1000-8000-00805f9b34fb", DataType.LAST_NAME),
    LINK_LOSS_SERVICE(GattAttributeType.SERVICE, "00001803-0000-1000-8000-00805f9b34fb"),
    LN_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a6b-0000-1000-8000-00805f9b34fb", DataType.LN_CONTROL_POINT),
    LN_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a6a-0000-1000-8000-00805f9b34fb", DataType.LN_FEATURE),
    LOCAL_TIME_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a0f-0000-1000-8000-00805f9b34fb", DataType.LOCAL_TIME_INFORMATION),
    LOCATION_AND_NAVIGATION_SERVICE(GattAttributeType.SERVICE, "00001819-0000-1000-8000-00805f9b34fb"),
    LOCATION_AND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a67-0000-1000-8000-00805f9b34fb", DataType.LOCATION_AND_SPEED),
    MAGNETIC_DECLINATION(GattAttributeType.CHARACTERISTIC, "00002a2c-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_DECLINATION),
    MAGNETIC_FLUX_DENSITY_2D(GattAttributeType.CHARACTERISTIC, "00002aa0-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_FLUX_DENSITY_2D),
    MAGNETIC_FLUX_DENSITY_3D(GattAttributeType.CHARACTERISTIC, "00002aa1-0000-1000-8000-00805f9b34fb", DataType.MAGNETIC_FLUX_DENSITY_3D),
    MANUFACTURER_NAME_STRING(GattAttributeType.CHARACTERISTIC, "00002a29-0000-1000-8000-00805f9b34fb", DataType.MANUFACTURER_NAME_STRING),
    MAXIMUM_RECOMMENDED_HEART_RATE(GattAttributeType.CHARACTERISTIC, "00002a91-0000-1000-8000-00805f9b34fb", DataType.MAXIMUM_RECOMMENDED_HEART_RATE),
    MEASUREMENT_INTERVAL(GattAttributeType.CHARACTERISTIC, "00002a21-0000-1000-8000-00805f9b34fb", DataType.MEASUREMENT_INTERVAL),
    MODEL_NUMBER_STRING(GattAttributeType.CHARACTERISTIC, "00002a24-0000-1000-8000-00805f9b34fb", DataType.MODEL_NUMBER_STRING),
    NAVIGATION(GattAttributeType.CHARACTERISTIC, "00002a68-0000-1000-8000-00805f9b34fb", DataType.NAVIGATION),
    NEW_ALERT(GattAttributeType.CHARACTERISTIC, "00002a46-0000-1000-8000-00805f9b34fb", DataType.NEW_ALERT),
    NEXT_DST_CHANGE_SERVICE(GattAttributeType.SERVICE, "00001807-0000-1000-8000-00805f9b34fb"),
    NORDIC_AND_TAIDOC_SERVICE(GattAttributeType.SERVICE, "00001523-1212-efde-1523-785feabcd123") {
        override val isSupported:Boolean
            get() = true
    },
    NORDIC_AND_TAIDOC_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "00001524-1212-efde-1523-785feabcd123") {
        override val isSupported:Boolean
            get() = true
        private fun addCheck(bytes:ByteArray):ByteArray {
            var sum = 0
            for (i in 0 until bytes.size - 1)
            {
                sum += bytes[i].toInt()
            }
            bytes[bytes.size - 1] = (sum and 0xFF).toByte()
            return bytes
        }

        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? {
            val inits = mutableListOf<Tuple<ByteArray, String>>()
            when (cmdType) {
                CommandType.INIT ->
                    return null
                CommandType.START -> {
                    inits.add(Tuple(addCheck(byteArrayOf(0x51.toByte(), 0x43.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0xA3.toByte(), 0.toByte())), "Start measurement"))
                    return inits
                }
                CommandType.STOP -> {
                    inits.add(Tuple(addCheck(byteArrayOf(0x51.toByte(), 0x50.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0xA3.toByte(), 0.toByte())), "Stop"))
                    return inits
                }
                CommandType.RESET -> {}
            }
            return null
        }

        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            LLog.i("Nordic / Taidoc charteristic value: " + StringUtil.toHexString(characteristic.value))
            return null
        }

        override val readAfterWrite = false
    },
    NUMBER_OF_DIGITALS(GattAttributeType.DESCRIPTOR, "00002909-0000-1000-8000-00805f9b34fb"),
    PASSKEY_SERVICE(GattAttributeType.SERVICE, "00002c02-0000-1000-8000-00805f9b34fb", DataType.LEVEL) {
        override val isSupported:Boolean
            get() = true
    },
    PASSKEY_KEY(GattAttributeType.CHARACTERISTIC, "00002c03-0000-1000-8000-00805f9b34fb", DataType.KEY) {
        override val isSupported:Boolean
            get() = true
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val key = characteristic.getStringValue(0)
            SleepAndWake.runInMain{ ToastHelper.showToastShort("Key: $key") }
            try
            {
                return Data().add(gatt.device.address, DataType.KEY, Integer.valueOf(key))
            }
            catch (e:NumberFormatException) {
                return null
            }

        }
    },
    PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS(GattAttributeType.CHARACTERISTIC, "00002a04-0000-1000-8000-00805f9b34fb",
        arrayOf<DataType>(DataType.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS)
    ) {
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val minConnectionInterval = characteristic.getUInt16(0)
            val maxConnectionInterval = characteristic.getUInt16(2)
            val slaveLatency = characteristic.getUInt16(4)
            val connectionSupervisorTimeoutMulitplier = characteristic.getUInt16(6)
            val result = Stringer(", ")

            result.append(minConnectionInterval)
            result.append(maxConnectionInterval)
            result.append(slaveLatency)
            result.append("$connectionSupervisorTimeoutMulitplier ms")
            return Data().add(gatt.device.address, DataType.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS, result.toString())
        }
    },
    PERIPHERAL_PRIVACY_FLAG(GattAttributeType.CHARACTERISTIC, "00002a02-0000-1000-8000-00805f9b34fb", DataType.PERIPHERAL_PRIVACY_FLAG),
    PHONE_ALERT_STATUS_SERVICE(GattAttributeType.SERVICE, "0000180E-0000-1000-8000-00805f9b34fb"),
    PNP_ID(GattAttributeType.CHARACTERISTIC, "00002a50-0000-1000-8000-00805f9b34fb", DataType.PNP_ID),
    POLLEN_CONCENTRATION(GattAttributeType.CHARACTERISTIC, "00002a75-0000-1000-8000-00805f9b34fb", DataType.POLLEN_CONCENTRATION),
    POSITION_QUALITY(GattAttributeType.CHARACTERISTIC, "00002a69-0000-1000-8000-00805f9b34fb", DataType.POSITION_QUALITY),
    PRESSURE(GattAttributeType.CHARACTERISTIC, "00002a6d-0000-1000-8000-00805f9b34fb", DataType.PRESSURE),
    PROTOCOL_MODE(GattAttributeType.CHARACTERISTIC, "00002a4e-0000-1000-8000-00805f9b34fb", DataType.PROTOCOL_MODE),
    PTS_SERVICE1(GattAttributeType.CHARACTERISTIC, "49535343-5d82-6099-9348-7aac4d5fbc51") {
        override val isSupported: Boolean
            get() = true
    },
    PTS_SERVICE2(GattAttributeType.CHARACTERISTIC, "49535343-c9d0-cc83-a44a-6fe238d06d33") {
        override val isSupported: Boolean
            get() = true
    },
    PTS_ISSC_TRANSPARENT_SERVICE(GattAttributeType.CHARACTERISTIC, "49535343-FE7D-4AE5-8FA9-9FAFD205E455") {
        override val isSupported: Boolean
            get() = true
    },
    // Is called transmit with respect to the BLE Module in the LIT 001958 doc.
    PTS_ISSC_TRANSPARENT_RECEIVE(GattAttributeType.CHARACTERISTIC, "49535343-1E4D-4BD9-BA61-23C647249616") {
        override fun parse(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic): Data? {
            val value = characteristic.value
            LLog.d("Received on ${toString()}: ${value.toHex()} ${value.toReadable()} (${value.size})")
            return PtsDecrypt.processPacket(gatt.device.address, value)
        }

        override fun write(gatt: BluetoothGatt?, data: Data): ByteArray? {
            val value = ByteArray(8)
            if (data.hasDataType(DataType.PTS_SERIAL)) {
                val serialNumber = data.getInt(gatt?.device?.address, DataType.PTS_SERIAL)
                value.setUInt32(0, PTSConnectPacketType.serialNumberRequest.id)
                value.setUInt32(4, serialNumber)

            }
            return value
        }

        override val isSupported: Boolean
            get() = true
    },
    // Is called receive in the LIT 001958 doc.
    PTS_ISSC_TRANSPARENT_TRANSMIT(GattAttributeType.CHARACTERISTIC, "49535343-8841-43F4-A8D4-ECBE34729BB3") {
        override fun parse(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic): Data? {
            val value = characteristic.value
            LLog.d("Received on ${toString()}: ${value.toHex()} (${value.size})")
            return null
        }

        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                val array = write(null, Data().add(DataType.PTS_SERIAL))
                if (array != null)
                    inits.add(Tuple(array, "Initializer"))
                inits
            }
            else -> null
        }

        override val isSupported: Boolean
            get() = true
    },
    PTS_SERVICE1_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "49535343-026e-3a9b-954c-97daef17e26e") {
        override fun parse(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic): Data? {
            val value = characteristic.value
            LLog.d("Received on " + toString() + ": " + StringUtil.toHexString(value))
            return null
        }

        override val isSupported: Boolean
            get() = true
    },
    PTS_SERVICE2_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "49535343-aca3-481c-91ec-d85e28a60318") {
        override fun parse(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic): Data? {
            val value = characteristic.value
            LLog.d("Received on " + toString() + ": " + StringUtil.toHexString(value))
            return null
        }

        override val isSupported: Boolean
            get() = true
    },
    PTS_ISSC_UNKNOWN_CHARACTERISTIC(GattAttributeType.CHARACTERISTIC, "49535343-4c8a-39b3-2f49-511cff073b7e") {
        override fun parse(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic): Data? {
            val value = characteristic.value
            LLog.d("Received on " + toString() + ": " + StringUtil.toHexString(value))
            return null
        }

        override val isSupported: Boolean
            get() = true
    },

    RADBEACON_DEVICENAME(GattAttributeType.CHARACTERISTIC, "0000aaa2-0000-1000-8000-00805f9b34fb", DataType.RADBEACON_DEVICE_NAME),
    RAINFALL(GattAttributeType.CHARACTERISTIC, "00002a78-0000-1000-8000-00805f9b34fb", DataType.RAINFALL),
    RECONNECTION_ADDRESS(GattAttributeType.CHARACTERISTIC, "00002a03-0000-1000-8000-00805f9b34fb", DataType.RECONNECTION_ADDRESS),
    RECORD_ACCESS_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a52-0000-1000-8000-00805f9b34fb", DataType.RECORD_ACCESS_CONTROL_POINT),
    REFERENCE_TIME_UPDATE_SERVICE(GattAttributeType.SERVICE, "00001806-0000-1000-8000-00805f9b34fb"),
    REFERENCE_TIME_INFORMATION(GattAttributeType.CHARACTERISTIC, "00002a14-0000-1000-8000-00805f9b34fb", DataType.REFERENCE_TIME_INFORMATION),
    REPORT(GattAttributeType.CHARACTERISTIC, "00002a4d-0000-1000-8000-00805f9b34fb", DataType.REPORT),
    REPORT_MAP(GattAttributeType.CHARACTERISTIC, "00002a4b-0000-1000-8000-00805f9b34fb", DataType.REPORT_MAP),
    REPORT_REFERENCE(GattAttributeType.DESCRIPTOR, "00002908-0000-1000-8000-00805f9b34fb"),
    RESTING_HEART_RATE(GattAttributeType.CHARACTERISTIC, "00002a92-0000-1000-8000-00805f9b34fb", DataType.RESTING_HEART_RATE),
    RINGER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a40-0000-1000-8000-00805f9b34fb", DataType.RINGER_CONTROL_POINT),
    RINGER_SETTING(GattAttributeType.CHARACTERISTIC, "00002a41-0000-1000-8000-00805f9b34fb", DataType.RINGER_SETTING),
    RSC_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a54-0000-1000-8000-00805f9b34fb", DataType.RSC_FEATURE),
    RSC_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a53-0000-1000-8000-00805f9b34fb", DataType.RSC_MEASUREMENT),
    RUNNING_SPEED_AND_CADENCE_SERVICE(GattAttributeType.SERVICE, "00001814-0000-1000-8000-00805f9b34fb"),
    SC_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a55-0000-1000-8000-00805f9b34fb", DataType.SC_CONTROL_POINT),
    SCAN_PARAMETERS_SERVICE(GattAttributeType.SERVICE, "00001813-0000-1000-8000-00805f9b34fb"),
    SCAN_INTERVAL_WINDOW(GattAttributeType.CHARACTERISTIC, "00002a4f-0000-1000-8000-00805f9b34fb", DataType.SCAN_INTERVAL_WINDOW),
    SCAN_REFRESH(GattAttributeType.CHARACTERISTIC, "00002a31-0000-1000-8000-00805f9b34fb", DataType.SCAN_REFRESH),
    SENSOR_LOCATION(GattAttributeType.CHARACTERISTIC, "00002a5d-0000-1000-8000-00805f9b34fb", DataType.SENSOR_LOCATION) {
        override val isSupported:Boolean
            get() = true
    },
    SERIAL_NUMBER_STRING(GattAttributeType.CHARACTERISTIC, "00002a25-0000-1000-8000-00805f9b34fb", DataType.SERIAL_NUMBER_STRING),
    SERVER_CHARACTERISTIC_CONFIGURATION(GattAttributeType.DESCRIPTOR, "00002903-0000-1000-8000-00805f9b34fb"),
    SERVICE_CHANGED(GattAttributeType.CHARACTERISTIC, "00002a05-0000-1000-8000-00805f9b34fb", DataType.SERVICE_CHANGED),
    SOFTWARE_REVISION_STRING(GattAttributeType.CHARACTERISTIC, "00002a28-0000-1000-8000-00805f9b34fb", DataType.SOFTWARE_REVISION_STRING),
    SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS(GattAttributeType.CHARACTERISTIC, "00002a93-0000-1000-8000-00805f9b34fb", DataType.SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS),
    SUPPORTED_NEW_ALERT_CATEGORY(GattAttributeType.CHARACTERISTIC, "00002a47-0000-1000-8000-00805f9b34fb", DataType.SUPPORTED_NEW_ALERT_CATEGORY),
    SUPPORTED_UNREAD_ALERT_CATEGORY(GattAttributeType.CHARACTERISTIC, "00002a48-0000-1000-8000-00805f9b34fb", DataType.SUPPORTED_UNREAD_ALERT_CATEGORY),
    SYSTEM_ID(GattAttributeType.CHARACTERISTIC, "00002a23-0000-1000-8000-00805f9b34fb", DataType.SYSTEM_ID),
    TEMPERATURE(GattAttributeType.CHARACTERISTIC, "00002a6e-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE),
    TEMPERATURE_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a1c-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val bytes = characteristic.value
            val flag = characteristic.getUInt8(0)
            var celsius = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_FLOAT, 1).toDouble()
            LLog.i(StringUtil.toHexString(bytes))
            if (flag and 0x1 == 0x1)
            {
                LLog.i("Fahrenheit measurement with value: " + celsius + " = " + (celsius - 32.0f) * 5.0f / 9.0f + " Celsius")
                if (gatt.device.name.contains("BlueSC"))
                {
                    return null
                }
                else
                {
                    celsius = (celsius - 32.0f) * 5.0f / 9.0f
                }
            }
            if (flag and 0x2 == 0x2)
            {
                if (bytes.size > 5)
                    LLog.i("Timestamp present in measurement")
                else
                    return null
            }
            if (flag and 0x4 == 0x4)
            {
                LLog.i("Temperature type present in measurement")
                if (gatt.device.name.contains("BlueSC")) return null
            }
            LLog.i("Celsius = $celsius")
            return Data().add(gatt.device.address, DataType.TEMPERATURE, celsius)
        }
    },
    TEMPERATURE_TYPE(GattAttributeType.CHARACTERISTIC, "00002a1d-0000-1000-8000-00805f9b34fb", DataType.TEMPERATURE_TYPE),
    THREE_ZONE_HEART_RATE_LIMITS(GattAttributeType.CHARACTERISTIC, "00002a94-0000-1000-8000-00805f9b34fb", DataType.THREE_ZONE_HEART_RATE_LIMITS),
    TI_IRTEMPERATURE_SERVICE(GattAttributeType.SERVICE, "f000aa00-0451-4000-b000-000000000000") {
        override val isSupported:Boolean get() = true
    },
    TI_IRTEMPERATURE_DATA(GattAttributeType.CHARACTERISTIC, "f000aa01-0451-4000-b000-000000000000", arrayOf<DataType>(DataType.TEMPERATURE_IR, DataType.TEMPERATURE_OBJECT)) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val SCALE_LSB = 0.03125f
            val rawObject = characteristic.getUInt16(0)
            val rawAmbient = characteristic.getUInt16(2)
            val tObject = (0.25f * SCALE_LSB * rawObject.toFloat()).toDouble()
            val tAmbient = (0.25f * SCALE_LSB * rawAmbient.toFloat()).toDouble()
            return Data()
                .add(gatt.device.address, DataType.TEMPERATURE_OBJECT, tObject)
                .add(gatt.device.address, DataType.TEMPERATURE_IR, tAmbient)

        }
    },
    TI_IRTEMPERATURE_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa02-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_TEMPERATURE_CONFIG)
    ) {
        override val isSupported:Boolean get() = true
        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                inits.add(Tuple(byteArrayOf(1), "Initializer"))
                inits
            }
            else -> null
        }
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            return Data().add(gatt.device.address, DataType.TI_TEMPERATURE_CONFIG, value)
        }
    }, // 0: disable, 1: enable
    TI_IRTEMPERATURE_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa03-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_TEMPERATURE_PERIOD)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            val period = (10 * value).toLong()
            return Data().add(gatt.device.address, DataType.TI_TEMPERATURE_PERIOD, period)
        }
    }, // Period in tens of milliseconds

    TI_ACCELEROMETER_SERVICE(GattAttributeType.SERVICE, "f000aa10-0451-4000-b000-000000000000"),
    TI_ACCELEROMETER_DATA(GattAttributeType.CHARACTERISTIC, "f000aa11-0451-4000-b000-000000000000"),
    TI_ACCELEROMETER_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa12-0451-4000-b000-000000000000"), // 0: disable, 1: enable
    TI_ACCELEROMETER_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa13-0451-4000-b000-000000000000"), // Period in tens of milliseconds
    TI_HUMIDITY_SERVICE(GattAttributeType.SERVICE, "f000aa20-0451-4000-b000-000000000000") {
        override val isSupported:Boolean get() = true
    },
    TI_HUMIDITY_DATA(GattAttributeType.CHARACTERISTIC, "f000aa21-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.HUMIDITY, DataType.TEMPERATURE)) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val rawTemp = characteristic.getUInt16(0)
            val rawHum = characteristic.getUInt16(2)
            val temp = rawTemp.toDouble() / UINT16_LIMIT * 165 - 40
            //-- calculate relative humidity [%RH]
            val hum = rawHum.toDouble() / UINT16_LIMIT
            return Data()
                .add(gatt.device.address, DataType.HUMIDITY, hum)
                .add(gatt.device.address, DataType.TEMPERATURE, temp)
        }
    },
    TI_HUMIDITY_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa22-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_HUMIDITY_CONFIG)
    ) {
        override val isSupported:Boolean get() = true
        override fun commands(cmdType: CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                inits.add(Tuple(byteArrayOf(1), "Initializer"))
                inits
            }
            else -> null
        }
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            return Data().add(gatt.device.address, DataType.TI_HUMIDITY_CONFIG, value)
        }
    },
    TI_HUMIDITY_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa23-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_HUMIDITY_PERIOD)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            val period = (10 * value).toLong()
            return Data().add(gatt.device.address, DataType.TI_HUMIDITY_PERIOD, period)
        }
    },
    TI_MAGNETIC_SERVICE(GattAttributeType.SERVICE, "f000aa30-0451-4000-b000-000000000000"),
    TI_MAGNETIC_DATA(GattAttributeType.CHARACTERISTIC, "f000aa31-0451-4000-b000-000000000000"),
    TI_MAGNETIC_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa32-0451-4000-b000-000000000000"),
    TI_MAGNETIC_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa33-0451-4000-b000-000000000000"),
    TI_OPTICAL_SERVICE(GattAttributeType.SERVICE, "f000aa70-0451-4000-b000-000000000000") {
        override val isSupported:Boolean get() = true
    },
    TI_OPTICAL_DATA(GattAttributeType.CHARACTERISTIC, "f000aa71-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.LIGHT)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val rawLight = characteristic.getUInt16(0)
            val mantisse = rawLight and 0x0FFF
            val exponent = rawLight and 0xF000 shr 12
            val light = 0.01 * mantisse.toDouble() * Math.pow(2.0, exponent.toDouble())
            return Data().add(gatt.device.address, DataType.LIGHT, light)
        }
    },
    TI_OPTICAL_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa72-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_LIGHT_CONFIG)
    ) {
        override val isSupported:Boolean get() = true
        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                inits.add(Tuple(byteArrayOf(1), "Initializer"))
                inits
            }
            else -> null
        }
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            return Data().add(gatt.device.address, DataType.TI_LIGHT_CONFIG, value)
        }
    },
    TI_OPTICAL_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa73-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_LIGHT_PERIOD)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            val period = (10 * value).toLong()
            return Data().add(gatt.device.address, DataType.TI_LIGHT_PERIOD, period)
        }
    },
    TI_BAROMETER_SERVICE(GattAttributeType.SERVICE, "f000aa40-0451-4000-b000-000000000000") {
        override val isSupported:Boolean get() = true
    },
    TI_BAROMETER_DATA(GattAttributeType.CHARACTERISTIC, "f000aa41-0451-4000-b000-000000000000", arrayOf<DataType>(DataType.PRESSURE)) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.value
            val rawValue = Numbers.getUInt24(value, 3)
            val pres = 1.0 * rawValue
            return Data().add(gatt.device.address, DataType.PRESSURE, pres)
        }
    },
    TI_BAROMETER_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa42-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_BAROMETER_CONFIG)
    ) {
        override val isSupported:Boolean get() = true
        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                inits.add(Tuple(byteArrayOf(1), "Initializer"))
                inits
            }
            else -> null
        }
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            return Data().add(gatt.device.address, DataType.TI_BAROMETER_CONFIG, value)
        }
    },
    TI_BAROMETER_CALIBRATION(GattAttributeType.CHARACTERISTIC, "f000aa43-0451-4000-b000-000000000000") {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            LLog.e("Not implemented yet")
            return null
        }
    },
    TI_BAROMETER_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa44-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_BAROMETER_PERIOD)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            val period = (10 * value).toLong()
            return Data().add(gatt.device.address, DataType.TI_BAROMETER_PERIOD, period)
        }
    },
    TI_GYROSCOPE_SERVICE(GattAttributeType.SERVICE, "f000aa50-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_DATA(GattAttributeType.CHARACTERISTIC, "f000aa51-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa52-0451-4000-b000-000000000000"),
    TI_GYROSCOPE_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa53-0451-4000-b000-000000000000"),
    TI_IO_SERVICE(GattAttributeType.SERVICE, "f000aa64-0451-4000-b000-000000000000") {
        override val isSupported:Boolean
            get() = true
    },
    TI_IO_DATA(GattAttributeType.CHARACTERISTIC, "f000aa65-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_IO_DATA)
    ) {

        override val isSupported:Boolean
            get() = true
        //        @Override
        //        public byte[] initValue() {
        //            return new byte[]{IO_REDLED};
        //        }
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val io = characteristic.getUInt8(0)
            val service = ioservices[gatt.device.address]
            if (service != null)
            {
                when (service) {
                    IOService.LOCAL -> {}
                    IOService.REMOTE -> {
                        if (io and 0x1 > 0)
                            LLog.i("Red led ON")
                        else
                            LLog.i("Red led OFF")
                        if (io and 0x2 > 0)
                            LLog.i("Green led ON")
                        else
                            LLog.i("Green led OFF")
                        if (io and 0x4 > 0)
                            LLog.i("Buzzer ON")
                        else
                            LLog.i("Buzzer OFF")
                    }
                    IOService.TEST -> {
                        if (io and 0x1 > 0) LLog.i("IR Temperature sensor started correctly")
                        if (io and 0x2 > 0) LLog.i("Humidity sensor started correctly")
                        if (io and 0x4 > 0) LLog.i("Optical sensor started correctly")
                        if (io and 0x8 > 0) LLog.i("Pressure sensor started correctly")
                        if (io and 0x10 > 0) LLog.i("Pressure sensor started correctly")
                        if (io and 0x20 > 0)
                            LLog.i("Gyroscope and Accelerometer started correctly")
                        if (io and 0x40 > 0) LLog.i("Magnetometer started correctly")
                        if (io and 0x80 > 0) LLog.i("Unknown sensor started correctly")
                    }
                }
            }
            return Data().add(gatt.device.address, DataType.TI_IO_DATA, io)
        }
    },
    TI_IO_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa66-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_IO_CONFIG)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            val value = characteristic.getUInt8(0)
            return IOService.fromOrdinal(value)?.let {
                ioservices[gatt.device.address] = it
                Data().add(gatt.device.address, DataType.TI_IO_CONFIG, it.toString())
            } ?: return null
        }
    },
    TI_REGISTER_SERVICE(GattAttributeType.SERVICE, "f000ac00-0451-4000-b000-000000000000"),
    TI_REGISTER_DATA(GattAttributeType.CHARACTERISTIC, "f000ac01-0451-4000-b000-000000000000"),
    TI_REGISTER_ADDRESS(GattAttributeType.CHARACTERISTIC, "f000ac02-0451-4000-b000-000000000000"),
    TI_REGISTER_DEV(GattAttributeType.CHARACTERISTIC, "f000ac03-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_SERVICE(GattAttributeType.SERVICE, "f000ccc0-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR1(GattAttributeType.CHARACTERISTIC, "f000ccc1-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR2(GattAttributeType.CHARACTERISTIC, "f000ccc2-0451-4000-b000-000000000000"),
    TI_CONNECTIONCONTROL_CHAR3(GattAttributeType.CHARACTERISTIC, "f000ccc3-0451-4000-b000-000000000000"),
    TI_OAD_SERVICE(GattAttributeType.SERVICE, "f000ffc0-0451-4000-b000-000000000000"),
    TI_OAD_IMG_IDENTIFY(GattAttributeType.CHARACTERISTIC, "f000ffc1-0451-4000-b000-000000000000"),
    TI_OAD_IMG_BLOCK(GattAttributeType.CHARACTERISTIC, "f000ffc2-0451-4000-b000-000000000000"),
    TI_MOVEMENT_SERVICE(GattAttributeType.SERVICE, "f000aa80-0451-4000-b000-000000000000") {
        override val isSupported:Boolean
            get() = true
    },
    TI_MOVEMENT_DATA(GattAttributeType.CHARACTERISTIC, "f000aa81-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.ACCELERATION, DataType.MAGNETIC_FLUX_DENSITY_3D, DataType.GYROSCOPE)
    ) {
        override val isSupported:Boolean get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
            //            byte[] values = charteristic.getValue();
            var scale = 65536f / 500f
            val gyro = floatArrayOf(1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0) / scale, 1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 2) / scale, 1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 4) / scale)
            var range = accelerationRange[gatt.device.address]
            if (range == null) range = 8
            scale = (32768 / range).toFloat()
            val accel = floatArrayOf(9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 6) / scale, 9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 8) / scale, 9.81f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 10) / scale)
            scale = (32768 / 4912).toFloat()
            val magn = floatArrayOf(-1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 12) / scale, 1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 14) / scale, -1.0f * characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 16) / scale)
            return Data()
                .add(gatt.device.address, DataType.ACCELERATION, accel)
                .add(gatt.device.address, DataType.MAGNETIC_FLUX_DENSITY_3D, magn)
                .add(gatt.device.address, DataType.GYROSCOPE, gyro)
        }
    },
    TI_MOVEMENT_CONFIG(GattAttributeType.CHARACTERISTIC, "f000aa82-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_ON, DataType.TI_MOVEMENT_CONFIG_GYRO_ON, DataType.TI_MOVEMENT_CONFIG_MAGNETOMETER_ON, DataType.TI_MOVEMENT_CONFIG_WAKE_ON_MOTION, DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE)
    ) {
        override val isSupported:Boolean
            get() = true
        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                inits.add(Tuple(byteArrayOf((GYRO_ENABLE or ACCEL_ENABLE or MAGNET_ENABLE or WAKE_ENABLE).toByte(), ACCEL_RANGE_2G.toByte()), "Initializer"))
                inits
            }
            else -> null
        }

        override fun characteristicToReadAfter():GattAttribute? {
            return TI_MOVEMENT_DATA
        }

        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val gyro = intArrayOf(getBitValue(characteristic, 0, 1), getBitValue(characteristic, 1, 1), getBitValue(characteristic, 2, 1))

            val accel = intArrayOf(getBitValue(characteristic, 3, 1), getBitValue(characteristic, 4, 1), getBitValue(characteristic, 5, 1))
            val magnet = getBitValue(characteristic, 6, 1)
            val wake = getBitValue(characteristic, 7, 1)
            val range:Int = Numbers.pow(2, 1 + getBitValue(characteristic, 8, 2))
            accelerationRange[gatt.device.address] = range
            return Data()
                .add(gatt.device.address, DataType.TI_MOVEMENT_CONFIG_GYRO_ON, gyro)
                .add(gatt.device.address, DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_ON, accel)
                .add(gatt.device.address, DataType.TI_MOVEMENT_CONFIG_MAGNETOMETER_ON, magnet)
                .add(gatt.device.address, DataType.TI_MOVEMENT_CONFIG_WAKE_ON_MOTION, wake)
                .add(gatt.device.address, DataType.TI_MOVEMENT_CONFIG_ACCELEROMETER_RANGE, range * 9.81)
        }
    },
    TI_MOVEMENT_PERIOD(GattAttributeType.CHARACTERISTIC, "f000aa83-0451-4000-b000-000000000000",
        arrayOf<DataType>(DataType.TI_MOVEMENT_PERIOD)
    ) {

        override val isSupported:Boolean
            get() = true
        override fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? = when (cmdType) {
            CommandType.INIT -> {
                val inits = ArrayList<Tuple<ByteArray, String>>()
                //minimum =   10 ms = 0x0A
                //maximum = 2550 ms = 0xFF
                inits.add(Tuple(byteArrayOf(0xB.toByte()), "Initializer"))
                inits
            }
            else -> null
        }

        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {

            val value = characteristic.getUInt8(0)
            val period = (10 * value).toLong()
            return Data().add(gatt.device.address, DataType.TI_MOVEMENT_PERIOD, period)
        }
    },
    TI_KEY_SERVICE(GattAttributeType.SERVICE, "0000ffe0-0000-1000-8000-00805f9b34fb") {
        override val isSupported:Boolean
            get() = true
    },
    TI_KEY_DATA(GattAttributeType.CHARACTERISTIC, "0000ffe1-0000-1000-8000-00805f9b34fb",
        arrayOf<DataType>(DataType.TI_LEFT_KEY, DataType.TI_RIGHT_KEY, DataType.TI_REED_RELAY)) {
        override val isSupported:Boolean
            get() = true
        override fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {


            val rawKeys = characteristic.getUInt8(0)
            //            LLog.INSTANCE.i("KEYS: " + StringUtil.toHexString(rawKeys));
            return Data()
                .add(gatt.device.address, DataType.TI_LEFT_KEY, rawKeys and 0x1)
                .add(gatt.device.address, DataType.TI_RIGHT_KEY, rawKeys and 0x2 shr 1)
                .add(gatt.device.address, DataType.TI_REED_RELAY, rawKeys and 0x4 shr 2)
        }
    },
    TIME_ACCURACY(GattAttributeType.CHARACTERISTIC, "00002a12-0000-1000-8000-00805f9b34fb", DataType.TIME_ACCURACY),
    TIME_SOURCE(GattAttributeType.CHARACTERISTIC, "00002a13-0000-1000-8000-00805f9b34fb", DataType.TIME_SOURCE),
    TIME_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290e-0000-1000-8000-00805f9b34fb"),
    TIME_UPDATE_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a16-0000-1000-8000-00805f9b34fb", DataType.TIME_UPDATE_CONTROL_POINT),
    TIME_UPDATE_STATE(GattAttributeType.CHARACTERISTIC, "00002a17-0000-1000-8000-00805f9b34fb", DataType.TIME_UPDATE_STATE),
    TIME_WITH_DST(GattAttributeType.CHARACTERISTIC, "00002a11-0000-1000-8000-00805f9b34fb", DataType.TIME_WITH_DST),
    TIME_ZONE(GattAttributeType.CHARACTERISTIC, "00002a0e-0000-1000-8000-00805f9b34fb", DataType.TIME_ZONE),
    TRUE_WIND_DIRECTION(GattAttributeType.CHARACTERISTIC, "00002a71-0000-1000-8000-00805f9b34fb", DataType.TRUE_WIND_DIRECTION),
    TRUE_WIND_SPEED(GattAttributeType.CHARACTERISTIC, "00002a70-0000-1000-8000-00805f9b34fb", DataType.TRUE_WIND_SPEED),
    TWO_ZONE_HEART_RATE_LIMIT(GattAttributeType.CHARACTERISTIC, "00002a95-0000-1000-8000-00805f9b34fb", DataType.TWO_ZONE_HEART_RATE_LIMIT),
    TX_POWER_SERVICE(GattAttributeType.SERVICE, "00001804-0000-1000-8000-00805f9b34fb"),
    TX_POWER_LEVEL(GattAttributeType.CHARACTERISTIC, "00002a07-0000-1000-8000-00805f9b34fb", DataType.TX_POWER_LEVEL),
    UNREAD_ALERT_STATUS(GattAttributeType.CHARACTERISTIC, "00002a45-0000-1000-8000-00805f9b34fb", DataType.UNREAD_ALERT_STATUS),
    USER_CONTROL_POINT(GattAttributeType.CHARACTERISTIC, "00002a9f-0000-1000-8000-00805f9b34fb", DataType.USER_CONTROL_POINT),
    USER_DATA_SERVICE(GattAttributeType.SERVICE, "0000181C-0000-1000-8000-00805f9b34fb"),
    USER_INDEX(GattAttributeType.CHARACTERISTIC, "00002a9a-0000-1000-8000-00805f9b34fb", DataType.USER_INDEX),
    UV_INDEX(GattAttributeType.CHARACTERISTIC, "00002a76-0000-1000-8000-00805f9b34fb", DataType.UV_INDEX),
    VALID_RANGE(GattAttributeType.DESCRIPTOR, "00002906-0000-1000-8000-00805f9b34fb"),
    VALUE_TRIGGER_SETTING(GattAttributeType.DESCRIPTOR, "0000290a-0000-1000-8000-00805f9b34fb"),
    VO2_MAX(GattAttributeType.CHARACTERISTIC, "00002a96-0000-1000-8000-00805f9b34fb", DataType.VO2_MAX),
    WAIST_CIRCUMFERENCE(GattAttributeType.CHARACTERISTIC, "00002a97-0000-1000-8000-00805f9b34fb", DataType.WAIST_CIRCUMFERENCE),
    WEIGHT_SCALE_SERVICE(GattAttributeType.SERVICE, "0000181D-0000-1000-8000-00805f9b34fb"),
    WEIGHT(GattAttributeType.CHARACTERISTIC, "00002a98-0000-1000-8000-00805f9b34fb", DataType.WEIGHT),
    WEIGHT_MEASUREMENT(GattAttributeType.CHARACTERISTIC, "00002a9d-0000-1000-8000-00805f9b34fb", DataType.WEIGHT_MEASUREMENT),
    WEIGHT_SCALE_FEATURE(GattAttributeType.CHARACTERISTIC, "00002a9e-0000-1000-8000-00805f9b34fb", DataType.WEIGHT_SCALE_FEATURE),
    WIND_CHILL(GattAttributeType.CHARACTERISTIC, "00002a79-0000-1000-8000-00805f9b34fb", DataType.WIND_CHILL),
    UNKNOWN(GattAttributeType.CHARACTERISTIC, "00000000-0000-0000-0000-000000000000"),
    ;

    val uuid: UUID = UUID.fromString(sUUID)
    val uuidFuncField: Long = (uuid.mostSignificantBits shr 32) and 0xFFFF

    open val writeType:Int
        get() = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

    open val isSupported:Boolean
        get() = Globals.verboseDevices

    internal open class RefData {
        var prevTimeCalled:Long = -1
    }

    internal class WheelDataItem:RefData() {
        var prevWheelRevolutions = -1
        var prevCrankRevolutions = -1
        var prevWheelEventTime = -1
        var prevCrankEventTime = 1
        var wheelRpm = 0.0
        var crankRpm = 0.0
        var distance = 0.0
        var powerAvg = 0.0
        var energyAtStart = 0

        fun reset() {
            prevWheelRevolutions = -1
            prevCrankRevolutions = -1
            prevWheelEventTime = -1
            prevCrankEventTime = 1
            wheelRpm = 0.0
            crankRpm = 0.0
            powerAvg = 0.0
            energyAtStart = 0
            prevTimeCalled = System.currentTimeMillis()
        }
    }

    enum class IOService {
        LOCAL,
        REMOTE,
        TEST;

        companion object {
            fun fromOrdinal(ord:Int):IOService? {
                return if (ord >= 0 && ord < IOService.values().size) values()[ord] else null
            }
        }
    }

    constructor(type:GattAttributeType, sUUID:String, dataType:DataType) : this(type, sUUID, arrayOf<DataType>(dataType))

    override fun toString():String {
        return super.toString()/*.replace('_', ' ')*/.capitalize()
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    fun stringMeasurement(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        if (dataTypes == null || dataTypes.isEmpty()) return null
        val result = try {
            String(characteristic.value, Charset.forName("UTF-8"))
        } catch (e:UnsupportedEncodingException) {
            StringUtil.toReadableString(characteristic.value)
        }
        return Data().add(gatt.device.address, dataTypes[0], result)
    }


    private fun bytesMeasurement(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        if (dataTypes == null || dataTypes.isEmpty()) {
            LLog.e("No datatypes defined for $this", 300000)
            return null
        }
        return Data().add(gatt.device.address, dataTypes[0], characteristic.value)
    }

    private fun longUintMeasurement(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        if (dataTypes == null || dataTypes.isEmpty())
        {
            LLog.e("No datatypes defined for $this", 300000)
            return null
        }
        val bytes = characteristic.value
        //  LLog.INSTANCE.i("Length of bytes = " + bytes.length);

        return Data().add(gatt.device.address, dataTypes[0], StringUtil.toHex(bytes))
    }

    private fun uint8Measurement(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        if (dataTypes == null || dataTypes.size == 0)
        {
            LLog.e("No datatypes defined for $this", 300000)
            return null
        }
        val value = characteristic.getUInt8(0)
        return Data().add(gatt.device.address, dataTypes[0], value)
    }

    private fun uint16Measurement(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        if (dataTypes == null || dataTypes.size == 0)
        {
            LLog.e("No datatypes defined for $this", 300000)
            return null
        }
        val value = characteristic.getUInt16(0)
        return Data().add(gatt.device.address, dataTypes[0], value)
    }

    open fun parse(gatt:BluetoothGatt, characteristic:BluetoothGattCharacteristic):Data? {
        when (this) {
            SYSTEM_ID -> return longUintMeasurement(gatt, characteristic)
            FIRMWARE_REVISION_STRING,
            HARDWARE_REVISION_STRING,
            SOFTWARE_REVISION_STRING,
            MANUFACTURER_NAME_STRING,
            MODEL_NUMBER_STRING,
            SERIAL_NUMBER_STRING,
            DEVICE_NAME,
            RADBEACON_DEVICENAME -> return stringMeasurement(gatt, characteristic)
            UNKNOWN -> {
                LLog.i("Attribute " + this.toString() + " " + characteristic.uuid + " = " + StringUtil.toReadableString(characteristic.value))
                return null
            }
            else -> {
                val bytes = characteristic.value
                return when (bytes.size) {
                    1 -> uint8Measurement(gatt, characteristic)
                    2 -> uint16Measurement(gatt, characteristic)
                    else -> bytesMeasurement(gatt, characteristic)
                }
            }
        }
    }

    open fun write(gatt: BluetoothGatt?, data: Data): ByteArray? = null

    fun maySetNotification():Boolean {
        when (this) {
            TI_REGISTER_DATA -> return false
            TI_CONNECTIONCONTROL_CHAR1 -> return false
            TI_CONNECTIONCONTROL_CHAR2 -> return false
            TI_CONNECTIONCONTROL_CHAR3 -> return false
            TI_OAD_IMG_BLOCK -> return false
            TI_OAD_IMG_IDENTIFY -> return false
            else -> return true
        }
    }

    fun needsInit():Boolean {
        return commands(CommandType.INIT) != null
    }

    open fun commands(cmdType:CommandType):List<Tuple<ByteArray, String>>? {
        //case TI_IO_DATA: return new byte[]{IO_REDLED};
        //case TI_IO_CONFIG: return new byte[]{IO_LOCAL};
        return null
    }

    open val readAfterWrite: Boolean = true

    open fun characteristicToReadAfter():GattAttribute? {
        return null
    }

//    open fun convertValues(vararg values:Int):ByteArray {
//        val result = ByteArray(values.size)
//        for (i in result.indices)
//        {
//            result[i] = (values[i] and 255).toByte()
//        }
//        return result
//    }

    companion object {
        const val insulinScaler = 1.0
        private const val UINT16_LIMIT = 65536
        private const val UINT32_LIMIT = 4294967296L
        private const val GYRO_ENABLE = 0x07
        private const val ACCEL_ENABLE = 0x38
        private const val MAGNET_ENABLE = 0x40
        private const val WAKE_ENABLE = 0x80
        private const val ACCEL_RANGE_2G = 0x0
        private val accelerationRange = mutableMapOf<String, Int>()
        private val ioservices = mutableMapOf<String, IOService>()
        private val refData = mutableMapOf<String, RefData>()
        private val gson = GsonBuilder()
//                    .excludeFieldsWithoutExposeAnnotation()
            .setPrettyPrinting()
            .create()

        private fun getBitValue(characteristic:BluetoothGattCharacteristic, pos:Int, bits:Int):Int {
            return Numbers.getBits(characteristic.getUInt16(0), pos, bits)
        }

        fun fromUUID(uuid:UUID?):GattAttribute {
            if (uuid == null) return UNKNOWN
            for (attr in values()) if (attr.uuid == uuid) return attr
//            LLog.d("Unknown attribute $uuid")
            return UNKNOWN
        }

        fun stringFromUUID(uuid:UUID):String {
            for (attr in values())
                if (attr.uuid == uuid) return attr.toString()
            return "$UNKNOWN $uuid"
        }

        fun fromCharacteristic(characteristic:BluetoothGattCharacteristic?):GattAttribute {
            if (characteristic == null) return UNKNOWN
            for (attr in values()) if (attr.uuid == characteristic.uuid) return attr
//            LLog.d("Unknown characteristic ${characteristic.uuid}")
            return UNKNOWN
        }
    }


}
