package com.plusot.bluelib.sensor

enum class ScanType {
    INTERNAL,
    ANT_SCAN,
    BT_SCAN,
    BLE_SCAN,
    EXTERNAL;

    fun nextScan(): ScanType {
        return values()[(this.ordinal + 1) % values().size]
    }
}

interface ScanManager {
    val devices: List<ScannedDevice>

    fun close()
    fun startScan(): Boolean
    fun stopScan(comment: String)
}
