package com.plusot.bluelib.sensor.data

import com.plusot.bluelib.BuildConfig
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.util.*
import com.plusot.bluelib.util.json.JSoNField
import com.plusot.bluelib.util.json.JSoNHelper

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.StandardCharsets

import java.util.Arrays
import java.util.EnumMap
import java.util.EnumSet
import java.util.HashMap
import kotlin.math.min

class Data (private var defaultSensorId: String? = null, private var time: Long = System.currentTimeMillis()) {
    private val values = EnumMap<DataType, MutableMap<String, Tuple<Any, Long>>>(DataType::class.java)

    val dataTypes: Array<DataType>
        get() = values.keys.toTypedArray()

    fun clear() = values.clear()

    val supportedDataTypes: EnumSet<DataType>
        get() {
            val supported = EnumSet.noneOf(DataType::class.java)
            for (type in values.keys) {
                if (type.visible) supported.add(type)
            }
            return supported
        }

    interface Checker {
        fun isChecked(type: DataType): Boolean
    }

    fun hasData(): Boolean {
        return values.size > 0
    }

    fun length(): Int {
        return values.size
    }

    fun add(type: DataType): Data {
        if (values[type] == null) values[type] = HashMap()
        return this
    }

    fun getValuesForType(type: DataType) = values[type]

    private fun setValue(sensorId: String, type: DataType, value: Any, time: Long): Data {
        return setValue(sensorId, type, Tuple<Any, Long>(value, time))
    }

    private fun setValue(sensorId: String, type: DataType, valueTime: Tuple<Any, Long>): Data {
        val map = values.getOrPut(type, { mutableMapOf() })
        map[sensorId] = valueTime
        return this
    }

    private fun getValueTime(sensorId: String?, type: DataType): Tuple<Any, Long>? {
        val map = values[type] ?: return null
        if (sensorId != null) {
            val t = map[sensorId]
            if (t != null) return Tuple(clone(t.t1()), t.t2())
        }
        return map.values.firstOrNull()?.let { return Tuple(clone(it.t1()), it.t2()) }
    }

    fun getValue(sensorId: String?, type: DataType): Any? {
        val t = getValueTime(sensorId, type)
        return t?.t1()
    }

    fun split(translator: SensorIdTranslator): Map<String, Data> {
        val splitData = mutableMapOf<String, Data>()
        for (type in values.keys) {
            val map = values[type] ?: continue
            for (sensorId in map.keys) getValueTime(sensorId, type)?.let {
                val translatedSensorId = translator.translate(sensorId)
                val data = splitData.getOrPut(translatedSensorId, { Data(/*sensorId, */time = time) })
                data.setValue(translatedSensorId, type, it)
            }
        }
        return splitData
    }

    fun add(types: Array<DataType>?): Data {
        if (types == null || types.isEmpty()) return this
        for (type in types) add(type)
        return this
    }

    fun add(sensorId: String, type: DataType, value: String): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: Double): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: Float): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: Int): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: Int, add2History: Boolean): Data {
        return _add(sensorId, type, value, System.currentTimeMillis(), add2History)
    }

    fun add(sensorId: String, type: DataType, value: Boolean): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: Long): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: ByteArray): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: IntArray): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: IntArray, add2History: Boolean): Data {
        return _add(sensorId, type, value, System.currentTimeMillis(), add2History)
    }

    fun add(sensorId: String, type: DataType, value: FloatArray): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, value: DoubleArray): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(sensorId: String, type: DataType, valueTime: Tuple<Any, Long>): Data? {
        return _add(sensorId, type, valueTime.t1(), valueTime.t2(), false)
    }

    fun add(sensorId: String, type: DataType, value: Double, time: Long): Data {
        return _add(sensorId, type, value, time, false)
    }

    fun add(type: DataType, value: String, timestamp: Long = time): Data {
        return _add(defaultSensorId, type, value, timestamp, false)
    }

    fun add(type: DataType, value: Double, timestamp: Long = time): Data {
        return _add(defaultSensorId, type, value, timestamp, false)
    }

    fun add(type: DataType, value: Long, timestamp: Long = time): Data {
        return _add(defaultSensorId, type, value, timestamp, false)
    }

    fun add(type: DataType, value: Float): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: Int): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: Boolean): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: Int, add2History: Boolean): Data {
        return _add(defaultSensorId, type, value, System.currentTimeMillis(), add2History)
    }

    fun add(type: DataType, value: Long): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: ByteArray): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: IntArray): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: IntArray, add2History: Boolean): Data {
        return _add(defaultSensorId, type, value, System.currentTimeMillis(), add2History)
    }

    fun add(type: DataType, value: FloatArray): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, value: DoubleArray): Data {
        return _add(defaultSensorId, type, value, time, false)
    }

    fun add(type: DataType, valueTime: Tuple<Any, Long>): Data? {
        return _add(defaultSensorId, type, valueTime.t1(), valueTime.t2(), false)
    }

    private fun _add(sensorId: String?, type: DataType, value: Any, time: Long, add2History: Boolean): Data {
        //if (!type.visible()) return this;
        if (add2History)
            synchronized(history) {
                val map = history.getOrPut(type, { mutableMapOf() } )
                val typeHistory = map.getOrPut(sensorId ?: defaultSensorId ?: genericSensorId, { mutableListOf() })
                typeHistory.add(value)
            }
        setValue(sensorId ?: defaultSensorId ?: genericSensorId, type, value, time)
        this.time = time //Math.max(this.time, time);
        return this
    }

    fun getSensorIds(type: DataType): Array<String>? {
        val map = values[type] ?: return null
        return map.keys.toTypedArray()
    }

    fun hasDataType(dataType: DataType): Boolean {
        return values.keys.contains(dataType) && values[dataType] != null && values[dataType]?.isEmpty() == false
    }

    fun getDouble(sensorId: String? = null, type: DataType): Double {
        return _getDouble(getValue(sensorId, type))
    }

    fun getDoubleNullable(sensorId: String? = null, type: DataType): Double? {
        return _getDoubleNullable(getValue(sensorId, type))
    }

    private fun getFloat(sensorId: String?, type: DataType): Float {
        return getDouble(sensorId, type).toFloat()
    }

    fun getInt(sensorId: String?, type: DataType, defaultValue: Int = 0): Int {
        val obj = getValue(sensorId, type) ?: return defaultValue
        if (obj is Boolean) return if (obj) 1 else 0
        if (obj is Double) return obj.toInt()
        if (obj is Int) return obj
        if (obj is Long) return obj.toInt()
        if (obj is Float) return obj.toInt()
        if (obj is String) return Integer.valueOf(obj)
        if (obj is FloatArray) {
            val v = MathVector(obj, 3)
            return v.length().toInt()
        }
        if (obj is DoubleArray) {
            val v = MathVector(obj)
            return v.length().toInt()
        }
        if (obj is ByteArray) {
            val v = MathVector(obj.map { it.toDouble()}.toDoubleArray())
            return v.length().toInt()
        }
        if (obj is IntArray) {
            val v = MathVector(obj.map { it.toDouble()}.toDoubleArray())
            return v.length().toInt()
        }
        return defaultValue
    }

    fun getBool(sensorId: String?, type: DataType, defaultValue: Boolean = false): Boolean {
        val obj = getValue(sensorId, type) ?: return defaultValue
        if (obj is Boolean) return obj
        if (obj is Double) return obj > 0.0
        if (obj is Int) return obj > 0
        if (obj is Long) return obj > 0L
        if (obj is Float) return obj > 0.0F
        if (obj is String) return obj.equals("true", true)
        if (obj is FloatArray) {
            val v = MathVector(obj, 3)
            return v.length().toInt() > 0
        }
        if (obj is DoubleArray) {
            val v = MathVector(obj)
            return v.length().toInt() > 0
        }
        if (obj is ByteArray) {
            val v = MathVector(obj)
            return v.length().toInt() > 0
        }
        if (obj is IntArray) {
            val v = MathVector(obj)
            return v.length().toInt() > 0
        }
        return defaultValue
    }

    fun getLong(sensorId: String? = null, type: DataType): Long? {
        val obj = getValue(sensorId, type) ?: return null
        if (obj is Boolean) return if (obj) 1L else 0L
        if (obj is Double) return obj.toLong()
        if (obj is Int) return obj.toLong()
        if (obj is Long) return obj
        if (obj is Float) return obj.toLong()
        if (obj is String) return java.lang.Long.valueOf(obj)
        if (obj is FloatArray) {
            val v = MathVector(obj, 3)
            return v.length().toLong()
        }
        if (obj is DoubleArray) {
            val v = MathVector(obj)
            return v.length().toLong()
        }
        if (obj is ByteArray) {
            val v = MathVector(obj)
            return v.length().toLong()
        }
        if (obj is IntArray) {
            val v = MathVector(obj)
            return v.length().toLong()
        }
        return null
    }

    fun getLong(sensorId: String? = null, type: DataType, defaultValue: Long) = getLong(sensorId, type) ?: defaultValue


    fun getString(sensorId: String?, type: DataType): String? {
        val obj = getValue(sensorId, type) ?: return null
        if (obj is Boolean) return if (obj) "true" else "false"
        if (obj is Double) return Format.format(obj, type.decimals)
        if (obj is Int) return "" + obj
        if (obj is Long) return "" + obj
        if (obj is Float) return Format.format(obj, type.decimals)
        if (obj is String) return obj
        if (obj is FloatArray) return StringUtil.toString(obj, ",", type.decimals)
        if (obj is IntArray) return StringUtil.toString(obj, ",")
        if (obj is ByteArray) return StringUtil.toString(obj, ",")
        return if (obj is DoubleArray) StringUtil.toString(obj, ",", type.decimals) else null
    }

    fun getDoubles(sensorId: String?, type: DataType): DoubleArray {
        val obj = getValue(sensorId, type) ?: return doubleArrayOf()
        if (obj is Boolean) return doubleArrayOf(if (obj) 1.0 else 0.0)
        if (obj is Double) return doubleArrayOf(obj)
        if (obj is Int) return doubleArrayOf(obj.toDouble())
        if (obj is Long) return doubleArrayOf(obj.toDouble())
        if (obj is Float) return doubleArrayOf(obj.toDouble())
        if (obj is String) return doubleArrayOf(java.lang.Double.valueOf(obj))
        if (obj is FloatArray) {
            val ret = DoubleArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toDouble()
            return ret
        }
        if (obj is IntArray) {
            val ret = DoubleArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toDouble()
            return ret
        }
        if (obj is ByteArray) {
            val ret = DoubleArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toDouble()
            return ret
        }
        return obj as? DoubleArray ?: doubleArrayOf()
    }

    fun getBytes(sensorId: String?, type: DataType): ByteArray {
        val obj = getValue(sensorId, type) ?: return byteArrayOf()
        if (obj is Boolean) return byteArrayOf((if (obj) 1 else 0).toByte())
        if (obj is Double) return byteArrayOf(obj.toInt().toByte())
        if (obj is Int) return byteArrayOf(obj.toByte())
        if (obj is Long) return byteArrayOf(obj.toByte())
        if (obj is Float) return byteArrayOf(obj.toInt().toByte())
        if (obj is String) return obj.toByteArray(StandardCharsets.UTF_8)
        if (obj is FloatArray) {
            val ret = ByteArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toInt().toByte()
            return ret
        }
        if (obj is DoubleArray) {
            val ret = ByteArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toInt().toByte()
            return ret
        }
        if (obj is IntArray) {
            val ret = ByteArray(obj.size)
            for (i in ret.indices) ret[i] = obj[i].toByte()
            return ret
        }
        return obj as? ByteArray ?: byteArrayOf()
    }

    fun getStringWithoutLabel(sensorId: String?, type: DataType): String? {
        val obj = getValue(sensorId, type) ?: return null
        if (obj is Boolean) return type.toValueString(obj, false, false)
        if (obj is Double) return type.toValueString(obj, false, false)
        if (obj is Int) return type.toValueString(obj, false, false)
        if (obj is Long) return type.toValueString(obj, false, false)
        if (obj is Float) return type.toValueString(obj, false, false)
        if (obj is String && obj != "") return obj
        if (obj is FloatArray) return type.toValueString(obj)
        if (obj is ByteArray) return type.toValueString(obj)
        if (obj is IntArray) return type.toValueString(obj)
        return if (obj is DoubleArray) type.toValueString(obj) else null
    }

    fun greater(sensorId: String, type: DataType, otherData: Data?): Boolean? {
        val obj = getValue(sensorId, type)
        if (obj == null || otherData == null) return null
        if (!otherData.hasDataType(type)) return null
        if (obj is Boolean) return obj > otherData.getBool(sensorId, type)
        if (obj is Double) return obj > otherData.getDouble(sensorId, type)
        if (obj is Int) return obj > otherData.getInt(sensorId, type, 0)
        if (obj is Long) return obj > otherData.getLong(sensorId, type, 0L)
        if (obj is Float) return obj > otherData.getFloat(sensorId, type)
        if (obj is String && obj != "") return obj.compareTo(otherData.getString(sensorId, type) ?: "") > 0
        if (obj is IntArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as IntArray? ?: return true
            val v2 = MathVector(f2)
            return v1.length() > v2.length()
        }
        if (obj is ByteArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as ByteArray? ?: return true
            val v2 = MathVector(f2)
            return v1.length() > v2.length()
        }
        if (obj is FloatArray) {
            val v1 = MathVector(obj, obj.size)
            val f2 = otherData.getValue(sensorId, type) as FloatArray? ?: return true
            val v2 = MathVector(f2, f2.size)
            return v1.length() > v2.length()
        }
        if (obj is DoubleArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as DoubleArray? ?: return true
            val v2 = MathVector(f2)
            return v1.length() > v2.length()
        }
        return null
    }

    fun smaller(sensorId: String, type: DataType, otherData: Data?): Boolean? {
        val obj = getValue(sensorId, type)
        if (obj == null || otherData == null) return null
        if (!otherData.hasDataType(type)) return null
        if (obj is Boolean) return obj < otherData.getBool(sensorId, type)
        if (obj is Double) return obj < otherData.getDouble(sensorId, type)
        if (obj is Int) return obj < otherData.getInt(sensorId, type, 0)
        if (obj is Long) return obj < otherData.getLong(sensorId, type, 0L)
        if (obj is Float) return obj < otherData.getFloat(sensorId, type)
        if (obj is String && obj != "") return obj < otherData.getString(sensorId, type) ?: ""
        if (obj is IntArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as IntArray? ?: return false
            val v2 = MathVector(f2)
            return v1.length() < v2.length()
        }
        if (obj is ByteArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as ByteArray? ?: return false
            val v2 = MathVector(f2)
            return v1.length() < v2.length()
        }

        if (obj is FloatArray) {
            val v1 = MathVector(obj, obj.size)
            val f2 = otherData.getValue(sensorId, type) as FloatArray? ?: return false
            val v2 = MathVector(f2, f2.size)
            return v1.length() < v2.length()
        }
        if (obj is DoubleArray) {
            val v1 = MathVector(obj)
            val f2 = otherData.getValue(sensorId, type) as DoubleArray? ?: return false
            val v2 = MathVector(f2)
            return v1.length() < v2.length()
        }
        return null
    }

    fun equal(sensorId: String, type: DataType, otherData: Data?): Boolean? {
        val obj = getValue(sensorId, type)
        if (obj == null || otherData == null) return null
        if (!otherData.hasDataType(type)) return null
        if (obj is Boolean) return obj == otherData.getBool(sensorId, type)
        if (obj is Double) return obj == otherData.getDouble(sensorId, type)
        if (obj is Int) return obj == otherData.getInt(sensorId, type, 0)
        if (obj is Long) return obj == otherData.getLong(sensorId, type)
        if (obj is Float) return obj as Float? == otherData.getFloat(sensorId, type)
        if (obj is String && obj != "") return obj == otherData.getString(sensorId, type)
        if (obj is IntArray) {
            val f2 = otherData.getValue(sensorId, type) as IntArray? ?: return false
            for (i in 0 until min(obj.size, f2.size)) {
                if (obj[i] != f2[i]) return false
            }
            return true
        }
        if (obj is ByteArray) {
            val f2 = otherData.getValue(sensorId, type) as ByteArray? ?: return false
            for (i in 0 until min(obj.size, f2.size)) {
                if (obj[i] != f2[i]) return false
            }
            return true
        }
        if (obj is FloatArray) {
            val f2 = otherData.getValue(sensorId, type) as FloatArray? ?: return false
            for (i in 0 until min(obj.size, f2.size)) {
                if (obj[i] != f2[i]) return false
            }
            return true
        }
        if (obj is DoubleArray) {
            val f2 = otherData.getValue(sensorId, type) as DoubleArray? ?: return false
            for (i in 0 until min(obj.size, f2.size)) {
                if (obj[i] != f2[i]) return false
            }
            return true
        }
        return null
    }

    fun getVoiceString(sensorId: String, type: DataType): String? {
        val obj = getValue(sensorId, type) ?: return null
        if (obj is Boolean) return type.toVoiceString(obj)
        if (obj is Double) return type.toVoiceString(obj)
        if (obj is Int) return type.toVoiceString(obj)
        if (obj is Long) return type.toVoiceString(obj)
        if (obj is Float) return type.toVoiceString(obj.toDouble())
        if (obj is String) return type.toVoiceString(obj)
        if (obj is IntArray) return type.toVoiceString(obj)
        if (obj is ByteArray) return type.toVoiceString(obj)
        if (obj is FloatArray) return type.toVoiceString(obj)
        return if (obj is DoubleArray) type.toVoiceString(obj) else null
    }

    fun getTime(sensorId: String, type: DataType): Long {
        val t = getValueTime(sensorId, type)
        return if (t != null) t.t2() else time
    }

    fun getTime(): Long {
        return time
    }

    fun add(otherData: Data): Data {
        return merge(otherData)
    }

    fun merge(otherData: Data): Data {
        time = otherData.time //Math.max(time, otherData.time);
        for (type in otherData.values.keys) {
            val otherMap = otherData.values[type] ?: continue
            for (sensorId in otherMap.keys) otherData.getValueTime(sensorId, type)?. let { setValue(sensorId, type, it) }
        }
        return this
    }

    operator fun plusAssign(otherData: Data) { merge(otherData) }


    fun mergeCheckEqual(otherData: Data, replace: Boolean): Boolean {
        var equal = true
        time = otherData.time //Math.max(time, otherData.time);
        if (values.size != otherData.values.size) equal = false
        for (type in otherData.values.keys) {
            val otherMap = otherData.values[type] ?: continue
            val map = values[type]
            if (map == null || map.size != otherMap.size) equal = false
            for (sensorId in otherMap.keys) {
                val otherValueTime = otherData.getValueTime(sensorId, type)
                val valueTime = getValueTime(sensorId, type)
                if ((otherValueTime == null && valueTime != null) ||
                    (otherValueTime != null && valueTime == null) ||
                    (otherValueTime != null && valueTime != null && otherValueTime.t1() != valueTime.t1()))
                    equal = false
                if ((!equal || replace) && otherValueTime != null) setValue(sensorId, type, otherValueTime)
            }
        }
        return equal
    }

    fun mergeAggregates(otherData: Data) {
        time = Math.max(time, otherData.time)
        for (type in otherData.values.keys) {
            val otherMap = otherData.values[type] ?: continue
            if (type.isAggregate) for (sensorId in otherMap.keys) otherData.getValueTime(sensorId, type)?. let { setValue(sensorId, type, it) }
        }
    }

    fun clone(): Data {
        val newData = Data(defaultSensorId, time)
        for (type in values.keys) {
            val map = values[type]
            if (map != null)
                for (sensorId in map.keys) {
                    getValueTime(sensorId, type)?.let { newData.setValue(sensorId, type, it) }
                }
        }
        return newData
    }

    fun cloneAggregates(): Data {
        val newData = Data(defaultSensorId, time)
        for (type in values.keys)
            if (type.isAggregate) {
                val map = values[type]
                if (map != null)
                    for (sensorId in map.keys) {
                        getValueTime(sensorId, type)?.let { newData.setValue(sensorId, type, it) }
                    }
            }
        return newData
    }

    fun clone(type: DataType): Data? {
        val map = values[type]
        if (map != null) {
            val newData = Data(defaultSensorId, time)
            for (sensorId in map.keys) {
                getValueTime(sensorId, type)?.let {newData.setValue(sensorId, type, it) }
            }
            return newData
        }
        return null
    }

    fun clone(sensorId: String, type: DataType): Data? {
        val map = values[type]
        if (map != null) {
            val vt = getValueTime(sensorId, type)
            if (vt != null)
                return Data(defaultSensorId, time).setValue(sensorId, type, vt)

        }
        return null
    }

    fun clone(checker: Checker): Data {

        val newData = Data(defaultSensorId, time)
        for (type in values.keys)
            if (checker.isChecked(type)) {
                val map = values[type]
                if (map != null)
                    for (sensorId in map.keys) {
                        getValueTime(sensorId, type)?.let { newData.setValue(sensorId, type, it) }
                    }
            }
        return newData
    }

    override fun toString(): String = toString(", ")

    fun toString(sensorId: String, type: DataType, delim: String= ", "): String? {
        val obj = getValue(sensorId, type)
        when {
            obj is Boolean -> return type.toString(obj)
            obj is Int -> return type.toString(obj)
            obj is Long -> return type.toString(obj)
            obj is Double -> return type.toString(obj)
            obj is Float -> return type.toString(obj.toDouble())
            obj is String -> return "${type.toProperString()} = $obj"
            obj is ByteArray -> return type.toString(obj, delim)
            obj is IntArray -> return type.toString(obj, delim)
            obj is FloatArray -> {
                val value = StringUtil.toString(obj as FloatArray?, delim, type.decimals)
                return "${type.toProperString()} = $value"
            }
            obj is DoubleArray -> {
                val value = StringUtil.toString(obj as DoubleArray?, delim, type.decimals)
                return "${type.toProperString()} = $value"
            }
            obj != null -> return "${type.toProperString()} = $obj (${obj.javaClass.simpleName})"
            else -> return null
        }
    }

    fun toStringOr(sensorId: String, type: DataType, delim: String= ", "): String = toString(sensorId, type, delim) ?: "${type.toProperString()} = null"

    fun toString(delim: String): String {
        val sb = Stringer(delim)
        for (type in values.keys) {
            val map = values[type] ?: continue
            for (sensorId in map.keys) toString(sensorId, type, delim)?.let { sb.append(it) }
        }
        sb.append("${R.string.timeStamp.getString()} = ${TimeUtil.formatTime(getTime())}")
        return sb.toString()
    }

    fun toStringList(): List<String> {
        val list = mutableListOf<String>()
        for (type in values.keys) {
            val map = values[type] ?: continue
            for (sensorId in map.keys) toString(sensorId, type, ", ")?.let { list.add(it) }
        }
        list.add("${R.string.timeStamp.getString()} = ${TimeUtil.formatTime(getTime())}")
        list.add("${R.string.app_name.getString()} version = ${appVersion()}")
        list.add("${R.string.app_name.getString()} build = ${BuildConfig.BUILD_TIME.toTimeStr()}")
        return list.sorted()
    }



    interface SensorIdTranslator {
        fun translate(sensorId: String): String
    }

    fun toJSON(translator: SensorIdTranslator?): JSONObject? {
        val jArray = JSONArray()

        for (type in values.keys) {
            val map = values[type] ?: continue
            for (sensorId in map.keys) {
                var id = sensorId
                if (translator != null) id = translator.translate(sensorId)
                val obj = getValue(id, type)
                if (obj != null) jArray.put(type.toJSON(obj, id))
            }
        }
        if (jArray.length() == 0) return null
        val jObj = JSONObject()
        try {
            jObj.put(JSoNField.VALUES.toString(), jArray)
            jObj.put(JSoNField.TIME.toString(), getTime())
        } catch (e: JSONException) {
            LLog.e("Could not create JSON object", e)
        }

        return jObj
    }

    fun toNameValueString(): Tuple<String, String> {
        val names = Stringer("\n")
        val values = Stringer("\n")

        for (type in this.values.keys) {
            val map = this.values[type] ?: continue
            for (sensorId in map.keys) {
                val obj = this.getValue(sensorId, type)
                if (obj is Double) {
                    val value = obj
                    names.append(type.toProperString())
                    values.append(type.toValueString(value, true, false))
                } else if (obj is Float) {
                    val value = obj
                    names.append(type.toProperString())
                    values.append(type.toValueString(value, true, false))
                } else if (obj is Int) {
                    val value = obj
                    names.append(type.toProperString())
                    values.append(type.toValueString(value, true, false))
                } else if (obj is Long) {
                    val value = obj
                    names.append(type.toProperString())
                    values.append(type.toValueString(value, true, false))
                } else if (obj is String) {
                    val value = obj
                    if (value != "") {
                        names.append(type.toProperString())
                        values.append(value)
                    }
                } else if (obj is ByteArray) {
                    val value = StringUtil.toHex(obj as ByteArray?)
                    names.append(type.toProperString())
                    values.append(value)
                } else if (obj is IntArray) {
                    val value = StringUtil.toString(obj as IntArray?, ", ")
                    names.append(type.toProperString())
                    values.append(value)
                } else if (obj is DoubleArray) {
                    val value = StringUtil.toString(
                        obj as DoubleArray?,
                        ", ",
                        type.decimals
                    ) + " " + type.unit.toString()
                    names.append(type.toProperString())
                    values.append(value)
                } else if (obj is FloatArray) {
                    val value = StringUtil.toString(
                        obj as FloatArray?,
                        ", ",
                        type.decimals
                    ) + " " + type.unit.toString()
                    names.append(type.toProperString())
                    values.append(value)
                } else if (obj != null) {
                    LLog.i("Could not create NameValueString for " + type + " = " + obj.toString() + " (" + obj.javaClass.simpleName + ")")
                }
            }
        }
        return Tuple(names.toString(), values.toString())
    }

    fun toNameValueString(sensorId: String, type: DataType): Tuple<String, String>? {
        val value = toValueString(sensorId, type)
        return if (value != null) Tuple(type.toProperString(), value) else null
    }

    fun toValueString(sensorId: String, type: DataType): String? {
        return toValueString(sensorId, type, true, 0, 0f, 0f, false)
    }

    fun toValueString(sensorId: String, type: DataType, withUnit: Boolean): String {
        return toValueString(sensorId, type, withUnit, 0, 0f, 0f, false)
    }

    fun toValueString(sensorId: String, type: DataType, offset: Float): String {
        return toValueString(sensorId, type, true, 0, offset, 0f, false)
    }

    fun toValueString(sensorId: String, type: DataType, mask: Int): String {
        return toValueString(sensorId, type, true, mask, 0f, 0f, false)
    }

    private fun toValueString(
        sensorId: String,
        type: DataType,
        withUnit: Boolean,
        mask: Int,
        offset: Float,
        cutOff: Float,
        downSizeFraction: Boolean
    ): String {
        val ret = _toValueString(sensorId, type, false, mask, offset, cutOff, downSizeFraction)
        if (ret == null || ret.isEmpty()) return ""
        return if (ret.size == 1 || !withUnit) ret[0] else ret[0] + " " + ret[1]
    }

    fun toValueStringParts(sensorId: String, type: DataType): Array<String>? {
        return _toValueString(sensorId, type, false, 0, 0f, 0.0f, false)
    }

    fun toValuePair(sensorId: String, type: DataType): Pair<String, String>? {
        val array =  _toValueString(sensorId, type, false, 0, 0f, 0.0f, false) ?: return null
        if (array.isEmpty()) return null
        if (array.size == 1) return array[0] to ""
        return array[0] to array[1]
    }

    private fun _toValueString(
        sensorId: String,
        type: DataType,
        speech: Boolean,
        mask: Int,
        offset: Float,
        cutOff: Float,
        downSizeFraction: Boolean
    ): Array<String>? {
        val obj = getValue(sensorId, type) ?: return null
        if (obj is Double) {
            var value = obj + offset
            if (cutOff > 0) {
                if (downSizeFraction && Math.abs(value) < 1) value *= value
                if (Math.abs(value) < cutOff) value = 0.0
            }
            return type.toValueString(speech, value)
        } else if (obj is Float) {
            var value = obj + offset
            if (cutOff > 0) {
                if (downSizeFraction && Math.abs(value) < 1f) value *= value
                if (Math.abs(value) < cutOff) value = 0.0f
            }
            return type.toValueString(speech, value)
        } else if (obj is Int) {
            var value = obj + offset.toInt()
            if (mask != 0) value = value and mask
            return type.toValueString(speech, value)
        } else if (obj is Long) {
            val value = obj + offset.toInt()
            return type.toValueString(speech, value)
        } else if (obj is String) {
            return arrayOf(obj)
        } else if (obj is ByteArray) {
            return arrayOf(StringUtil.toHex(obj))
        } else if (obj is IntArray) {
            return arrayOf(StringUtil.toString(obj, ", "))
        } else if (obj is DoubleArray) {
            return if (speech) arrayOf(
                StringUtil.toString(obj, ", ", type.decimals),
                type.unit.toVoiceString()
            ) else arrayOf(StringUtil.toString(obj, ", ", type.decimals), type.unit.toString())
        } else if (obj is FloatArray) {
            return if (speech) arrayOf(
                StringUtil.toString(obj, ", ", type.decimals),
                type.unit.toVoiceString()
            ) else arrayOf(StringUtil.toString(obj, ", ", type.decimals), type.unit.toString())
        } else {
            LLog.i("Could not create NameValueString for " + type + " = " + obj.toString() + " (" + obj.javaClass.simpleName + ")")
        }
        return null
    }


    fun setTime(time: Long) {
        if (time > this.time) this.time = time
    }

//    public void setTime() {
//        this.time = System.currentTimeMillis();
//    }

    fun isValueChange(newData: Data, type: DataType): Boolean {
        if (!newData.hasDataType(type)) return false
        if (!hasDataType(type)) return true
        val map = newData.values[type] ?: return false
        for (sensorId in map.keys) {
            val strs1 = this.toValueStringParts(sensorId, type)
            val strs2 = newData.toValueStringParts(sensorId, type)
            if (strs1 != null && strs1.size > 0 && strs2 != null && strs2.size > 0) return strs1[0] != strs2[0]
        }
        return true
    }

    fun isValueChange(other: Data?): Boolean { //}, EnumSet<DataType> types) {
        if (other == null) return false
        //        if (types == null || types.size() == 0)
        for (type in DataType.values()) {
            if (isValueChange(other, type)) return true
        }
        //        } else for (DataType type: types) {
        //            if (isValueChange(other, type)) return true;
        //        }
        return false
    }

    companion object {
        const val genericSensorId = "world"
        private val history = EnumMap<DataType, MutableMap<String, MutableList<Any>>>(DataType::class.java)

        fun fromDataTypes(types: Array<DataType>?): Data? {
            return if (types == null || types.isEmpty()) null else Data().add(types)
        }

        private fun _getDouble(obj: Any?): Double {
            if (obj == null) return java.lang.Double.NaN
            if (obj is Double) return obj
            if (obj is Boolean) return obj.toDouble()
            if (obj is Int) return obj.toDouble()
            if (obj is Long) return obj.toDouble()
            if (obj is Float) return obj.toDouble()
            if (obj is String)
                try {
                    return java.lang.Double.valueOf(obj)
                } catch (e: NumberFormatException) {
                    return java.lang.Double.NaN
                }

            if (obj is FloatArray) {
                val v = MathVector(obj, 3)
                return v.length()
            }
            if (obj is IntArray) {
                return if (obj.size > 0) 1.0 * obj[0] else 0.0
            }
            if (obj is ByteArray) {
                return if (obj.size > 0) 1.0 * obj[0] else 0.0
            }

            if (obj is DoubleArray) {
                val v = MathVector(obj)
                return v.length()
            }

            return java.lang.Double.NaN
        }

        private fun _getDoubleNullable(obj: Any?): Double? {
            if (obj == null) return null
            if (obj is Double) return obj
            if (obj is Boolean) return obj.toDouble()
            if (obj is Int) return obj.toDouble()
            if (obj is Long) return obj.toDouble()
            if (obj is Float) return obj.toDouble()
            if (obj is String)
                return try {
                    java.lang.Double.valueOf(obj)
                } catch (e: NumberFormatException) {
                    null
                }

            if (obj is FloatArray) {
                val v = MathVector(obj, 3)
                return v.length()
            }
            if (obj is IntArray) {
                return if (obj.size > 0) 1.0 * obj[0] else 0.0
            }
            if (obj is ByteArray) {
                return if (obj.size > 0) 1.0 * obj[0] else 0.0
            }

            if (obj is DoubleArray) {
                val v = MathVector(obj)
                return v.length()
            }

            return null
        }

        private fun clone(obj: Any): Any {
            //if (obj == null) return null
            if (obj is Double) return 1.0 * obj
            if (obj is Int) return 1 * obj
            if (obj is Long) return 1L * obj
            if (obj is Float) return 1.0F * obj
            if (obj is String) return "" + obj
            if (obj is FloatArray) return obj.copyOf(obj.size)
            if (obj is IntArray) return Arrays.copyOf(obj, obj.size)
            if (obj is ByteArray) return Arrays.copyOf(obj, obj.size)
            return if (obj is DoubleArray) Arrays.copyOf(obj, obj.size) else obj
        }

        fun fromJSON(jsonStr: String): Data? {
            try {
                val json = JSONObject(jsonStr)
                return fromJSON(json)
            } catch (e: JSONException) {
                LLog.e("Could not create JSON object from $jsonStr", e)
            }

            return null
        }

        fun fromJSON(json: JSONObject): Data? {
            val time = JSoNHelper.get(json, JSoNField.TIME, -1L)
            if (time == -1L) return null
            val jArray = JSoNHelper.get(json, JSoNField.VALUES) ?: return null
            val data = Data(time = time)

            try {
                for (i in 0 until jArray.length()) {
                    val jObj = jArray.getJSONObject(i)
                    val sensorId = JSoNHelper.getNullable(jObj, JSoNField.SENSORID, null as String?) ?: continue
                    if (data.defaultSensorId == null) data.defaultSensorId = sensorId
                    val className = JSoNHelper.getNullable(jObj, JSoNField.CLASS, null as String?)
                    val dt = DataType.fromJSONString(JSoNHelper.getNullable(jObj, JSoNField.DATATYPE, null as String?)) ?: continue
                    if (className.equals("Integer", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0))
                    } else if (className.equals("Double", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0.0))
                    } else if (className.equals("Float", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0.0f))
                    } else if (className.equals("Long", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, 0L))
                    } else if (className.equals("String", ignoreCase = true)) {
                        JSoNHelper.getNullable(jObj, JSoNField.VALUE, null as String?)?.let { data.add(sensorId, dt,it) }
                    } else if (className.equals("int[]", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, intArrayOf()))
                    } else if (className.equals("float[]", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, floatArrayOf()))
                    } else if (className.equals("double[]", ignoreCase = true)) {
                        data.add(sensorId, dt, JSoNHelper.get(jObj, JSoNField.VALUE, doubleArrayOf()))
                    } else {
                        LLog.i("Unknown classname: $className")
                        JSoNHelper.getNullable(jObj, JSoNField.VALUE, null as String?)?.let { data.add(sensorId, dt, it) }
                    }
                    //if (sensorId != null) deviceData.setSensorId(dt, sensorId);

                }
            } catch (e: JSONException) {
                LLog.e("Could not create Data from JSON object", e)
            }

            return data
        }

        fun getHistoryLength(sensorId: String, type: DataType): Int {
            val map = history[type] ?: return 0
            return map[sensorId]?.size ?: 0
        }

        fun historyClear(sensorId: String, type: DataType) {
            val map = history[type] ?: return
            val typeHistory = map[sensorId] ?: return
            typeHistory.clear()
        }

        fun getHistoryAsDoubles(sensorId: String, type: DataType, offset: Int): DoubleArray {
            var result = DoubleArray(0)
            val map = history[type] ?: return result

            val typeHistory = map[sensorId] ?: return result
            if (typeHistory.size < offset) return result
            synchronized(history) {
                result = DoubleArray(typeHistory.size - offset)
                for (i in result.indices) {
                    result[i] = _getDouble(typeHistory[i + offset])
                }
            }
            return result
        }
    }
}
