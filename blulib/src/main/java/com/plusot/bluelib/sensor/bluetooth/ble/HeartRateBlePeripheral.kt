package com.plusot.bluelib.sensor.bluetooth.ble;

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.toHex

class HeartRateBlePeripheral: BlePeripheral(GattAttribute.HEART_RATE_SERVICE.uuid) {
    private val heartrateCharacteristic = BluetoothGattCharacteristic(
        GattAttribute.HEART_RATE_MEASUREMENT.uuid,
        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
        BluetoothGattCharacteristic.PERMISSION_READ)

    init {
        heartrateCharacteristic.addDescriptor(getClientCharacteristicConfigurationDescriptor())
        heartrateCharacteristic.addDescriptor(getCharacteristicUserDescriptionDescriptor("Characteristic for heart rate measurement"))
        service.addCharacteristic(heartrateCharacteristic)

        var characteristic = BluetoothGattCharacteristic(
            GattAttribute.BODY_SENSOR_LOCATION.uuid,
            BluetoothGattCharacteristic.PROPERTY_READ,
            BluetoothGattCharacteristic.PERMISSION_READ
        )
        service.addCharacteristic(characteristic)

        characteristic = BluetoothGattCharacteristic(
            GattAttribute.HEART_RATE_CONTROL_POINT.uuid,
            BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        service.addCharacteristic(characteristic)
    }

    override fun setValue(data: Data) {
        if (data.hasDataType(DataType.HEART_RATE)) {
            heartrateCharacteristic.value = byteArrayOf(0b00000001, 0, 0, 0)
            val hr = data.getInt(null, DataType.HEART_RATE, 0)
            heartrateCharacteristic.setValue(
                hr,
                BluetoothGattCharacteristic.FORMAT_UINT16,
                1
            )
            currentValue[heartrateCharacteristic.uuid] = heartrateCharacteristic.value;

            //LLog.d("Set value ${heartrateCharacteristic.value.toHex()}") //, Globals.logHistory)
            LLog.d("Sending HR $hr bpm")
            serviceDelegate?.sendNotification(heartrateCharacteristic)
        }
    }

    override fun writeCharacteristic(characteristic: BluetoothGattCharacteristic, offset: Int, value: ByteArray): Int {
        if (offset != 0) return BluetoothGatt.GATT_INVALID_OFFSET
        if (value.size != 1) return BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH

        if (value[0].toInt() and 1 == 1) {
            LLog.d("Characteristic written: ${value.toHex()}")
//            runInMain {
//                mHeartRateMeasurementCharacteristic.setValue(INITIAL_EXPENDED_ENERGY, EXPENDED_ENERGY_FORMAT, 2);
//            }
        }
        return BluetoothGatt.GATT_SUCCESS
    }

    override fun notificationsDisabled(characteristic: BluetoothGattCharacteristic) {
        if (characteristic.uuid != GattAttribute.HEART_RATE_MEASUREMENT.uuid) return
        LLog.d("Notifications disabled")
    }

    override fun notificationsEnabled(characteristic: BluetoothGattCharacteristic, indicate: Boolean) {
        if (characteristic.uuid != GattAttribute.HEART_RATE_MEASUREMENT.uuid) return
        if (indicate) return
        LLog.d("Notifications enabled")
    }
}