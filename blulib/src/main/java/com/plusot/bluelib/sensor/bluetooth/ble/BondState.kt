package com.plusot.bluelib.sensor.bluetooth.ble

import android.bluetooth.BluetoothDevice

/**
 * Package: com.plusot.bluelib.sensor.bluetooth.ble
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-09-10.
 * Copyright © 2019 Plusot. All rights reserved.
 */

enum class BondState(val intState: Int) {
    BOND_UNKNOWN(-1),
    BOND_NONE(BluetoothDevice.BOND_NONE),
    BOND_BONDING(BluetoothDevice.BOND_BONDING),
    BOND_BONDED(BluetoothDevice.BOND_BONDED),
    ;

    companion object {
        fun fromIntState(intState: Int): BondState = values().filter { it.intState == intState }.firstOrNull() ?: BOND_UNKNOWN

    }
}

fun Int.toBondState() = BondState.fromIntState(this)