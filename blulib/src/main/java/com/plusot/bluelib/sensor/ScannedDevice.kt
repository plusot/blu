package com.plusot.bluelib.sensor

import com.plusot.bluelib.preferences.PreferenceHelper
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.Tuple

import java.util.*

abstract class ScannedDevice(val type: DeviceType): Device.Listener {
    var scanCount = 0.0
        private set
    val deviceData = Data()
    private var _timeRead = -1L

    protected var supported = EnumSet.noneOf(DataType::class.java)
    private var selected: Boolean? = null
    var reconnectCount = 0
    var rssi = Integer.MIN_VALUE
        get() = if (field == Integer.MIN_VALUE) 0 else field

    val dataTypes: Array<DataType>
        get() = deviceData.dataTypes

    val supportedDataTypes: EnumSet<DataType>
        get() {
            var count = supported.size
            if (supported.isEmpty()) {
                var list: List<Int>?
                if (supportedTypesPersistant)
                    list = PreferenceHelper.getIntList("Supported_$address")
                else {
                    list = supportedTypes["Supported_$address"]
                    if (list == null) supportedTypes["Supported_$address"] = ArrayList()
                }
                if (list != null && list.size > 0)
                    for (index in list) {
                        val type = DataType.fromInt(index)
                        if (type != null) supported.add(type)
                    }
                count = supported.size
            }
            supported.addAll(deviceData.supportedDataTypes)
            if (supported.size > count) {
                val list = ArrayList<Int>()
                for (type in supported) list.add(type.ordinal)
                PreferenceHelper.setIntList("Supported_$address", list)
                if (!supportedTypesPersistant) supportedTypes["Supported_$address"] = list
            }
            return supported
        }

    var timeRead: Long
        get() = if (_timeRead > 0L) _timeRead else deviceData.getTime()
        set(value) { _timeRead = value }

    open var isSelected: Boolean
        get() {
            if (selected == null) {
                if (readSelectedFromPreferences) {
                    selected = PreferenceHelper[ScannedDevice::class.java.simpleName + "_" + SELECTED + "_" + name + "_" + address, false]
                        if (selected == false) {
                            selected = PreferenceHelper[ScannedDevice::class.java.simpleName + "_" + SELECTED + "_" + address, false]
                        }
                } else {
                    selected = false
                }
                selected?.let { selectedMap[name + "_" + address] = it }
            }
            return selected!!
        }
        set(selected) {
            if (this.selected == null || this.selected != selected) {
                PreferenceHelper[ScannedDevice::class.java.simpleName + "_" + SELECTED + "_" + name + "_" + address] = selected
                selectedMap[name + "_" + address] = selected
                this.selected = selected
            }
        }

    abstract val address: String
    abstract val iconId: Int
    abstract val manufacturer: String
    abstract val name: String

    var isBeacon: Boolean = false
        protected set

    override fun toString(): String {
        return name
    }

    fun toNameValueString(): Tuple<String, String> {
        return deviceData.toNameValueString()
    }

    fun hasDataType(type: DataType): Boolean {
        return deviceData.hasDataType(type)
    }

    abstract fun close(source: String)

    fun incScanCount() {
        scanCount += 1.0
    }

//    fun reduceScanCount() {
//        scanCount *= REDUCE_fACTOR
//    }

    companion object {
        var readSelectedFromPreferences = false
        private val SELECTED = "Selected"
        var REDUCE_fACTOR = 0.95
        private val selectedMap = HashMap<String, Boolean>()
        private var supportedTypesPersistant = false
        private val supportedTypes = HashMap<String, List<Int>>()

        fun hasSelected(name: String?): String? {
            if (name == null) return null
            for (key in selectedMap.keys) {
                if (key.toLowerCase().startsWith(name.toLowerCase()) && selectedMap[key] == true) return key
            }
            return null
        }

        fun setSupportedTypesPersistant(usePreferences: Boolean) {
            ScannedDevice.supportedTypesPersistant = usePreferences
        }
    }

    abstract fun resumeDevice()
    abstract fun pauseDevice()
}
