package com.plusot.bluelib.sensor.internal


import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.StringUtil

class ClockDevice(scannedDevice: ScannedDevice) : InternalDevice(DeviceType.INTERNAL_CLOCK, scannedDevice) {
    private var mayRun = true
    private var thread = Thread {
        while (mayRun && Globals.runMode.isRun) {
            try {
                val now = System.currentTimeMillis()
                if (!isActive) {
                    old = 0L
                } else {
                    val data = Data()
                    if (total < 0) total = PreferenceKey.TOTAL_TIME.long
                    if (old != 0L) {
                        active += now - old
                        data.add(address, DataType.TIME_ACTIVITY, active)
                        if (GpsDevice.speed > 2.4 / 3.6) {
                            moving += now - old
                            data.add(address, DataType.TIME_ACTIVE, moving)
                        }

                        total += now - old
                        if (now - totalSaved > 60000) {
                            PreferenceKey.TOTAL_TIME.long = total
                            totalSaved = now
                        }
                    }
                    old = now
                    fireData(
                        data
                            .add(address, DataType.TIME_CLOCK, now)
                            .add(address, DataType.TIME_TOTAL, total)
                    )
                }
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                LLog.d("Clock interrupted")
            }
        }
    }

    init {
        thread.start()
    }

    override fun close(source: String) {
        LLog.i("Closing Clock")
        mayRun = false;
        thread.interrupt()
        if (total > -1) PreferenceKey.TOTAL_TIME.long = total
        super.close(source)
    }

    companion object {
        var moving: Long = 0L
            private set
        var active: Long = 0L
            private set
        private var total: Long = -1L
        private var totalSaved: Long = 0
        private var old: Long = 0


        fun reset() {
            moving = -1
            active = -1
            old = 0
        }

        fun resetTotal() {
            total = 0
        }

        internal val staticAddress: String?
            get() = StringUtil.proper(DeviceType.INTERNAL_CLOCK.toString())
    }
}