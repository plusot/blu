package com.plusot.bluelib.sensor.data

/**
 * Created by peet on 19/10/16.
 */

enum class CommandType {
    INIT,
    START,
    STOP,
    RESET
}
