package com.plusot.bluelib.sensor

import androidx.annotation.CallSuper
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.SleepAndWake
import com.plusot.bluelib.util.runInMain

abstract class Device(protected val scannedDevice: ScannedDevice) {
    private val listeners = mutableSetOf<Listener>()
    protected var isActive = false

    private val data = Data()
//    protected var singleCall = false
    var manufacturer = ""
        protected set
    private val listenerSyncObj = Any()

    abstract val address: String

    enum class StateInfo {
        CONNECTED,
        DISCONNECTED,
        COMS,
        DEAD,
        CLOSED,
        SEARCHING,
        TRACKING,
        PROCESSING_REQUEST,
        WARNING,
        UNRECOGNIZED
    }

    interface Listener {
        fun onDeviceState(device: Device, state: StateInfo, data: Data?) //accuracy: SensorAccuracy?)
        fun onDeviceData(device: Device, data: Data)
        fun onCommandDone(device: Device, commandId: String, data: ByteArray, success: Boolean, closeOnDone: Boolean)
    }

    init {
        synchronized(listenerSyncObj) {
            this.listeners.add(scannedDevice)
        }
        runInMain(200) {  resumeDevice() }
    }

    @CallSuper open fun close(source: String) {
        LLog.d("Close by: $source")
        if (isActive) pauseDevice()
        synchronized(listenerSyncObj) {
            listeners.clear()
        }
    }

    protected fun fireState(state: StateInfo, data: Data? = null) { //, accuracy: SensorAccuracy? = null) {
        SleepAndWake.runInMain(){
            synchronized(listenerSyncObj) {
                for (listener in listeners) listener.onDeviceState(this@Device, state, data)
            }
        }
    }

//    protected fun fireCommandDone(commandId: String, data: ByteArray, success: Boolean) {
//        SleepAndWake.runInMain("Device.fireCommandDone"){
//            synchronized(listenerSyncObj) {
//                for (listener in listeners) listener.onCommandDone(this@Device, commandId, data, success, singleCall)
//            }
//        }
//    }

    @CallSuper
    open fun resumeDevice() {
        isActive = true
        LLog.d("Resuming ${scannedDevice.name}")
    }

    @CallSuper
    open fun pauseDevice() {
        isActive = false
        LLog.d("Pausing ${scannedDevice.name}")
    }

    protected fun fireData(data: Data?, checkEqual: Boolean = true): Boolean {
        if (!isActive) return false
        if (data == null || !data.hasData()) return false
        //if (deviceData.hasDataType(DataType.HEART_RATE)) LLog.i("Data = " + deviceData);
        allData.merge(data)
        if (checkEqual && this.data.mergeCheckEqual(data, false)) {
            //if (DEBUG) LLog.i("Equal deviceData: " + deviceData.toString());
            return false
        }

        synchronized(listenerSyncObj) {
            for (listener in listeners) listener.onDeviceData(this@Device, data)
        }
        return true
    }

    companion object {
        private val allData = Data()

        fun getDoubleValue(type: DataType): Double = allData.getDouble(null, type)
    }
}
