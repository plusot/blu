package com.plusot.bluelib.sensor.bluetooth.ble

enum class GattAttributeType private constructor(private val label: String) {
    SERVICE("Service"),
    CHARACTERISTIC("Characteristic"),
    DESCRIPTOR("Descriptor");

    override fun toString(): String = label
}
