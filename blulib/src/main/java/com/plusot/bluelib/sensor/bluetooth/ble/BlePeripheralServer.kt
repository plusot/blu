package com.plusot.bluelib.sensor.bluetooth.ble

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.util.nextFromList
import java.util.*

/**
 * Package: com.plusot.bluelib.sensor.bluetooth.ble
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-06-11.
 * Copyright © 2019 Plusot. All rights reserved.
 */


class BlePeripheralServer(private val peripherals: List<BlePeripheral>): PeripheralServiceDelegate {
    var gattServer: BluetoothGattServer? = null
    var adapter: BluetoothAdapter? = null
    val remoteDevices = mutableSetOf<BluetoothDevice>()
    var advertiser: BluetoothLeAdvertiser? = null


    private val advCallback = object : AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            when (errorCode) {
                ADVERTISE_FAILED_ALREADY_STARTED -> LLog.e("Not broadcasting. App was already advertising")
                ADVERTISE_FAILED_DATA_TOO_LARGE -> LLog.e("Not broadcasting. Advertising data too large")
                ADVERTISE_FAILED_FEATURE_UNSUPPORTED -> LLog.e("Not broadcasting. Advertising feature not supported")
                ADVERTISE_FAILED_INTERNAL_ERROR -> LLog.e("Not broadcasting. Advertising internal error")
                ADVERTISE_FAILED_TOO_MANY_ADVERTISERS -> LLog.e("Not broadcasting. Too many advertisers")
                else -> LLog.e("Not broadcasting. Unhandled advertising error: $errorCode")
            }
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            LLog.v("Broadcasting")
        }
    }

    private val gattServerCallback = object : BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice, status: Int, newState: Int) {
            LLog.d("Called")
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    remoteDevices.add(device)
                    LLog.v("Connected to device: ${device.address}")
                } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                    remoteDevices.remove(device)
                    LLog.v("Disconnected device: ${device.address}")
                }
            } else {
                remoteDevices.remove(device)
                LLog.v("Disconnected device: ${device.address}")
                LLog.e("Error when connecting: $status")
            }
        }

        override fun onCharacteristicReadRequest(
            device: BluetoothDevice, requestId: Int, offset: Int,
            characteristic: BluetoothGattCharacteristic) {

            val value = getPeripheral(characteristic)?.currentValue?.get(characteristic.uuid)
            LLog.d("Device tried to read characteristic: " + GattAttribute.fromUUID(characteristic.uuid))
            LLog.d("Value: " + Arrays.toString(value))
            if (offset != 0) {
                gattServer?.sendResponse(
                    device, requestId, BluetoothGatt.GATT_INVALID_OFFSET, offset,
                    /* No need to respond with a value */ null
                )/* value (optional) */
                return
            }
            gattServer?.sendResponse(
                device, requestId, BluetoothGatt.GATT_SUCCESS,
                offset, value
            )
        }

//        override fun onNotificationSent(device: BluetoothDevice, status: Int) {
//            super.onNotificationSent(device, status)
//            //LLog.v("Notification sent. Status: $status")
//        }

        override fun onCharacteristicWriteRequest(
            device: BluetoothDevice, requestId: Int,
            characteristic: BluetoothGattCharacteristic, preparedWrite: Boolean, responseNeeded: Boolean,
            offset: Int, value: ByteArray
        ) {
            val status = getPeripheral(characteristic)?.writeCharacteristic(characteristic, offset, value) ?: BluetoothGatt.GATT_FAILURE
            LLog.v("Characteristic Write request: ${Arrays.toString(value)}, status $status")
            if (responseNeeded) {
                gattServer?.sendResponse(
                    device, requestId, status,
                    /* No need to respond with an offset */ 0, null
                )/* No need to respond with a value */
            }
        }

        override fun onDescriptorReadRequest(
            device: BluetoothDevice, requestId: Int,
            offset: Int, descriptor: BluetoothGattDescriptor
        ) {
            LLog.d("Device tried to read descriptor: " + descriptor.uuid)
            LLog.d("Value: " + Arrays.toString(descriptor.value))
            if (offset != 0) {
                gattServer?.sendResponse(
                    device,
                    requestId,
                    BluetoothGatt.GATT_INVALID_OFFSET,
                    offset,
                    null
                )/* value (optional) */
                return
            }
            if (descriptor.uuid == GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.uuid) {
                val returnValue = if (remoteDevices.contains(device)) {
                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                } else {
                    BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
                }
                gattServer?.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS, offset,
                    returnValue
                )
            } else
                gattServer?.sendResponse(
                    device, requestId, BluetoothGatt.GATT_FAILURE, offset,
                    null
                )

        }

        override fun onDescriptorWriteRequest(
            device: BluetoothDevice, requestId: Int,
            descriptor: BluetoothGattDescriptor, preparedWrite: Boolean, responseNeeded: Boolean,
            offset: Int,
            value: ByteArray
        ) {
            val status: Int
            if (descriptor.uuid == GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.uuid) {
                val characteristic = descriptor.characteristic
                val supportsNotifications =
                    characteristic.properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0
                val supportsIndications =
                    characteristic.properties and BluetoothGattCharacteristic.PROPERTY_INDICATE != 0

                if (!(supportsNotifications || supportsIndications)) {
                    status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                } else if (value.size != 2) {
                    status = BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH
                } else if (Arrays.equals(value, BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)) {
                    status = BluetoothGatt.GATT_SUCCESS
                    getPeripheral(characteristic)?.notificationsDisabled(characteristic)
                    descriptor.value = value
                } else if (supportsNotifications && Arrays.equals(
                        value,
                        BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    )
                ) {
                    status = BluetoothGatt.GATT_SUCCESS
                    getPeripheral(characteristic)?.notificationsEnabled(characteristic, false /* indicate */)
                    descriptor.value = value
                } else if (supportsIndications && Arrays.equals(
                        value,
                        BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                    )
                ) {
                    status = BluetoothGatt.GATT_SUCCESS
                    getPeripheral(characteristic)?.notificationsEnabled(characteristic, true /* indicate */)
                    descriptor.value = value
                } else {
                    status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                }
            } else {
                status = BluetoothGatt.GATT_SUCCESS
                descriptor.value = value
            }
            LLog.d("Descriptor Write Request ${descriptor.uuid} ${value.contentToString()}, status $status")
            if (responseNeeded) {
                gattServer?.sendResponse(
                    device, requestId, status,
                    /* No need to respond with offset */ 0, null
                )
            }
        }

        override fun onServiceAdded(status: Int, service: BluetoothGattService?) {
            LLog.d("Service add ${GattAttribute.fromUUID(service?.uuid)} = ${GattState.fromId(status)}")

            val peripheral = peripherals.nextFromList(peripherals.firstOrNull { it.service.uuid == service?.uuid })
            if (peripheral != null) gattServer?.addService(peripheral.service)
        }
    }


    init {
        peripherals.forEach { it.serviceDelegate = this }
        startServer()
    }

    private fun startServer() {
        val context = Globals.appContext ?: return
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
        if (bluetoothManager != null) {
            adapter = bluetoothManager.adapter
            gattServer = bluetoothManager.openGattServer(context, gattServerCallback)
            if (gattServer == null) {
                //TODO: Throw Bluetooth dialog
                LLog.e("Please check Bluetooth settings")
            } else peripherals.firstOrNull()?.service?.let {
                gattServer?.addService(it)
            }

            val builder = AdvertiseData.Builder().setIncludeTxPowerLevel(true)
            peripherals.forEach { builder.addServiceUuid(it.getServiceUUID()) }
            val advertiseData = builder.build()

            if (adapter?.isMultipleAdvertisementSupported == true) {
                advertiser = adapter?.bluetoothLeAdvertiser
                advertiser?.startAdvertising(
                    AdvertiseSettings.Builder()
                        .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                        .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                        .setConnectable(true)
                        .build(),
                    advertiseData,
                    AdvertiseData.Builder()
                        .setIncludeDeviceName(true)
                        .build(),
                    advCallback

                )
            } else {
                LLog.e("BLE advertising is not supported")
            }
        } else {
            LLog.d("Peripheral server not started.")
        }
    }

    override fun sendNotification(characteristic: BluetoothGattCharacteristic) {
        val indicate = (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_INDICATE) == BluetoothGattCharacteristic.PROPERTY_INDICATE
        remoteDevices.forEach {
            // true for indication (acknowledge) and false for notification (un acknowledge).
            try {
                if (it.bondState == BondState.BOND_BONDED.intState) gattServer?.notifyCharacteristicChanged(it, characteristic, indicate)
            } catch (e: NullPointerException) {
                LLog.e("Null pointer exception caught", e)
            }
        }
    }

    fun close() {
        gattServer?.close()
        if (adapter?.isEnabled == true && advertiser != null) {
            // If stopAdvertising() gets called before close() a null
            // pointer exception is raised.
            LLog.d("Advertising stopping")
            advertiser?.stopAdvertising(advCallback)
        }
    }

    fun setValue(data: Data) {
        //LLog.d("setValue = $data", Globals.logHistory)
        peripherals.forEach { it.setValue(data) }
    }

    private fun getPeripheral(characteristic: BluetoothGattCharacteristic) =
        peripherals.firstOrNull {
            it.characteristics.firstOrNull { attr -> attr.uuid == characteristic.uuid } != null
        }



}