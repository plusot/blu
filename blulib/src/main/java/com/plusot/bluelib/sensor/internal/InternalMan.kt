package com.plusot.bluelib.sensor.internal

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScanManager
import com.plusot.bluelib.sensor.ScanType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.util.SleepAndWake

/**
 * Package: com.plusot.bluelib.sensor.internal
 * Project: helloWatch
 *
 * Created by Peter Bruinink on 2019-05-20.
 * Copyright © 2019 Plusot. All rights reserved.
 */
class InternalMan : ScanManager {
    private val scannedDevices = mutableMapOf<DeviceType, ScannedInternalDevice>()
    private var scanActive = false

    override val devices: List<ScannedDevice>
        get() = scannedDevices.values.toList()


    override fun close() {
        if (debug) LLog.i("Closing InternalMan")

        for (sd in scannedDevices.values) sd.close("Close method of InternalMan")
        scannedDevices.clear()

        if (debug) LLog.i("Closed InternalMan")
        instance = null
    }

    fun addScannedDevice(type: DeviceType) {
        val sd = scannedDevices[type]
        if (sd == null) {
            val newSd = ScannedInternalDevice(type)
            scannedDevices[type] = newSd
            BlueLib.fireScanned(newSd, true)
        } else
            BlueLib.fireScanned(sd, false)

    }
    override fun startScan(): Boolean {
        val sensorManager = Globals.appContext?.getSystemService(Context.SENSOR_SERVICE) as SensorManager? ?: return false
        scanActive = true
        BlueLib.fireScanning(ScanType.INTERNAL, true)


        val sensors = sensorManager.getSensorList(Sensor.TYPE_ALL)
        sensors.forEach {
            val type = DeviceType.fromSensorType(it.type)
            if (type != null) addScannedDevice(type)
        }

        if (GpsDevice.available()) addScannedDevice(DeviceType.INTERNAL_GPS)
        addScannedDevice(DeviceType.INTERNAL_BATTERY_SENSOR)
        addScannedDevice(DeviceType.INTERNAL_CLOCK)
        addScannedDevice(DeviceType.INTERNAL_MEMORY_SENSOR)

//        scannedDevices[InternalDevice.InternalType.BATTERY_SENSOR] =
//        scannedDevices[InternalDevice.InternalType.MEMORY_SENSOR] =

        SleepAndWake.runInMain (1000){ stopScan("Sleeper") }
        return true
    }

    override fun stopScan(comment: String) {
//        LLog.d("Called stopScan from $comment while scanning = $scanActive")

        if (!scanActive) return
        scanActive = false
        BlueLib.fireScanning(ScanType.INTERNAL, false)
    }

    companion object {
        private var debug = false
        private var instance: InternalMan? = null

        fun getInstance(): InternalMan = instance ?: synchronized(this) {
            InternalMan().also {
                instance = it
            }
        }

    }
}
