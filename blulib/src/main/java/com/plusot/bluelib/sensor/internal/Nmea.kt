package com.plusot.bluelib.sensor.internal

enum class Nmea(private val label: String) {
    GPGGA("\$GPGGA"),
    GNGGA("\$GNGGA"),
    GPGSV("\$GPGSV");

    companion object {

        fun fromString(value: String): Nmea? {
            for (nmea in Nmea.values()) if (nmea.label == value) return nmea
            return null
        }
    }
}