package com.plusot.bluelib.sensor.bluetooth


import com.plusot.bluelib.util.StringUtil
import com.plusot.bluelib.util.Tuple

/**
 * Created by peet on 2/11/16.
 */

enum class ScanRecordType private constructor(private val id: Byte) {
    DATA_TYPE_FLAGS(0x01.toByte()),
    DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL(0x02.toByte()),
    DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE(0x03.toByte()),
    DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL(0x04.toByte()),
    DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE(0x05.toByte()),
    DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL(0x06.toByte()),
    DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE(0x07.toByte()),
    DATA_TYPE_LOCAL_NAME_SHORT(0x08.toByte()) {
        override fun parse(record: ByteArray, pos: Int, len: Int): String {
            return StringUtil.toReadableString(record, pos, len)
        }
    },
    DATA_TYPE_LOCAL_NAME_COMPLETE(0x09.toByte()) {
        override fun parse(record: ByteArray, pos: Int, len: Int): String {
            return StringUtil.toReadableString(record, pos, len)
        }
    },
    DATA_TYPE_TX_POWER_LEVEL(0x0A.toByte()),
    DATA_TYPE_DEVICE_CLASS(0x0D.toByte()),
    DATA_TYPE_SIMPLE_PAIRING_HASH_C192(0x0E.toByte()),
    DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R192(0x0F.toByte()),
    DATA_TYPE_DEVICE_ID_OR_SECURITY_TK(0x10.toByte()),
    DATA_TYPE_SECURITY_OUT_OF_BANDS_FLAG(0x11.toByte()),
    DATA_TYPE_SLAVE_CONNECTION_INTERVAL_RANGE(0x12.toByte()),
    DATA_TYPE_SOLICITATION_UUID_16_BIT(0x14.toByte()),
    DATA_TYPE_SOLICITATION_UUID_128_BIT(0x15.toByte()),
    DATA_TYPE_SERVICE_DATA_16_BIT(0x16.toByte()),
    DATA_TYPE_PUBLIC_TARGET(0x17.toByte()),
    DATA_TYPE_RANDOM_TARGET(0x18.toByte()),
    DATA_TYPE_APPEARANCE(0x19.toByte()),
    DATA_TYPE_ADVERTISING_INTERVAL(0x1A.toByte()),
    DATA_TYPE_BLE_ADDRESS(0x1B.toByte()),
    DATA_TYPE_LE_ROLE(0x1C.toByte()),
    DATA_TYPE_SIMPLE_PAIRING_HASH_C256(0x1D.toByte()),
    DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R256(0x1E.toByte()),
    DATA_TYPE_SOLICITATION_UUID_32_BIT(0x1F.toByte()),
    DATA_TYPE_SERVICE_DATA_32_BIT(0x20.toByte()),
    DATA_TYPE_SERVICE_DATA_128_BIT(0x21.toByte()),
    DATA_TYPE_LE_SECURE_CONFIRMATION(0x22.toByte()),
    DATA_TYPE_LE_SECURE_RANDOM(0x23.toByte()),
    DATA_TYPE_URI(0x24.toByte()),
    DATA_TYPE_INDOOR_POSITIONING(0x25.toByte()),
    DATA_TYPE_TRANSPORT_DISCOVERY(0x26.toByte()),
    DATA_TYPE_3D_INFO(0x3D.toByte()),
    DATA_TYPE_PLUSOT_MTAG(0xFC.toByte()),
    DATA_TYPE_PLUSOT_SCANDATA(0xFD.toByte()),
    DATA_TYPE_PLUSOT_SOAPDATA(0xFE.toByte()),
    DATA_TYPE_MANUFACTURER_SPECIFIC_DATA(0xFF.toByte()),
    UNKNOWN(0.toByte());

    open fun parse(record: ByteArray, pos: Int, len: Int): String {
        return StringUtil.toHex(record, pos, len)
    }

    companion object {

        fun fromId(id: Byte): ScanRecordType {
            for (type in values())
                if (id == type.id) return type
            return UNKNOWN
        }

        fun tupleFromId(id: Byte): Tuple<ScanRecordType, String> {
            val type = fromId(id)
            return if (type !== UNKNOWN) Tuple(type, type.toString()) else Tuple(UNKNOWN, StringUtil.toHexString(id))
        }
    }
}
