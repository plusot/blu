package com.plusot.bluelib.permissions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.dialog.Alerts
import com.plusot.bluelib.dialog.ClickResult
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey
import com.plusot.bluelib.sensor.bluetooth.ble.BleMan
import com.plusot.bluelib.util.ToastHelper
import com.plusot.bluelib.util.runInMain
import java.util.*

/**
 * Package: nl.mytenga.robin.util
 * Project: robin
 *
 * Created by Peter Bruinink on 18-08-18.
 * Copyright © 2018 Plusot. All rights reserved.
 */

enum class PermissionEvent {
    COMPLETED,
    INCOMPLETE,
    BLUETOOTH_ON
}

class Permit(val permission: String) {

    val isGranted: Boolean
        get() {
            val context = Globals.appContext ?: return false
            if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, permission)) return true
            return false
        }
}

class PermissionNullActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_null)

        val permissions = intent?.getStringArrayExtra(PermitQuery.PERMISSIONS)
        if (permissions != null) {
            ActivityCompat.requestPermissions(this, permissions, PermitQuery.PERMISSIONS_REQUEST)
        }

        val specialPermissions = intent?.getStringArrayExtra(PermitQuery.SPECIAL_PERMISSIONS)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return
        if (specialPermissions != null) for (special in specialPermissions) when (special) {
            PermitQuery.OVERLAY_PERMISSION -> {
                val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
                startActivityForResult(intent, PermitQuery.ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE)
            }
            PermitQuery.WRITE_SETTINGS_PERMISSION -> {
                val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                startActivityForResult(intent, PermitQuery.ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE)
//            LLog.e("No permission to write settings yet")
            }
            PermitQuery.UNKNOWN_APP_PERMISSION -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val intent = Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES).setData(Uri.parse("package:$packageName"))
                startActivityForResult(intent, PermitQuery.ACTION_UNKNOWN_APP_PERMISSION_REQUEST_CODE)
            }

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermitQuery.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        PermitQuery.onActivityResult(this, resultCode)
        super.onActivityResult(requestCode, resultCode, data)
        finish()
    }

}

object PermitQuery {
    const val PERMISSIONS_REQUEST = 8002
    const val ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 8004
    const val ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE = 8005
    const val ACTION_UNKNOWN_APP_PERMISSION_REQUEST_CODE = 8006
    const val UNKNOWN_APP_PERMISSION = "UNKNOWN_APP_PERMISSION"
    const val WRITE_SETTINGS_PERMISSION = "WRITE_SETTINGS_PERMISSION"
    const val OVERLAY_PERMISSION = "OVERLAY_PERMISSION"

    const val PERMISSIONS = "permissions"
    const val SPECIAL_PERMISSIONS = "special_permissions"

    private var onPermitAccepted: ((PermissionEvent) -> Unit)? = null

    private fun hasEnoughPermits(activity: Activity, permits: List<Permit>, specialPermissions: List<String>?): Boolean {
        val permissionsToRequest = mutableListOf<String>()
        permits.forEach { if (!it.isGranted) permissionsToRequest.add(it.permission) }

        var needsSpecial = false
        if (specialPermissions != null) for (special in specialPermissions) {
            when (special) {
                UNKNOWN_APP_PERMISSION -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !activity.packageManager.canRequestPackageInstalls()) needsSpecial = true
                WRITE_SETTINGS_PERMISSION -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(activity)) needsSpecial = true
                OVERLAY_PERMISSION -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.System.canWrite(activity)) needsSpecial = true
            }
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            PreferenceKey.PERMISSIONS_ACCEPTED.isTrue = true
            return true
        }
        return !(!PreferenceKey.PERMISSIONS_ACCEPTED.isTrue || permissionsToRequest.size > 0 || needsSpecial)
    }

    fun askPermission(context: Context, permissions: List<String>, specialPermissions: List<String>?, listener: ((PermissionEvent) -> Unit)?) {
        onPermitAccepted = listener
        val intent = Intent(context, PermissionNullActivity::class.java)
        intent.putExtra(PERMISSIONS, permissions.toTypedArray())
        if (specialPermissions != null) intent.putExtra(SPECIAL_PERMISSIONS, specialPermissions.toTypedArray())
        context.startActivity(intent)
    }

    fun checkPermission(activity: Activity, permissions: List<String>, specialPermissions: List<String>? = null, listener: ((PermissionEvent) -> Unit)?) {
        val permits = mutableListOf<Permit>()
        onPermitAccepted = listener
        permissions.forEach { permits.add(Permit(it)) }

        if (hasEnoughPermits(activity, permits, specialPermissions)) runInMain {
            onPermitAccepted?.invoke(PermissionEvent.COMPLETED)
        } else {
            Alerts.showYesNoDialog(activity, R.string.askPermissionTitle, R.string.askPermissionMsg)  {
                //var handled = false
                when (it) {
                    ClickResult.YES -> {
                        //handled = true
                        PreferenceKey.PERMISSIONS_ACCEPTED.isTrue = true
                        askPermission(activity, permissions, specialPermissions, listener)
                    }
                    ClickResult.NO, ClickResult.CANCEL, ClickResult.NEUTRAL -> //if (!handled)
                        Alerts.showOkDialog(
                            activity,
                            R.string.permissions_not_accepted_title,
                            R.string.permissions_not_accepted_msg,
                            0L
                        ) { activity.finish() }
                    ClickResult.DISMISS -> {}
                }

            }
        }
    }

    fun onRequestPermissionsResult(activity: Activity, requestCode: Int, permissions: Array<String>, grantResults: IntArray?) {
        when (requestCode) {
            PERMISSIONS_REQUEST -> {
                if (grantResults != null && grantResults.isNotEmpty()) {
                    var count = 0
                    for (result in grantResults)
                        if (result == PackageManager.PERMISSION_GRANTED) count++
                    LLog.d("Permissions granted: $count")
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasEnoughPermits(activity, permissions.toList().map { Permit(it) }, listOf()))
                    onPermitAccepted?.invoke(PermissionEvent.INCOMPLETE)
                else
                    onPermitAccepted?.invoke(PermissionEvent.COMPLETED)

            }

        }
        onPermitAccepted = null
    }

    fun onActivityResult(activity: Activity?, resultCode: Int) {
        when (resultCode) {
            BleMan.REQUEST_ENABLE_BT -> {
                ToastHelper.showToastLong(R.string.ble_enabled)
                onPermitAccepted?.invoke(PermissionEvent.BLUETOOTH_ON)
            }
            ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                LLog.d("Overlays granted = " + Settings.canDrawOverlays(activity))
            }
            ACTION_MANAGE_WRITE_SETTINGS_PERMISSION_REQUEST_CODE -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                LLog.d("Write settings = " + Settings.System.canWrite(activity))
            }
            ACTION_UNKNOWN_APP_PERMISSION_REQUEST_CODE -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                LLog.d("Unknown apps = " + activity?.packageManager?.canRequestPackageInstalls())
            }
        }


    }

}
