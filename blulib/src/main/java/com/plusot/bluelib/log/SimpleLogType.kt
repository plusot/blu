package com.plusot.bluelib.log

enum class SimpleLogType(
    val beforeLines: String?,
    val betweenLines: String?,
    val behindLines: String?,
    val sep: String,
    val ext: String,
    val isAppend: Boolean,
    val isWriteOnSessionOnly: Boolean
) {
    JSON(null, ",", null, "", ".json", true, true),
    TEXT(null, null, null, "", ".txt", true, false),
    CSV(null, null, null, "", ".csv", true, true),
    HRV(null, null, null, "", ".hrv", true, true),
    LOG(null, null, null, "", ".txt", true, true),
    ARGOS("Minutes,Torq (N-m),Km/h,Watts,Km,Cadence,Hrate,ID,Altitude (m)", null, null, ",", ".csv", true, true)


}
