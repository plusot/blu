package com.plusot.bluelib.log

import android.util.Log
import com.plusot.bluelib.Globals
import com.plusot.bluelib.util.TimeUtil

import java.io.*
import java.util.ArrayList
import java.util.HashMap

class SimpleLog(private val type: SimpleLogType, private val spec: String) {
    private var writer: PrintWriter? = null
    private var logFilename: String? = null
    private var closeing = false
    private var timeFlushed: Long = 0

    private val folder: String
        get() = if (spec == LLog.SPEC) Globals.logPath else Globals.dataPath

    private fun getFileName(session: String?): String? {
        return if (session == null) logFilename else folder + spec + Globals.FILE_DETAIL_DELIM + session + type.ext
    }

    private fun open(): Boolean {
        if (Globals.runMode.isFinished) return false
        var isNew = true

        var file = File(folder)
        if (!file.isDirectory && !file.mkdirs()) return false
        val localSession: String = session

        logFilename = folder + spec + Globals.FILE_DETAIL_DELIM + localSession + type.ext
        logFilename?.let { logFilename ->
            file = File(logFilename)
            try {
                if (type.isAppend)
                    isNew = file.createNewFile()
                if (isNew)
                    Log.d(Globals.TAG, "$CLASSTAG.open: Opening new file $logFilename")
                else
                    Log.d(Globals.TAG, "$CLASSTAG.open: Appending to $logFilename")
                writer = PrintWriter(BufferedWriter(FileWriter(file, type.isAppend)))
                if (isNew && type.beforeLines != null) {
                    writer?.println(type.beforeLines)
                }
            } catch (e: IOException) {
                Log.e(Globals.TAG, "$CLASSTAG.open: Error creating file", e)
                writer = null
            }
        }

        return isNew
    }

    fun log(map: Map<String, String>) {
        if (closeing) return
        if (type.isWriteOnSessionOnly) return
        if (type == SimpleLogType.JSON) {
            val sb = StringBuilder()
            for (key in map.keys) {
                if (sb.length > 0) sb.append(",")
                sb.append("\"").append(key).append("\":").append(map[key])
            }
            log("{$sb}")
        }
    }

    fun log(logStr: String) {
        if (closeing) return
        if (type.isWriteOnSessionOnly) return
        var isNew = false
        synchronized(this) {
            if (writer != null && writer?.checkError() == true) {
                Log.d(Globals.TAG, "$CLASSTAG.log: Closing & opening writer due to error.")
                writer?.close()
                writer = null
            }
            if (writer == null) {
                if (open() && writer != null) {
                    isNew = true
                }
            }
            if (writer == null) {
                Log.d(Globals.TAG, "$CLASSTAG.saveCall: Could not write: $logStr")
            } else {
                //recordsWritten++;
                if (!isNew && type.betweenLines != null)
                    writer?.println(type.betweenLines)
                writer?.println(logStr)
                flush(false)
            }
        }
    }

    fun log(logStr: String, e: Throwable) {
        if (closeing) return
        if (type.isWriteOnSessionOnly) return
        var isNew = false
        synchronized(this) {
            if (writer != null && writer?.checkError() == true) {
                Log.d(Globals.TAG, "$CLASSTAG.log: Closing & opening writer due to error.")
                writer?.close()
                writer = null
            }
            if (writer == null) {
                if (open() && writer != null) {
                    isNew = true
                }
            }
            if (writer == null) {
                Log.d(Globals.TAG, "$CLASSTAG.saveCall: Could not write: $logStr")
            } else writer?.let {writer ->
                //recordsWritten++;
                if (!isNew && type.betweenLines != null)
                    writer.println(type.betweenLines)
                writer.println(logStr)
                e.printStackTrace(writer)
                flush(true)
            }
        }
    }

    private fun flush(force: Boolean) {
        val now = System.currentTimeMillis()
        if (!force && now - timeFlushed < Globals.FLUSH_TIME) return
        if (writer != null) writer?.flush()
        timeFlushed = System.currentTimeMillis()
    }

    fun close() {
        synchronized(this) {
            try {
                closeing = true
                Log.d(Globals.TAG, "Closing log file")
                writer?.let {
                    if (type.behindLines != null) it.println(type.behindLines)
                    it.close()
                }
                writer = null
                logFilename = null
            } finally {
                closeing = false
            }
        }
    }

    companion object {
        private val CLASSTAG = SimpleLog::class.java.simpleName
        private val instances = HashMap<String, SimpleLog>()
        val session = TimeUtil.formatFileTime()

        fun stopInstances() {
            if (instances.size > 0) for (instance in instances.values) synchronized(this) {
                instance.close()
            }
            instances.clear()
        }

        fun hasInstances(): Boolean {
            return instances.size > 0
        }

        fun getInstance(type: SimpleLogType, spec: String): SimpleLog? {
            if (!Globals.runMode.isRun) return null
            var instance: SimpleLog?
            synchronized(this) {
                instance = instances.getOrPut(spec, { SimpleLog(type, spec) })
            }
            return instance
        }

        fun closeInstances() {
            synchronized(this) {
                for (instance in instances.values) {
                    instance.close()
                }
            }
        }

        fun getFileNames(session: String?): List<String> {
            val list = ArrayList<String>()
            synchronized(this) {
                for (instance in instances.values) {
                    val name = instance.getFileName(session)
                    if (name != null && File(name).exists()) list.add(name)
                }
            }
            return list
        }
    }

}
