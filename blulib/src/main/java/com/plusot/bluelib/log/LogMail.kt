package com.plusot.bluelib.log

import android.Manifest
import android.accounts.AccountManager
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.plusot.bluelib.Globals
import com.plusot.bluelib.util.FileExplorer
import com.plusot.bluelib.util.FileUtil
import com.plusot.bluelib.util.TimeUtil
import com.plusot.bluelib.util.ToastHelper

import java.io.File
import java.util.ArrayList
import java.util.HashSet

typealias MailCompleteListener = () -> Unit

class LogMail {

    private var listener: MailCompleteListener? = null
    private val mailContents: String
    private val sending: String
    private val no_mail: String
    private val address: String


    interface Listener {
        fun onMailComplete()
    }

    constructor(mailContents: String, sending: String, no_mail: String, listener: MailCompleteListener) {
        this.listener = listener
        //		this.context = context;
        this.mailContents = mailContents
        this.sending = sending
        this.no_mail = no_mail
        this.address = PLUSOT_MAIL

        send()
    }

    constructor(mailContents: String, idSending: Int, idNoMail: Int, listener: MailCompleteListener) {
        val appContext = Globals.appContext
        this.listener = listener
        this.mailContents = mailContents
        if (appContext != null) {
            this.sending = appContext.getString(idSending)
            this.no_mail = appContext.getString(idNoMail)
        } else {
            this.sending = "Sending"
            this.no_mail = "No mail"
        }
        this.address = PLUSOT_MAIL
        send()
    }

    private inner class Result internal constructor(internal val success: Boolean)

    private inner class SendMailTask : AsyncTask<Void, Void, Result>() {
        override fun doInBackground(vararg params: Void): Result {
            val appContext = Globals.appContext ?: return Result(false)

            val files = createFileList()
            for (file in files) {
                LLog.i("ListFiles: $file")
            }

            var name: String? = null
            if (ActivityCompat.checkSelfPermission(
                    appContext,
                    Manifest.permission.GET_ACCOUNTS
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val manager = AccountManager.get(appContext)
                //Account[] accounts = manager.getAccounts();

                var accounts = manager.getAccountsByType("com.google") //manager.getAccounts();
                if (accounts.isEmpty())
                    accounts = manager.getAccountsByType("com.google.android.exchange")
                if (accounts.isEmpty())
                    accounts = manager.accounts
                for (account in accounts) {
                    LLog.i(".accounts: " + account.name + ", " + account.type)
                }
                if (accounts.isNotEmpty()) name = accounts[0].name
            }
            val success = mailMe(
                arrayOf(address),
                name,
                Globals.TAG + "_" + TimeUtil.formatTime(System.currentTimeMillis()),
                mailContents,
                files
            )

            return Result(success)
        }

        override fun onPostExecute(result: Result) {
            if (!result.success) ToastHelper.showToastLong(no_mail)
            listener?.invoke()
        }
    }

    private fun mailMe(
        emailTo: Array<String>,
        emailCC: String?,
        subject: String,
        emailText: String,
        filePaths: Array<String>
    ): Boolean {
        val appContext = Globals.appContext
        val emailIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        emailIntent.type = "text/plain"
        //emailIntent.setType("application/zip"); message/rfc822
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo)
        if (emailCC != null) emailIntent.putExtra(Intent.EXTRA_CC, arrayOf(emailCC))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //		ArrayList<String> list = new ArrayList<String>();
        //		list.addWatchable(emailText);
        //		Log.i(" Email text = " + emailText);
        //		emailIntent.putStringArrayListExtra(android.content.Intent.EXTRA_TEXT, list); // emailText);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText)
        //has to be an ArrayList
        val uris = ArrayList<Uri>()
        //convert from paths to Android friendly Parcelable Uri's
        for (file in filePaths) {
            LLog.i("File $file")
            val fileIn = File(file)
            val uri: Uri
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M && appContext != null) {
                uri = FileProvider.getUriForFile(
                    appContext,
                    appContext.packageName + ".provider",
                    fileIn
                )
            } else
                uri = Uri.fromFile(fileIn)
            uris.add(uri)
        }
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris)
        return if (appContext != null)
            try {
                emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                appContext.startActivity(emailIntent) //Intent.createChooser(emailIntent, "Send mail..."));
                true
            } catch (e: Exception) {
                LLog.e(" Could not send email ", e)
                false
            }
        else
            false
    }

    private fun send() {
        ToastHelper.showToastShort(sending)
        SendMailTask().execute()
    }

    companion object {
        private val PLUSOT_MAIL = "report@plusot.com"
        private val filesLock = Any()

        private fun createFileList(): Array<String> {
            val result: Array<String>
            synchronized(filesLock) {
                val now = System.currentTimeMillis()
                val files = HashSet<String>()
                FileUtil.systemLogToFile()
                var exp = FileExplorer(Globals.logPath, Globals.SYSLOG_SPEC, Globals.LOG_EXT, 0)
                exp.getFileList(now)?.map{ Globals.logPath + it }?.let { files.addAll(it) }

                exp = FileExplorer(Globals.logPath, Globals.APPLOG_SPEC, Globals.LOG_EXT, 0)
                exp.getFileList(now)?.map{ Globals.logPath + it }?.let { files.addAll(it) }

                SimpleLog.closeInstances()
                result = files.toTypedArray()
            }
            //		if (zipIt) {
            //			String zipFile = getZipFileName(timePart(), false);
            //			FileUtil.toZip(result, zipFile);
            //			LLog.i(".zipFile: Zipped new files to " + zipFile);
            //			result = new String[]{ zipFile };
            //		}
            return result
        }
    }
}
