package com.plusot.bluelib.log

import android.util.Log
import com.plusot.bluelib.Globals
import com.plusot.bluelib.util.TimeUtil
import java.util.*

object LLog {
    private val history = HashMap<String, Long>()
    private val mode = Mode.NORMAL
    var debugAsInfo = false

    val method: String
        get() = getMethod(4)

    val SPEC = "applog"

    enum class Mode {
        NORMAL,
        VERBOSE,
        SHORT
    }

    fun getMethod(level: Int = 4): String {
        if (mode == Mode.SHORT) return ""
        val current = Thread.currentThread()
        val stack = current.stackTrace
        val sb = StringBuilder()
        var className: String
        if (level >= 0 && stack.size >= level + 1 && !stack[level].isNativeMethod) {
            when (mode) {
                Mode.VERBOSE -> className = stack[level].className
                Mode.NORMAL -> className = stack[level].fileName.replace(".java", "")
                else -> className = stack[level].fileName.replace(".java", "")
            }
            val lineNumber = stack[level].lineNumber
            val methodName = stack[level].methodName
            if (sb.isNotEmpty()) sb.append('/')
            sb.append("$className.$methodName($lineNumber)")
        } else {
            stack.forEachIndexed { index, element ->
                if (!element.isNativeMethod) {
                    when (mode) {
                        LLog.Mode.VERBOSE -> className = element.className
                        LLog.Mode.NORMAL -> className = element.fileName.replace(".java", "")
                        else -> className = element.fileName.replace(".java", "")
                    }
                    val lineNumber = element.lineNumber
                    val methodName = element.methodName
                    if (sb.isNotEmpty()) sb.append('/')
                    sb.append("$className.$methodName($index:$lineNumber)")
                }
            }
            //sb.append(":")
        }
        return sb.toString()

    }

    private fun save(level: Char, tag: String, msg: String, tw: Throwable?, historyTimeOut: Long): Boolean {
        if (historyTimeOut >= 0) {
            val time = history[msg]
            val now = System.currentTimeMillis()
            if (time != null && (historyTimeOut == 0L || now - time < historyTimeOut)) return false
            if (history.size > 1000) {
                var oldestMsg: String? = null
                var oldest = java.lang.Long.MAX_VALUE
                for (oldMsg in history.keys) history[oldMsg] ?.let {
                    if (it < oldest) {
                        oldest = it
                        oldestMsg = oldMsg
                    }
                }
                oldestMsg?.let { history.remove(it) }
            }
            history[msg] = now
        }

        val log = SimpleLog.getInstance(SimpleLogType.LOG, SPEC) ?: return true
        if (tw == null)
            log.log(TimeUtil.formatTime(decimals = 3) + ';'.toString() + level + ';'.toString() + tag + ';'.toString() + msg)
        else
            log.log(
                TimeUtil.formatTime(decimals = 3) + ';'.toString() + level + ';'.toString() + tag + ';'.toString() + msg,
                tw
            ) //tw.getMessage());
        return true
    }

    fun v(msg: String, tw: Throwable) {
        v(Globals.TAG, getMethod(4) + ": " + msg, tw, -1)
    }

    fun v(msg: String) {
        v(Globals.TAG, getMethod(4) + ": " + msg, null, -1)
    }

    private fun v(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (save('v', tag, msg, tw, checkHistory)) Log.v(tag, msg, tw)
    }

    //    public static void v(String tag, String msg) {
    //        v(tag, msg, null, false);
    //    }

    private fun w(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        save('w', tag, msg, tw, checkHistory)
        Log.w(tag, msg, tw)
    }

    //    public static void w(String tag, String msg) {
    //        w(tag, msg, null, false);
    //    }

    fun w(msg: String) {
        w(Globals.TAG, getMethod(4) + ": " + msg, null, -1)
    }


    private fun d(tag: String, msg: String, tw: Throwable?, checkHistory: Long): Boolean {
        val result =  save('d', tag, msg, tw, checkHistory)
        if (result) {
            if (debugAsInfo)
                Log.i(tag, msg, tw)
            else
                Log.d(tag, msg, tw)
        }
        return result
    }

    fun d(msg: String) {
        d(Globals.TAG, getMethod(4) + ": " + msg, null, -1)
    }

    fun d(msg: String, checkHistory: Long) {
        d(Globals.TAG, getMethod(4) + ": " + msg, null, checkHistory)
    }

    fun d(id: Int) {
        val appContext = Globals.appContext ?: return
        d(Globals.TAG, getMethod(4) + ": " + appContext.getText(id), null, -1)
    }

    fun d(msg: String, tw: Throwable) {
        d(Globals.TAG, getMethod(4) + ": " + msg, tw, -1)
    }

    //    public static void d(String tag, String msg) {
    //        d(tag, msg, null, false);
    //    }

    //    public static void d(String tag, String msg, boolean checkHistory) {
    //        d(tag, msg, null, checkHistory);
    //    }

    private fun i(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (save('i', tag, msg, tw, checkHistory)) Log.i(tag, msg, tw)
    }

    fun i(msg: String, tw: Throwable) {
        i(Globals.TAG, getMethod(4) + ": " + msg, tw, -1)
    }

    //    public static void i(String tag, String msg) {
    //        i(tag, msg, null, false);
    //    }

    fun i(msg: String) {
        i(Globals.TAG, getMethod(4) + ": " + msg, null, -1)
    }

    fun i(id: Int) {
        val appContext = Globals.appContext ?: return
        i(Globals.TAG, getMethod(4) + ": " + appContext.getText(id), null, -1)
    }

    private fun e(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (save('e', tag, msg, tw, checkHistory)) Log.e(tag, msg, tw)
    }

    fun e(msg: String, tw: Throwable) {
        e(Globals.TAG, getMethod(4) + ": " + msg, tw, -1)
    }

    fun e(msg: String) {
        e(Globals.TAG, getMethod(4) + ": " + msg, null, -1)
    }

    fun e(msg: String, checkHistory: Long) {
        e(Globals.TAG, getMethod(4) + ": " + msg, null, checkHistory)
    }

    //    public static void e(String tag, String msg) {
    //        e(tag, msg, null, false);
    //    }

}
