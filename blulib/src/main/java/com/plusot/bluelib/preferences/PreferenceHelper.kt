package com.plusot.bluelib.preferences

import android.annotation.TargetApi
import android.content.SharedPreferences
import android.os.Build
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.util.addIfNotEmpty
import com.plusot.bluelib.util.getDefaultSharedPreferences
import com.plusot.bluelib.util.toIntOr
import java.util.*

object PreferenceHelper {
    private var prefs: SharedPreferences? = null

    @Synchronized
    fun getPrefs(): SharedPreferences? {
        if (prefs == null)
            try {
                val appContext = Globals.appContext ?: return null
                prefs = appContext.getDefaultSharedPreferences()

            } catch (e: NoClassDefFoundError) {
                LLog.d("Could not find class")
            }

        return prefs
    }

    internal fun setFromListArray(key: String, value: Int) {
        if (getPrefs() == null) return
        prefs?.let { prefs ->
            val editor = prefs.edit()
            editor.putString(key, value.toString())
            editor.apply()
        }
    }


    internal fun getFromListArray(key: String, defaultValue: Int): Int {
        if (getPrefs() == null) return defaultValue
        if (prefs == null) return defaultValue
        prefs?.let { prefs ->
            if (!prefs.contains(key)) setFromListArray(key, defaultValue)
            return try {
                val str = prefs.getString(key, defaultValue.toString())
                if (str != null) Integer.valueOf(str) else defaultValue
            } catch (e: NumberFormatException) {
                LLog.e("Could not convert " + prefs.getString(key, defaultValue.toString()) + " to integer for " + key)
                defaultValue
            } catch (e: ClassCastException) {
                LLog.e("Could not convert to integer for $key", e)
                defaultValue
            }
        }
        return  defaultValue
    }

    operator fun  get(key: String, defaultValue: String): String {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) {
                val value =  prefs.getString(key, defaultValue)
                if (value != null) return value
            }
            prefs.edit().putString(key, defaultValue).apply()
            return defaultValue

        } ?: return defaultValue
    }

    operator fun get(key: String): String? {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getString(key, null)
            prefs.edit().putString(key, null).apply()
            return null
        } ?: return null
    }

    operator fun get(key: String, defaultValue: Double): Double {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key))
                return prefs.getFloat(key, java.lang.Double.valueOf(defaultValue).toFloat()).toDouble()
            prefs.edit().putFloat(key, defaultValue.toFloat()).apply()
            return defaultValue
        } ?: return defaultValue
    }

    operator fun get(key: String, defaultValue: Float): Float {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getFloat(key, defaultValue)
            prefs.edit().putFloat(key, defaultValue).apply()
            return defaultValue
        } ?: return defaultValue
    }

    operator fun get(key: String, defaultValue: Int): Int {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getInt(key, defaultValue)
            prefs.edit().putInt(key, defaultValue).apply()
            return defaultValue
        } ?: return defaultValue
    }

    private fun ByteArray.toDelimitedString(): String = this.fold("") { acc, value -> "${acc.addIfNotEmpty("|")}$value"}

    private fun String?.toByteArray(): ByteArray {
        if (this == null) return byteArrayOf()
        val parts = this.split('|')
        val array = ByteArray(parts.size)
        parts.forEachIndexed { index, s -> array[index] = s.toIntOr(0).toByte() }
        return array
    }

    operator fun get(key: String, defaultValue: ByteArray): ByteArray {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getString(key, defaultValue.toDelimitedString()).toByteArray()
            prefs.edit().putString(key, defaultValue.toDelimitedString()).apply()
            return defaultValue
        } ?: return defaultValue
    }

    operator fun get(key: String, defaultValue: Long): Long {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getLong(key, defaultValue)
            prefs.edit().putLong(key, defaultValue).apply()
            return defaultValue
        } ?: return defaultValue
    }

    operator fun get(key: String, defaultValue: Boolean): Boolean {
        getPrefs()
        prefs?.let { prefs ->
            if (prefs.contains(key)) return prefs.getBoolean(key, defaultValue)
            prefs.edit().putBoolean(key, defaultValue).apply()
            return defaultValue
        } ?: return defaultValue
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    internal fun getStringSet(key: String, defaultSet: Set<String>?): Set<String>? {
        getPrefs()
        try {
            LLog.i("getStringSet $key")
            prefs?.let { prefs ->
                if (prefs.contains(key)) {
                    val set = HashSet<String>()
                    val storedSet = prefs.getStringSet(key, defaultSet)
                    if (storedSet != null) set.addAll(storedSet)
                    return set
                }
                prefs.edit().putStringSet(key, defaultSet).apply()
                return defaultSet
            } ?: return defaultSet
        } catch (e: ClassCastException) {
            LLog.i("getStringSet Invalid cast")
        }

        var strings: Array<String>? = null
        if (defaultSet == null) {
            val str = get(key)
            if (str != null && str.isNotEmpty()) strings =
                str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            val sb = StringBuilder()
            for (tmp in defaultSet) {
                if (sb.length > 0) sb.append(";")
                sb.append(tmp)
            }
            val str = get(key, sb.toString())
            if (str.isNotEmpty()) strings =
                str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }
        if (strings == null) return defaultSet
        val set = HashSet<String>()
        Collections.addAll(set, *strings)

        return set
    }

    operator fun get(key: String, defaultList: List<String>?): List<String>? {
        getPrefs()
        if (prefs == null) return defaultList

        var strings: Array<String>? = null
        if (defaultList == null) {
            val str = get(key)
            if (str != null && str.isNotEmpty()) strings =
                str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            val sb = StringBuilder()
            for (tmp in defaultList) {
                if (sb.isNotEmpty()) sb.append(";")
                sb.append(tmp)
            }
            val str = get(key, sb.toString())
            if (str.isNotEmpty()) strings =
                str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }
        if (strings == null) return defaultList
        val list = ArrayList<String>()
        Collections.addAll(list, *strings)

        return list
    }

    fun getIntList(key: String): List<Int>? {
        getPrefs()
        if (prefs == null) return null

        var strings: Array<String>? = null
        val str = get(key)
        if (str != null && str.isNotEmpty()) strings =
            str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (strings == null) return null
        val list = ArrayList<Int>()
        for (sNumber in strings)
            try {
                list.add(Integer.decode(sNumber))
            } catch (n: NumberFormatException) {
                //do nothing
            }

        return list
    }

    operator fun get(key: String, index: Int, defaultValue: String?): String? {
        getPrefs()
        if (prefs == null) return defaultValue

        val list = get(key, null as List<String>?)
        if (list == null) {
            set(key, index, defaultValue)
            return defaultValue
        }
        if (index < 0) return defaultValue
        if (index >= list.size) {
            set(key, index, defaultValue)
            return defaultValue
        }
        return list[index]
    }

    fun getStrings(key: String, defaultValue: Array<String>?): Array<String>? {
        getPrefs()
        if (prefs == null) return defaultValue
        if (defaultValue == null) {
            val str = get(key)
            return str?.split(";".toRegex())?.dropLastWhile { it.isEmpty() }?.toTypedArray()
        }
        val sb = StringBuilder()
        for (tmp in defaultValue) {
            if (sb.length > 0) sb.append(";")
            sb.append(tmp)
        }
        val str = get(key, sb.toString())
        return str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    }

    internal fun setStringSet(key: String, set: Set<String>) {
        getPrefs()
        prefs?.edit()?.putStringSet(key, set)?.apply()
    }

    operator fun set(key: String, list: List<String>) {
        getPrefs()
        val sb = StringBuilder()
        for (tmp in list) {
            if (sb.length > 0) sb.append(";")
            sb.append(tmp)
        }
        set(key, sb.toString())
    }


    fun setIntList(key: String, list: List<Int>) {
        getPrefs()
        val sb = StringBuilder()
        for (tmp in list) {
            if (sb.length > 0) sb.append(";")
            sb.append("" + tmp)
        }
        set(key, sb.toString())
    }

    internal fun addToSet(key: String, value: String): Boolean {
        var localSet = getStringSet(key, null)?.toMutableSet()
        if (localSet == null) {
            localSet = HashSet()
        } else if (localSet.contains(value)) return false
        localSet.add(value)
        setStringSet(key, localSet)
        return true
    }

    internal fun deleteFromSet(key: String, value: String) {
        val set = getStringSet(key, null)?.toMutableSet() ?: return
        for (string in set) {
            if (string.startsWith(value)) {
                set.remove(string)
                break
            }
        }
        setStringSet(key, set)
    }

    internal fun getStringFromSet(key: String, index: Int): String? {
        val set = getStringSet(key, null)
        if (set != null) {
            val array = set.toTypedArray()
            if (index >= 0 && index < array.size) return array[index]
        }
        return null
    }

    operator fun set(key: String, index: Int, value: String?) {
        getPrefs()
        var list: MutableList<String>? = get(key, null as List<String>?)?.toMutableList()
        if (list == null) {
            list = ArrayList()
            if (index > 0) for (i in 0 until index) list.add("")
        }
        if (index < 0) {
            if (list.contains(value)) return
            if (value != null) list.add(value)
        } else {
            if (index >= list.size) for (i in list.size..index) list.add("")
            if (value != null) list[index] = value
        }
        val sb = StringBuilder()
        for (tmp in list) {
            if (sb.length > 0) sb.append(";")
            sb.append(tmp)
        }
        set(key, sb.toString())
    }

    //	public static void set(String key, int index, String value) {
    //		_set(key, index, value);
    //	}

    operator fun set(key: String, value: Long) {
        getPrefs()

        prefs?.let { prefs ->
            val editor = prefs.edit()
            editor.putLong(key, value)
            editor.apply()
        }
    }

    operator fun contains(key: String): Boolean {
        getPrefs()
        return prefs?.contains(key) ?: false
    }

    operator fun set(key: String, value: String) {
        getPrefs()
        prefs?.let {
            val editor = it.edit()
            editor.putString(key, value)
            editor.apply()
        }
    }

    operator fun set(key: String, value: ByteArray) {
        getPrefs()
        prefs?.let {
            val editor = it.edit()
            editor.putString(key, value.toDelimitedString())
            editor.apply()
        }
    }

    operator fun set(key: String, value: Boolean) {
        getPrefs()
        prefs?.let {
            val editor = it.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }
    }

    operator fun set(key: String, value: Int) {
        getPrefs()
        prefs?.let {
            val editor = it.edit()
            editor.putInt(key, value)
            editor.apply()
        }
    }

    operator fun set(key: String, value: Double) {
        getPrefs()
        prefs?.let {
            val editor = it.edit()
            editor.putFloat(key, value.toFloat())
            editor.apply()
        }
    }

}
