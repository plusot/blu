package com.plusot.bluelib.dialog

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.InputType
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.EditText
import android.widget.ListAdapter
import androidx.core.content.ContextCompat
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.util.SleepAndWake
import com.plusot.bluelib.util.getColored

enum class ClickResult {
    YES,
    NO,
    CANCEL,
    NEUTRAL,
    DISMISS
}

typealias Listener = (ClickResult) -> Unit
typealias ItemListener = (ClickResult, String?, Int) -> Unit
typealias InputListener = (ClickResult, String) -> Unit
typealias NumberInputListener = (ClickResult, Int) -> Unit

object Alerts {

    @SuppressLint("SetTextI18n")
    fun showInputDialog(
        context: Context,
        titleId: Int,
        message: Int,
        inputNumber: Int,
        hint: Int,
        listener: NumberInputListener
    ) {
        if (!Globals.runMode.isRun) return
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.alert_edit_text, null)

        val input = view.findViewById(R.id.input) as EditText
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_CLASS_NUMBER
        input.setText("" + inputNumber)
        input.hint = "" + hint

        AlertDialog.Builder(context)
            .setTitle(titleId)
            .setMessage(message)
            .setView(view)
            .setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener { _, _ ->
                try {
                    val number = Integer.parseInt(input.text.toString())
                    listener(ClickResult.YES, number)
                } catch (e: NumberFormatException) {
                    listener(ClickResult.YES, inputNumber)
                }
            })
            .setOnCancelListener { listener(ClickResult.CANCEL, inputNumber) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()

    }

    fun showOkDialog(context: Context, titleId: Int, message: String, listener: Listener) {
        showOkDialog(context, titleId, message, 0, listener)
    }

    @JvmOverloads
    fun showOkDialog(context: Context, titleId: Int, message: String, timeOut: Long = 0, listener: Listener? = null) {
        if (!Globals.runMode.isRun) return
        val dialog = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ -> listener?.invoke(ClickResult.YES) }
            .setOnCancelListener { listener?.invoke(ClickResult.CANCEL) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
        if (timeOut > 0) {
            SleepAndWake.runInMain("Alerts.showOkDialog", timeOut) {
                try {
                    dialog.dismiss()
                } catch (e: Exception) {
                    LLog.e("Could not dismiss dialog: " + e.message)
                }
            }
        }
    }

    @JvmOverloads
    fun showOkDialog(context: Context, titleId: Int, messageId: Int, timeOut: Long, listener: Listener? = null) {
        if (!Globals.runMode.isRun) return
        val dialog = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setMessage(messageId)
            .setPositiveButton(android.R.string.ok) { _, _ -> listener?.invoke(ClickResult.YES) }
            .setOnCancelListener { _ -> listener?.invoke(ClickResult.CANCEL) }
            .setIcon(android.R.drawable.ic_dialog_info)
            .show()
        if (timeOut > 0) {
            SleepAndWake.runInMain("Alert.showOkDialog", timeOut) {
                try {
                    dialog.dismiss()
                } catch (e: Exception) {
                    LLog.e("Could not dismiss dialog: " + e.message)
                }
            }
        }
    }

    @JvmOverloads
    fun showYesNoDialog(
        context: Context,
        titleId: Int,
        message: String,
        listener: Listener,
        iconId: Int = android.R.drawable.ic_dialog_info
    ) {
        if (Globals.runMode.isRun) {
            val builder = AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(message)
                .setPositiveButton(R.string.yes) { _, _ -> listener.invoke(ClickResult.YES) }
                .setNegativeButton(R.string.no) { _, _ -> listener.invoke(ClickResult.NO) }
                .setOnCancelListener { listener.invoke(ClickResult.CANCEL) }
                .setIcon(iconId)

            builder.setOnDismissListener { listener.invoke(ClickResult.DISMISS) }
            builder.show()
        }
    }

    fun showYesNoDialog(context: Context, titleId: Int, messageId: Int, listener: Listener) {
        if (Globals.runMode.isRun) {
            val builder = AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(R.string.yes) { _, _ -> listener.invoke(ClickResult.YES) }
                .setNegativeButton(R.string.no) { _, _ -> listener.invoke(ClickResult.NO) }
                .setOnCancelListener { listener.invoke(ClickResult.CANCEL) }
                .setIcon(android.R.drawable.ic_dialog_info)
            //.show();
            builder.setOnDismissListener { listener.invoke(ClickResult.DISMISS) }
            val dialog = builder.create()
            //dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show()
        }
    }


    fun showYesNoNeutralDialog(context: Context, titleId: Int, messageId: Int, neutral: Int, listener: Listener) {
        if (Globals.runMode.isRun) {
            val builder = AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(R.string.yes){ _, _ -> listener.invoke(ClickResult.YES) }
                .setNegativeButton(R.string.no) { _, _ -> listener.invoke(ClickResult.NO) }
                .setNeutralButton(neutral) { _, _ -> listener.invoke(ClickResult.NEUTRAL) }
                .setOnCancelListener{ listener.invoke(ClickResult.CANCEL) }
                .setIcon(android.R.drawable.ic_dialog_info)
            //.show();
            builder.setOnDismissListener(DialogInterface.OnDismissListener { listener.invoke(ClickResult.DISMISS) })
            val dialog = builder.create()
            //dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog.show()
        }
    }

    fun showYesNoDialog(context: Context, titleId: Int, messageId: Int, listener: Listener, icon: Int) {
        showYesNoDialog(context, titleId, messageId, R.string.yes, R.string.no, listener, icon)
    }

    fun showYesNoDialog(
        context: Context?,
        titleId: Int,
        messageId: Int,
        positive: Int,
        negative: Int,
        listener: Listener,
        icon: Int
    ) {
        var ctx = context
        var iconId = icon
        if (iconId == -1) iconId = android.R.drawable.ic_dialog_info
        if (Globals.runMode.isRun)
            try {
                var isOverlay = false
                val appContext = Globals.appContext ?: return
                if (ctx == null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
                                appContext,
                                Manifest.permission.SYSTEM_ALERT_WINDOW
                            )
                        ) {
                            LLog.i("Granted system alert window")
                            isOverlay = true
                        } else {
                            ctx = Globals.visibleActivity
                            if (ctx == null) return
                        }
                    } else
                        ctx = appContext
                }
                val builder = AlertDialog.Builder(ctx)
                    .setTitle(titleId)
                    .setMessage(messageId)
                    .setPositiveButton(positive) { _, _ -> listener.invoke(ClickResult.YES) }
                    .setNegativeButton(negative) { _, _ -> listener.invoke(ClickResult.NO) }
                    .setOnCancelListener { listener.invoke(ClickResult.CANCEL) }
                    .setCancelable(true)
                    .setIcon(iconId)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    builder.setOnDismissListener { listener.invoke(ClickResult.DISMISS) }
                }
                val dialog = builder.create()
                // for Android API 23+
                if (isOverlay && PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(
                        appContext,
                        Manifest.permission.SYSTEM_ALERT_WINDOW
                    )
                ) {
                    val w = dialog.window
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        w?.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
                    } else {
                        @Suppress("DEPRECATION")
                        w?.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)

                    }
                }
                dialog.show()
            } catch (e: WindowManager.BadTokenException) {
                LLog.i("Could not show Alert", e)
            }

    }

    fun showYesNoDialog(
        context: Context,
        titleId: Int,
        messageId: Int,
        positive: Int,
        negative: Int,
        neutral: Int,
        listener: Listener
    ) {
        val icon = android.R.drawable.ic_dialog_info

        if (Globals.runMode.isRun) {
            val builder = AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(positive) { _, _ -> listener.invoke(ClickResult.YES) }
                .setNegativeButton(negative) { _, _ -> listener.invoke(ClickResult.NO) }
                .setNeutralButton(neutral) { _, _ -> listener.invoke(ClickResult.NEUTRAL) }
                .setOnCancelListener { listener.invoke(ClickResult.CANCEL) }
                //.setCancelable(false)
                .setIcon(icon)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener { listener.invoke(ClickResult.DISMISS) }
            }
            val dialog = builder.create()
            dialog.show()

        }
    }

    fun showYesNoDialog(
        context: Context?,
        titleId: Int,
        message: String,
        positive: Int,
        negative: Int,
        listener: Listener,
        icon: Int
    ) {
        var ctx = context
        var icn = icon
        if (icn == -1) icn = android.R.drawable.ic_dialog_info

        if (Globals.runMode.isRun) {
            if (ctx == null) {
                LLog.i("Context is NULL for dialog with $message")
                ctx = Globals.visibleActivity
                if (ctx == null) return

            }
            //             {
            //                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //                    if (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(appContext, Manifest.permission.SYSTEM_ALERT_WINDOW))
            //                        LLog.i("Granted system alert window");
            //                    else {
            //                        context = Globals.getVisibleActivity();
            //                        if (context == null) return;
            //                    }
            //                } else
            //                    context = appContext;
            //            }
            val builder = AlertDialog.Builder(ctx)
                .setTitle(titleId)
                .setMessage(message)
                .setPositiveButton(positive) { _, _ -> listener.invoke(ClickResult.YES) }
                .setNegativeButton(negative) { _, _ -> listener.invoke(ClickResult.NO) }
                .setOnCancelListener { listener.invoke(ClickResult.CANCEL) }
                //.setCancelable(false)
                .setIcon(icn)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder.setOnDismissListener { listener.invoke(ClickResult.DISMISS) }
            }
            val dialog = builder.create()
            SleepAndWake.runInMain() {
                LLog.i("Trying dialog: $message")
                dialog.show()
            }
        }
    }

    fun showOkAlert(context: Context, titleId: Int, messageId: String) {

        if (Globals.runMode.isRun)
            AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    // continue with delete
                }
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    fun showOkAlert(context: Context, titleId: Int, messageId: Int) {
        if (Globals.runMode.isRun)
            AlertDialog.Builder(context)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    // continue with delete
                }
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    fun showItemsDialog(context: Context, titleId: Int, strings: Array<String>, listener: ItemListener) {
        if (!Globals.runMode.isRun) return
        val builder = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setItems(strings) { dialog, which ->
                listener.invoke(ClickResult.YES, strings[which], which)
                dialog.dismiss()
            }
            .setNegativeButton(R.string.cancel, null)
            .setOnCancelListener({ _ -> listener.invoke(ClickResult.CANCEL, null, -1) })
        val dialog = builder.create()
        val listView = dialog.getListView()
        listView.divider = ColorDrawable(dialog.context.resources.getColored(R.color.blueAccentColor))
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(2) // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false)
        listView.setHeaderDividersEnabled(false)

        dialog.show()
    }

    fun showItemsDialog(
        context: Context,
        titleId: Int,
        strings: Array<String>,
        checked: BooleanArray,
        listener: ItemListener
    ) {
        if (!Globals.runMode.isRun) return
        val builder = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setMultiChoiceItems(strings, checked) { _, which, isChecked ->
                if (isChecked)
                    listener.invoke(ClickResult.YES, strings[which], which)
                else
                    listener.invoke(ClickResult.NO, strings[which], which)
            }
            //                .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.ok) { _, _ -> listener.invoke(ClickResult.YES, null, -1) }
            .setOnCancelListener({ _ -> listener.invoke(ClickResult.CANCEL, null, -1) })
        val dialog = builder.create()
        val listView = dialog.getListView()
        listView.setDivider(ColorDrawable(dialog.getContext().getResources().getColored(R.color.blueAccentColor)))
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.setDividerHeight(2) // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false)
        listView.setHeaderDividersEnabled(false)

        dialog.show()
    }

    fun showItemsDialog(
        context: Context,
        titleId: Int,
        adapter: ListAdapter,
        listener: ItemListener,
        timeOut: Long
    ) {
        if (!Globals.runMode.isRun) return
        val builder = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setAdapter(adapter) { _, which -> listener.invoke(ClickResult.YES, null, which) }
            .setNegativeButton(R.string.cancel, null)
            .setOnCancelListener(DialogInterface.OnCancelListener {
                listener.invoke(
                    ClickResult.CANCEL,
                    null,
                    -1
                )
            })
        val dialog = builder.create()
        val listView = dialog.getListView()
        listView.divider = ColorDrawable(dialog.context.resources.getColored(R.color.blueAccentColor))
        //listView.setDivider(new ColorDrawable(Color.GRAY));
        listView.dividerHeight = 2 // set height
        //listView.setSelection();
        listView.setFooterDividersEnabled(false)
        listView.setHeaderDividersEnabled(false)

        dialog.show()
        if (timeOut > 0) {
            SleepAndWake.runInMain("Alerts.showItemsDialog", timeOut) {
                try {
                    dialog.dismiss()
                } catch (e: Exception) {
                    LLog.e("Could not dismiss dialog: " + e.message)
                }
            }
        }
    }


    fun showSingleChoiceDialog(
        context: Context,
        titleId: Int,
        strings: Array<String>,
        iSelected: Int,
        listener: ItemListener,
        timeOut: Long
    ) {
        if (!Globals.runMode.isRun) return
        val builder = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setSingleChoiceItems(strings, iSelected) { dialog, which ->
                listener.invoke(ClickResult.YES, strings[which], which)
                dialog.dismiss()
            }
            .setOnCancelListener { listener.invoke(ClickResult.CANCEL, null, -1) }
        val dialog = builder.create()
        dialog.show()
        if (timeOut > 0) {
            SleepAndWake.runInMain("Alerts.showOkDialog", timeOut) {
                try {
                    dialog.dismiss()
                } catch (e: Exception) {
                    LLog.e("Could not dismiss dialog: " + e.message)
                }
            }
        }
    }

    fun showSingleChoiceDialog(
        context: Context,
        titleId: Int,
        strings: Array<String>,
        selected: String?,
        listener: ItemListener
    ) {
        if (!Globals.runMode.isRun) return
        var iSelected = 0
        if (selected != null)
            for (str in strings) {
                if (selected.contains(str)) break
                iSelected++
            }
        if (iSelected >= strings.size) iSelected = -1
        val builder = AlertDialog.Builder(context)
            .setTitle(titleId)
            .setSingleChoiceItems(strings, iSelected) { dialog, which ->
                listener.invoke(ClickResult.YES, strings[which], which)
                dialog.dismiss()
            }
            .setOnCancelListener { listener.invoke(ClickResult.CANCEL, null, -1) }
        val dialog = builder.create()
        //        ListView listView= dialog.getListView();
        //        listView.setHeaderDividersEnabled(true);
        //        listView.setDivider(new ColorDrawable(Color.GRAY)); // set color
        //        listView.setDividerHeight(2); // set height
        //                .setMessage(messageId)
        //                .setIcon(android.R.drawable.ic_dialog_alert)
        dialog.show()
    }
}
