package com.plusot.bluelib

import android.Manifest
import android.app.Activity
import android.os.Build
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.permissions.PermissionEvent
import com.plusot.bluelib.permissions.PermitQuery
import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.ScanManager
import com.plusot.bluelib.sensor.ScanType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.bluetooth.ble.BleMan
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.internal.InternalMan
import com.plusot.bluelib.util.SleepAndWake
import com.plusot.bluelib.util.addMore
import com.plusot.bluelib.util.next

/**
 * Package: com.plusot.bluelib.sensor
 * Project: helloWatch
 *
 * Created by Peter Bruinink on 2019-05-16.
 * Copyright © 2019 Plusot. All rights reserved.
 */

enum class LibOption {
    USE_BODY_SENSORS,
    USE_BLE_SENSORS,
    USE_BT_SENSORS,
    USE_ANT_SENSORS,
    USE_INTERNAL_SENSORS,
    USE_NMEA,
    VERBOSE_DEVICES,
}

object BlueLib {
    var scanPeriod: Long = 5000L // 2019-09-16: Was 30000
    val listeners = mutableSetOf<Listener>()
    private var mayRun = true
    private val listenerLock = Object()
    private var currentScan: ScanManager? = null
    var preferredScan: ScanManager? = null
        set(value) {
            field = value
            synchronized(scanLock) {
                scanLock.notifyAll()
            }
        }

    private val scanManagers = mutableListOf<ScanManager>()
    private val scanLock = Object()
    private var isRunning = false
    private val pid: Int

    var useNmea = false
        private set

    init {
//        Log.d("Kaleido", "Init of BlueLib object ...")
        LLog.d("Init of BlueLib object")
        pid = android.os.Process.myPid()
    }

    private val scanThread = Thread {
        LLog.d("Scan thread started")
        while (Globals.runMode.isRun && mayRun) {
            try {
                currentScan?.stopScan("scanThread")
                if (preferredScan != null) {
                    currentScan = preferredScan
                    preferredScan = null
                } else
                    currentScan = scanManagers.next(currentScan)
                currentScan?.startScan()
                synchronized(scanLock) {
                    scanLock.wait(scanPeriod + 3000)
                }
            } catch (e: InterruptedException) {
                LLog.d("Scan thread interrupted")
            }
        }
    }

    interface Listener {
        fun onDeviceData(device: ScannedDevice, data: Data, isNew: Boolean)
        fun onDeviceScanned(device: ScannedDevice, isNew: Boolean)
        fun onDeviceState(device: ScannedDevice, state: Device.StateInfo, data: Data?)
        fun onScanning(scanType: ScanType, on: Boolean)
    }

    fun start(activity: Activity, vararg  options: LibOption) {
        if (isRunning) return
        isRunning = true
        Globals.init(activity.application,"BlueLib.start")
        val permissions = mutableListOf<String>()
        options.forEach {
            when (it) {
                LibOption.USE_BLE_SENSORS -> {
                    permissions.addMore(
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                }
                LibOption.USE_BODY_SENSORS -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                        permissions.addMore(
                            Manifest.permission.BODY_SENSORS,
                            Manifest.permission.ACTIVITY_RECOGNITION)
                    else
                        permissions.addMore(
                            Manifest.permission.BODY_SENSORS)

                }
                LibOption.USE_NMEA -> useNmea = true
                LibOption.USE_BT_SENSORS -> {}
                LibOption.USE_ANT_SENSORS -> {}
                LibOption.USE_INTERNAL_SENSORS -> {}
                LibOption.VERBOSE_DEVICES -> {}
            }
        }

        PermitQuery.checkPermission(activity, permissions) {
            when(it) {
                PermissionEvent.COMPLETED -> {
                    options.forEach { libOption ->
                        when (libOption) {
                            LibOption.USE_BLE_SENSORS -> scanManagers.add(BleMan.getInstance())
                            LibOption.USE_INTERNAL_SENSORS -> scanManagers.add(InternalMan.getInstance())
                            LibOption.USE_BODY_SENSORS -> {}
                            LibOption.USE_BT_SENSORS -> {}
                            LibOption.USE_ANT_SENSORS -> {}
                            LibOption.USE_NMEA -> {}
                            LibOption.VERBOSE_DEVICES -> {}
                        }
                    }
                    try {
                        scanThread.start()
                        LLog.d("Starting thread")
                    } catch (e: Exception) {
                        LLog.d("Thread already started?")
                    }
                }
                PermissionEvent.INCOMPLETE -> {}
                PermissionEvent.BLUETOOTH_ON -> {}
            }
        }
    }

    fun close() {
        if (android.os.Process.myPid() != pid) {
            LLog.d("Not closing BlueLib as close is called by wrong process pid")
            return
        }
        LLog.d("Closing BlueLib")
        Globals.runMode = Globals.RunMode.FINISHING
        mayRun = false
        isRunning = false
        scanManagers.forEach {
            it.close()
            SleepAndWake.runInMain (2000L){
                //                val pid = android.os.Process.myPid()
//                android.os.Process.killProcess(pid)
                Runtime.getRuntime().exit(0)
            }
        }
    }

    fun fireData(device: ScannedDevice, data: Data, isNew: Boolean) {
        synchronized(listenerLock) {
            listeners.forEach { it.onDeviceData(device, data, isNew) }
        }
    }

    fun fireState(device: ScannedDevice, state: Device.StateInfo, data: Data?) {
        synchronized(listenerLock) {
            listeners.forEach { it.onDeviceState(device, state, data) }
        }
    }

    fun fireScanned(device: ScannedDevice, isNew: Boolean) {
        synchronized(listenerLock) {
            listeners.forEach { it.onDeviceScanned(device, isNew) }
        }
    }

    fun fireScanning(type: ScanType, active: Boolean) {
        synchronized(listenerLock) {
            listeners.forEach { it.onScanning(type, active) }
        }
        if (!active) synchronized(scanLock) {
            scanLock.notifyAll()
        }
    }

    fun pause() {
        scanManagers.forEach { scanner ->
            scanner.devices.forEach { it.pauseDevice() }
        }
    }

    fun resume() {
        scanManagers.forEach { scanner ->
            scanner.devices.forEach { it.resumeDevice() }
        }
    }

    fun getScannedDevices(): List<ScannedDevice> {
        val devices = mutableListOf<ScannedDevice>()
        scanManagers.forEach {
            devices.addAll(it.devices)
        }
        return devices
    }


}