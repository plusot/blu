package com.plusot.bluelib.util

/**
 * Created by peet on 12-09-15.
 */
class Stringer (
    private val delim: String,
    private val header: String = "",
    private val footer: String = ""
) {
    private val sb = StringBuilder()
    private var added = 0

    fun append(value: String?): Stringer {
        if (added++ > 0) sb.append(delim)
        if (value == null) return this
        sb.append(value)
        return this
    }

    fun append(value: Int): Stringer {
        if (added++ > 0) sb.append(delim)
        sb.append(value)
        return this
    }

    fun appendNoDelim(value: String?): Stringer {
        if (value == null) return this
        sb.append(value)
        return this
    }

    fun appendNoDelim(value: Int): Stringer {
        sb.append(value)
        return this
    }

    fun append(values: Array<String>): Stringer {
        for (value in values) append(value)
        return this
    }

    override fun toString(): String {
        return header + sb.toString() + footer
    }
}
