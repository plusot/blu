package com.plusot.bluelib.util

import com.plusot.bluelib.log.LLog


/**
 * Created by peet on 11-11-15.
 */
object Numbers {

    fun parseInt(intStr: String): Int {
        val chars = intStr.toCharArray()
        var sign = 1
        var result = 0
        for (ch in chars) {
            if (ch == '-')
                sign = -1
            else {
                result *= 10
                result += ch.toInt() - 48
            }
        }
        return sign * result
    }

    fun parseDouble(intStr: String): Double {
        val chars = intStr.toCharArray()
        var countDecimals = false
        var sign = 1.0
        var result = 0.0
        var dec = 1
        for (ch in chars) {
            if (ch == '-')
                sign = -1.0
            else if (ch == '.' || ch == ',')
                countDecimals = true
            else {
                if (countDecimals) dec *= 10
                result *= 10.0
                result += (ch.toInt() - 48).toDouble()
            }
        }
        return sign * result / dec
    }

    fun getBits(value: Int, pos: Int, bits: Int): Int {
        val result = 0xffff and value shr pos
        when (bits) {
            1 -> return result and 0x1
            2 -> return result and 0x3
            3 -> return result and 0x7
            4 -> return result and 0xF
            5 -> return result and 0x1F
            6 -> return result and 0x3F
            7 -> return result and 0x7F
            else -> return result
        }
    }


    fun pow(base: Int, power: Int): Int {
        return if (power == 0) 1 else base * pow(base, power - 1)
    }

    fun unsigned2Signed(unsigned: Int, size: Int): Int {
        var uns = unsigned
        if (uns and (1 shl size - 1) != 0) {
            uns = -1 * ((1 shl size - 1) - (uns and (1 shl size - 1) - 1))
        }
        return uns
    }

    fun checkBit(value: Int, bit: Int): Boolean {
        return value and bit == bit
    }



    fun inRange(value: Any, minValue: Double, maxValue: Double): Boolean {
        val doubles = getDoubleValues(value, true) ?: return false
        for (aDouble in doubles) {
            if (aDouble > maxValue || aDouble < minValue) return false
        }
        return true
    }

    fun greaterThan(value: Any, minValue: Double): Boolean {
        val doubles = getDoubleValues(value, true) ?: return false
        for (aDouble in doubles) {
            if (aDouble > minValue) return true
        }
        return false
    }

    fun max(value: DoubleArray?): Double {
        var max = 0f
        if (value != null) {
            for (aValue in value) {
                max += Math.max(max.toDouble(), aValue).toFloat()
            }
        }
        return max.toDouble()
    }

    fun average(value: DoubleArray?): Double {
        var avg = 0.0
        if (value != null) {
            for (aValue in value) {
                avg += aValue
            }
            avg /= value.size.toDouble()
        }
        return avg
    }

    fun average(value: FloatArray?): Float {
        var avg = 0f
        if (value != null) {
            for (aValue in value) {
                avg += aValue
            }
            avg /= value.size.toFloat()
        }
        return avg
    }

    fun getDoubleValue(value: Any): Double {
        if (value is DoubleArray) {
            return average(value)
        }
        if (value is FloatArray) {
            return average(value).toDouble()
        }
        if (value is Double) return value
        if (value is Long) return value.toDouble()
        if (value is Float) return value.toDouble()
        if (value is Int) return value.toDouble()
        if (value is String)
            try {
                return java.lang.Double.valueOf(value)
            } catch (e: NumberFormatException) {
                LLog.e("NumberFormatException " + e.message)

            }

        return 0.0
    }

    fun getDoubleValues(value: Any, acceptSingleValue: Boolean): DoubleArray? {
        if (value is DoubleArray) {
            val result = DoubleArray(value.size)
            System.arraycopy(value, 0, result, 0, result.size)
            return result
        }
        if (value is FloatArray) {
            val result = DoubleArray(value.size)
            var i = 0
            for (f in value) {
                result[i++] = f.toDouble()
            }
            return result
        }
        return if (acceptSingleValue) {
            doubleArrayOf(getDoubleValue(value))
        } else null
    }


    fun getUInt8(arr: ByteArray, i: Int): Int {
        return if (i >= arr.size) 0 else 0xff and arr[i].toInt()
    }

    fun getUInt16(arr: ByteArray, i: Int): Int {
        return if (i >= arr.size - 1) 0 else (0xff and arr[i].toInt()) + (0xff and arr[i + 1].toInt() shl 8)
    }

    fun getUInt32(arr: ByteArray, i: Int): Long {
        return if (i >= arr.size - 3) 0 else ((0xff and arr[i].toInt()) + (0xff and arr[i + 1].toInt() shl 8) + (0xff and arr[i + 2].toInt() shl 16) + (0xff and arr[i + 3].toInt() shl 24)).toLong()
    }

    fun getUInt24(arr: ByteArray, i: Int): Int {
        return if (i >= arr.size - 2) 0 else (0xff and arr[i].toInt()) + (0xff and arr[i + 1].toInt() shl 8) + (0xff and arr[i + 2].toInt() shl 16)
    }

    //    public static int getUInt24(byte[] c, int offset) {
    //        int lowerByte = (int) c[offset] & 0xFF;
    //        int mediumByte = (int) c[offset+1] & 0xFF;
    //        int upperByte = (int) c[offset + 2] & 0xFF;
    //        return (upperByte << 16) + (mediumByte << 8) + lowerByte;
    //    }

    fun getSInt16(arr: ByteArray, i: Int): Int {
        return if (i >= arr.size) 0 else Numbers.unsigned2Signed(getUInt16(arr, i), 16)
    }

    fun getByte(arr: ByteArray, i: Int): Int {
        return if (i >= arr.size) 0 else 0xff and arr[i].toInt()
    }

    fun diffRatio(value: Double): Double {
        return Math.abs(1.0 - value)
    }
}

fun intMask(bitLength: Int, shift: Int = 0): Int {
    var mask = 0
    repeat(bitLength) {
        mask = mask shl 1
        mask = mask or 0x1
    }
    return mask shl shift
}

fun longMask(bitLength: Int, shift: Int = 0): Long {
    var mask = 0L
    repeat(bitLength) {
        mask = mask shl 1
        mask = mask or 0x1
    }
    return mask shl shift
}

fun ByteArray.getByte(pos: Int, bitShift: Int, bitLength: Int): Int {
    val mask = intMask(bitLength)
    return if (pos >= this.size) 0 else ((0xff and this[pos].toInt()) shr bitShift) and mask
}

fun ByteArray.setByte(pos: Int, bitShift: Int, bitLength: Int, value: Int) {
    var mask = 0
    repeat(bitLength) {
        mask = mask shl 1
        mask = mask or 0x1
    }
    mask = mask shl bitShift
    var inverseMask = mask xor 0xff
    if (pos < this.size) this[pos] = ((this[pos].toInt() and inverseMask) or (value shl bitShift)).toByte()
}

fun ByteArray.setUInt8(i: Int, value: Int) {
    if (i < this.size) {
        this[i] = (value and 0xff).toByte()
    }
}

//fun Int.toSignedBits(size: Int): Int {
//    var i = this
//    if (i < 0) {
//        i = (1 shl size - 1) + (i and (1 shl size - 1) - 1)
//    }
//    return i
//}

fun ByteArray.setUInt16(i: Int, value: Int) {
    if (i < this.size - 1) {
        this[i] = (value and 0xff).toByte()
        this[i + 1] = ((value shr 8) and 0xff).toByte()
    }
}

fun ByteArray.setSInt16(i: Int, value: Int) = this.setUInt16(i, value.toSigned(16))


fun ByteArray.setUInt32(i: Int, value: Int) {
    if (i < this.size - 3) {
        this[i] = (value and 0xff).toByte()
        this[i + 1] = ((value shr 8) and 0xff).toByte()
        this[i + 2] = ((value shr 16) and 0xff).toByte()
        this[i + 3] = ((value shr 24) and 0xff).toByte()
    }
}

fun ByteArray.setUInt32(i: Int, value: Long) {
    if (i < this.size - 3) {
        this[i] = (value and 0xff).toByte()
        this[i + 1] = ((value shr 8) and 0xff).toByte()
        this[i + 2] = ((value shr 16) and 0xff).toByte()
        this[i + 3] = ((value shr 24) and 0xff).toByte()
    }
}

fun ByteArray.setUInt64(i: Int, value: Long) {
    if (i < this.size - 7) {
        this[i] = (value and 0xff).toByte()
        this[i + 1] = ((value shr 8) and 0xff).toByte()
        this[i + 2] = ((value shr 16) and 0xff).toByte()
        this[i + 3] = ((value shr 24) and 0xff).toByte()
        this[i + 4] = ((value shr 32) and 0xff).toByte()
        this[i + 5] = ((value shr 40) and 0xff).toByte()
        this[i + 6] = ((value shr 48) and 0xff).toByte()
        this[i + 7] = ((value shr 56) and 0xff).toByte()
    }
}

fun ByteArray.insert(pos: Int, length: Int, other: ByteArray) {
    val l = Math.min(
        Math.min(length + pos, this.size) - pos,
        Math.min(length, other.size)
    )
    for (i in 0 until l) this[i + pos] = other[i]
}

fun ByteArray.insert(pos: Int, other: ByteArray) {
    val l = Math.min(this.size - pos, other.size)
    for (i in 0 until l) this[i + pos] = other[i]
}

fun ByteArray.getString(i: Int, length: Int): String {
    if (i >= this.size) return ""
    return try {
        String(this.copyOfRange(i, i + Math.min(length, this.size - i)))
    } catch (_: Exception) {
        ""
    }
}

fun ByteArray.toHex(): String =  StringUtil.toHexString(this)
fun ByteArray.toReadable(): String =  StringUtil.toReadableString(this)
fun ByteArray.toIntString() = this.fold("") { acc, value -> "${acc.addIfNotEmpty(" ")}$value"}


fun ByteArray.getBytes(i: Int, length: Int): ByteArray {
    if (i >= this.size) return byteArrayOf()
    return try {
        this.copyOfRange(i, i + Math.min(length, this.size - i))
    } catch (_: Exception) {
        byteArrayOf()
    }
}

fun ByteArray.getUInt8(i: Int): Int {
    return if (i >= this.size)
        0
    else
        (0xff and this[i].toInt())
}

fun ByteArray.getUInt16(i: Int): Int {
    return if (i >= this.size - 1)
        0
    else
        (       (0xff and this[i].toInt()) +
                (0xff and this[i + 1].toInt() shl 8)
                )
}

fun ByteArray.getUInt16BE(i: Int): Int {
    return if (i >= this.size - 1)
        0
    else
        (       (0xff and this[i + 1].toInt()) +
                (0xff and this[i].toInt() shl 8)
                )
}

fun ByteArray.getInt16(i: Int): Int = this.getUInt16(i).toSigned(16)


fun ByteArray.getUInt32(i: Int): Long {
    return if (i >= this.size - 3)
        0
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24)
                ).toLong()
}

fun ByteArray.getUInt40(i: Int): Long {
    return if (i >= this.size - 4)
        0
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24) +
                (0xffL and this[i + 4].toLong() shl 32)
                ).toLong()
}

fun ByteArray.getUInt40BE(i: Int): Long { // BE = Big Endian
    return if (i >= this.size - 4)
        0
    else
        (       (0xffL and this[i + 4].toLong()) +
                (0xffL and this[i + 3].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 1].toLong() shl 24) +
                (0xffL and this[i].toLong() shl 32)
                ).toLong()
}

fun ByteArray.getNibble(i: Int, rShift: Int = 0): Int {
    return if (i >= this.size)
        0
    else
        (0x0f and (this[i].toInt() shr rShift))
}

fun ByteArray.isZero(): Boolean {
    this.forEach { if (it.toInt() != 0) return false }
    return true
}

fun ByteArray.equalsOther(other: ByteArray?) : Boolean {
    if (other == null) return false
    if (this.size != other.size) return false
    this.forEachIndexed { index, byte -> if (other[index] != byte) return false }
    return true
}

fun ByteArray.getUInt64(i: Int): Long {
    return if (i >= this.size - 7)
        0L
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24) +
                (0xffL and this[i + 4].toLong() shl 32) +
                (0xffL and this[i + 5].toLong() shl 40) +
                (0xffL and this[i + 6].toLong() shl 48) +
                (0xffL and this[i + 7].toLong() shl 56)
                )
}

fun ByteArray.copyFrom(fromIndex: Int) = if (fromIndex < this.size) this.copyOfRange(fromIndex, this.size) else null

fun Int.toSigned(size: Int): Int =
    if (this and (1 shl size - 1) != 0)
        -1 * ((1 shl size - 1) - (this and (1 shl size - 1) - 1))
    else
        this

fun Long.toSigned(size: Int): Long =
    if (this and (1L shl size - 1) != 0L)
        -1L * ((1L shl size - 1) - (this and (1L shl size - 1) - 1))
    else
        this

fun Byte.checkBit(bit: Int): Boolean {
    return this.toInt() and bit == bit
}

fun Byte.checkBitOnIndex(index: Int): Boolean {
    val bit = 1 shl index
    return this.toInt() and bit == bit
}



