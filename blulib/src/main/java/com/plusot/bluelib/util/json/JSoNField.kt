package com.plusot.bluelib.util.json

/**
 * Created by peet on 06-11-15.
 */
enum class JSoNField {
    NONE,
    ARRAY,
    DATA,
    VALUES,
    TIME,
    DATATYPE,
    VALUE,
    CLASS,
    UNIT,
    RESULT,
    INFO,
    NAME,
    DEVICEID,
    HUMANNAME,
    MODEL,
    ACTIVATORCOLOR,
    VOLUME,
    SENSORID,
    SESSIONID,
    ROOM,
    PASSWORD,
    ERROR,
    OK;

    override fun toString(): String {
        return name.toLowerCase()
    }

    companion object {

        fun fromString(value: String?): JSoNField {
            if (value == null) return NONE
            for (category in JSoNField.values()) {
                if (category.toString() == value.toLowerCase()) return category
            }
            return NONE
        }
    }
}
