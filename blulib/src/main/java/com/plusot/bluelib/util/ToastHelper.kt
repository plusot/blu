package com.plusot.bluelib.util

import android.content.Context
import android.widget.Toast
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog


object ToastHelper {

    private var lastShown = 0L
    fun showToastLong(value: String) {
        //if (toast != null) toast.cancel();
        val appContext = Globals.appContext ?: return

        val toast = Toast.makeText(
            appContext,
            value,
            Toast.LENGTH_LONG
        )
        LLog.i(value)

        toast.show()
    }

    fun showToastLong(value: String, gravity: Int) {
        //if (toast != null) toast.cancel();
        val appContext = Globals.appContext ?: return

        val toast = Toast.makeText(
            appContext,
            value,
            Toast.LENGTH_LONG
        )
        LLog.i(value)

        toast.setGravity(gravity, 0, 0)
        toast.show()
    }

    fun showToastLong(value: Int) {
        //if (toast != null) toast.cancel();
        val appContext = Globals.appContext ?: return

        val toast = Toast.makeText(
            appContext,
            appContext.getString(value),
            Toast.LENGTH_LONG
        )
        LLog.i(appContext.getString(value))
        toast.show()
    }

    fun showToastLongSave(value: Int) {
        //if (toast != null) toast.cancel();
        val appContext = Globals.appContext ?: return

        SleepAndWake.runInMain() {
            val toast = Toast.makeText(
                appContext,
                appContext.getString(value),
                Toast.LENGTH_LONG
            )
            LLog.i(appContext.getString(value))
            toast.show()
        }
    }

    fun showToastShort(value: String) {
        //if (toast != null) toast.cancel();
        if (System.currentTimeMillis() - lastShown < 500) return
        lastShown = System.currentTimeMillis()
        val appContext = Globals.appContext ?: return

        val toast = Toast.makeText(
            appContext,
            value,
            Toast.LENGTH_SHORT
        )
        LLog.i(value)
        toast.show()
    }

    @JvmOverloads
    fun showToastShort(value: Int, gravity: Int = -1, xOffset: Int = 0, yOffset: Int = 0) {
        //if (toast != null) toast.cancel();
        val appContext = Globals.appContext ?: return

        val toast = Toast.makeText(
            appContext,
            appContext.getString(value),
            Toast.LENGTH_SHORT
        )
        if (gravity != -1) toast.setGravity(gravity, xOffset, yOffset)
        LLog.i(appContext.getString(value))
        toast.show()
    }

}
