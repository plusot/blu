package com.plusot.bluelib.util

import java.util.regex.Matcher
import java.util.regex.Pattern


class Tuple<T, V>(val first: T, val second: V) {

    fun t1(): T {
        return first
    }

    fun t2(): V {
        return second
    }

    companion object {

        fun splitInt(value: String): Tuple<String, Int> {
            var pattern = Pattern.compile("\\d+")
            var matcher = pattern.matcher(value)
            var i = 0
            // Find all matches
            if (matcher.find()) {
                i = Integer.valueOf(matcher.group())
            }

            pattern = Pattern.compile("[a-zA-Z]+")
            matcher = pattern.matcher(value)
            var str = ""
            // Find all matches
            if (matcher.find()) {
                // Get the matching string
                str = matcher.group()
            }
            return Tuple(str, i)

        }
    }

}



