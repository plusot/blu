package com.plusot.bluelib.util

import android.os.Build
import android.os.LocaleList
import android.text.*

import com.plusot.bluelib.log.LLog
import java.util.*

fun String.proper(): String {
    val value = this.replace("_", " ")
    val parts = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    for (i in parts.indices)
        if (parts[i].length > 1) {
            parts[i] = parts[i].substring(0, 1).toUpperCase() + parts[i].substring(1).toLowerCase()
        } else
            parts[i] = parts[i].toUpperCase()
    return Stringer(" ").append(parts).toString()
}

val String.toSpanned: Spanned
    get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(this)
    }

fun Int.toBin(limit: Int = 32): String {
    val l = Math.min(limit - 1, 31)
    val char = if ((this shr l) and 0x1 == 0x1) '1' else '0'
    return if (l > 0) {
        "$char${this.toBin(l)}"
    } else
        char.toString()
}

fun Int.zeroLeading(zeros: Int): String {
    val formatStr = "%0${zeros}d"
    return String.format(formatStr, this)
}

fun Int.toOrdinal(): String {
    //LLog.d("Locale ${Locale.getDefault().toLanguageTag()}")
    val ordinal = if (Locale.getDefault().toLanguageTag().startsWith("nl", true)) when (this) {
        0,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19 -> "de"
        else -> "ste"
    } else when (this % 10) {
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }

    return String.format("%d%s", this, ordinal)
}

fun Int.toOrdinalOrNot(): String {
    //LLog.d("Locale ${Locale.getDefault().toLanguageTag()}")
    val ordinal = if (Locale.getDefault().toLanguageTag().startsWith("nl", true))
        ""
    else when (this % 10) {
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }

    return String.format("%d%s", this, ordinal)
}

fun Long.toBin(limit: Int = 64): String {
    val l = Math.min(limit - 1, 63)
    val char = if ((this shr l) and 0x1L == 0x1L) '1' else '0'
    return if (l > 0) {
        "$char${this.toBin(l)}"
    } else
        char.toString()
}

fun <E> Set<E>.toString(delim: String): String {
    return this.fold("") { acc, value -> "${acc.addIfNotEmpty(delim)}$value" }
}
fun <E> List<E>.toString(delim: String): String {
    return this.fold("") { acc, value -> "${acc.addIfNotEmpty(delim)}$value" }
}

//fun <E> MutableSet<E>.toString(delim: String): String {
//    return this.fold("") { acc, value -> "${acc.addIfNotEmpty(delim)}$value" }
//}

fun Int.toHex(): String = StringUtil.toHexString(this)

fun Long.toHex(): String = String.format("0x%X", this)


object StringUtil {

    fun proper(value: String?): String? {
        if (value == null) return null
        val sValue = value.replace("_", " ")
        val parts = sValue.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (i in parts.indices)
            if (parts[i].length > 1) {
                parts[i] = parts[i].substring(0, 1).toUpperCase(Locale.getDefault()) + parts[i].substring(1).toLowerCase(Locale.getDefault())
            } else
                parts[i] = parts[i].toUpperCase(Locale.getDefault())
        return Stringer(" ").append(parts).toString()
    }

    fun toString(array: Array<Any>?, delim: String = " "): String {
        if (array == null) return ""
        val sb = StringBuilder()
        for (obj in array) {
            if (sb.isNotEmpty()) sb.append(delim)
            sb.append(obj.toString())
        }
        return sb.toString()
    }

    fun toString(array: FloatArray?, delim: String, decimals: Int = -1): String {
        if (array == null) return ""
        val sb = Stringer(delim)
        for (obj in array) {
            if (decimals >= 0)
                sb.append(Format.format(obj, decimals))
            else
                sb.append("" + obj)
        }
        return sb.toString()
    }

    fun toString(array: ByteArray?, delim: String = " ", scale: Double = 1.0, decimals: Int = 0): String {
        if (array == null) return ""
        val sb = Stringer(delim)
        for (obj in array) {
            if (scale != 1.0)
                sb.append(Format.format(scale * obj, decimals))
            else
                sb.append("" + obj)
        }
        return sb.toString()
    }

    fun toString(array: IntArray?, delim: String = " ", scale: Double = 1.0, decimals: Int = 0): String {
        if (array == null) return ""
        val sb = Stringer(delim)
        for (obj in array) {
            if (scale != 1.0)
                sb.append(Format.format(scale * obj, decimals))
            else
                sb.append("" + obj)
        }
        return sb.toString()
    }


    fun toString(array: LongArray?, delim: String): String {
        if (array == null) return ""
        val sb = Stringer(delim)
        for (obj in array) {
            sb.append("" + obj)
        }
        return sb.toString()
    }

    fun toString(array: Array<String>?, delim: String): String {
        if (array == null) return ""
        val sb = StringBuilder()
        for (obj in array) {
            if (sb.length > 0) sb.append(delim)
            sb.append("" + obj)
        }
        return sb.toString()
    }

    fun toString(coll: Collection<*>?, delim: String): String {
        if (coll == null) return ""
        val sb = StringBuilder()
        for (obj in coll) {
            if (obj is Collection<*>) {
                sb.append(toString(obj, delim))
            } else {
                if (sb.length > 0) sb.append(delim)
                sb.append("" + obj.toString())
            }
        }
        return sb.toString()
    }

    fun toString(array: DoubleArray?, delim: String = " ", decimals: Int = -1, limit: Int = -1): String {
        var lim = limit
        if (array == null) return ""
        val sb = StringBuilder()
        for (obj in array) {
            if (sb.isNotEmpty()) sb.append(delim)
            if (decimals >= 0)
                sb.append(Format.format(obj, decimals))
            else
                sb.append("" + obj)
            if (--lim == 0) break
        }
        return sb.toString()
    }

    fun toString(map: Map<String, *>): String {
        val sb = StringBuilder()

        for (key in map.keys) {
            if (sb.length != 0) sb.append("\n")
            sb.append(key).append(" = ")
            if (map[key] != null) sb.append(map[key].toString())
        }
        return sb.toString()
    }

    /*
     * toString(Object) applies only to objects used in this application!
     */
//    fun toStringXL(obj: Any, delim: String, decimals: Int = -1): String {
//        return (obj as? DoubleArray)?.let { toString(it, delim, decimals) }
//            ?: ((obj as? FloatArray)?.let { toString(it, delim, decimals) }
//                ?: ((obj as? IntArray)?.let { toString(it, delim) }
//                    ?: ((obj as? LongArray)?.let { toString(it, delim) } ?: obj.toString())))
//    }



    fun toHexString(data: Byte): String {
        return String.format("%02X", data.toInt() and 0xFF)
    }

    fun toHexString(data: Int): String {
        return String.format("0x%X", data)
    }

    fun toHexString(data: ByteArray?, len: Int = Integer.MAX_VALUE, longFormat: Boolean = false): String {
        if (null == data) {
            return "no deviceData"
        }

        val hexString = StringBuilder()
        for (i in 0 until Math.min(data.size, len)) {
            if (longFormat) hexString.append("[")
            hexString.append(String.format("%02X", data[i].toInt() and 0xFF))
            if (longFormat)
                hexString.append("]")
            else
                hexString.append(" ")
        }
        return hexString.toString().trim { it <= ' ' }
    }

    fun toHex(data: ByteArray?): String {
        if (null == data) {
            return "no deviceData"
        }
        val hexString = StringBuilder()
        hexString.append("0x")
        for (aData in data) {
            hexString.append(String.format("%02X", aData.toInt() and 0xFF))
        }
        return hexString.toString()
    }

    fun toHex(data: IntArray?): String {
        if (null == data) {
            return "no deviceData"
        }
        val hexString = StringBuilder()
        //hexString.append("0x");
        for (aData in data) {
            hexString.append(String.format(" %02X", aData))
        }
        return hexString.toString()
    }

    fun toHex(data: Long, len: Int): String {
        val format = "%0" + len + "X"
        return String.format(format, data)
    }

    fun toHex(data: ByteArray?, offset: Int, len: Int): String {
        if (null == data) return "no deviceData"
        if (offset >= data.size) return "no deviceData"

        val hexString = StringBuilder()
        hexString.append("0x")
        for (i in offset until Math.min(offset + len, data.size)) {
            hexString.append(String.format("%02X", data[i].toInt() and 0xFF))
        }
        return hexString.toString()
    }

    fun toReadableString(data: ByteArray?, offset: Int, len: Int): String {
        if (null == data) {
            return "no deviceData"
        }
        val hexString = StringBuilder()
        for (i in offset until Math.min(offset + len, data.size)) {
            if (data[i].toInt() and 0xFF >= 32 && data[i].toInt() and 0xFF <= 127) {
                hexString.append(data[i].toChar())
            } else {
                hexString.append(String.format("%02X", data[i].toInt() and 0xFF))
            }
        }
        return hexString.toString()
    }

    fun toReadableString(data: ByteArray, longFormat: Boolean): String {
        return toReadableString(data, data.size, longFormat)
    }

    fun toReadableString(data: ByteArray, len: Int = data.size, longFormat: Boolean = true): String {
        val hexString = StringBuilder()
        for (i in 0 until Math.min(len, data.size)) {
            if (data[i].toInt() and 0xFF in 32..127) {
                hexString.append(data[i].toChar())
            } else {
                if (longFormat) hexString.append("[")
                hexString.append(String.format("%02X", data[i].toInt() and 0xFF))
                if (longFormat)
                    hexString.append("]")
                else
                    hexString.append(" ")
            }
        }
        return hexString.toString().trim { it <= ' ' }
    }

    fun toReadableString(data: ByteArray?, st: Int, length: Int, longFormat: Boolean): String {
        var len = length
        if (null == data) {
            return "no deviceData"
        }
        if (len + st > data.size) len = data.size - st
        if (len <= 0) return ""
        val hexString = StringBuilder()
        for (i in 0 until len) {
            if (data[i + st].toInt() and 0xFF in 32..127) {
                hexString.append(data[i + st].toChar())
            } else {
                if (longFormat) hexString.append("[")
                hexString.append(String.format("%02X", data[i + st].toInt() and 0xFF))
                if (longFormat)
                    hexString.append("]")
                else
                    hexString.append(" ")
            }
        }
        return hexString.toString().trim { it <= ' ' }
    }

    fun toReadableString(data: String?, longFormat: Boolean): String {
        if (null == data) {
            return "no deviceData"
        }

        val hexString = StringBuilder()
        for (i in 0 until data.length) {
            if (data[i].toInt() and 0xFF >= 32 && data[i].toInt() and 0xFF <= 127) {
                hexString.append(data[i])
            } else {
                if (longFormat) hexString.append("[")
                hexString.append(String.format("%02X", data[i].toInt() and 0xFFFF))
                if (longFormat)
                    hexString.append("]")
                else
                    hexString.append(" ")
            }
        }
        return hexString.toString().trim { it <= ' ' }
    }

    fun trimAll(msg: String?): String {
        return msg?.replace(0xB0.toChar(), ' ')?.replace("[>\uFFFD \r\n]".toRegex(), "") ?: ""
    }

    fun trim(string: String?, length: Int): String {
        if (string == null) return ""
        return if (string.length > length) string.substring(0, length - 3) + "..." else string
    }

    fun trim(string: String?, sub: String): String? {
        if (string == null) return null
        var str: String = string
        if (str.startsWith(sub)) str = str.substring(sub.length)
        if (str.endsWith(sub)) str = str.substring(0, str.length - sub.length)
        return str
    }

    fun trim(string: String?, sub: String, sub2: String): String? {
        if (string == null) return null
        var str: String = string
        if (str.startsWith(sub)) str = str.substring(sub.length)
        if (str.endsWith(sub2)) str = str.substring(0, str.length - sub2.length)
        return str
    }

//    fun toStringArray(collection: Collection<Array<String>>?): Array<String>? {
//        if (collection == null) return null
//        val strings = arrayOfNulls<String>(collection.size)
//        var i = 0
//        for (item in collection) {
//            strings[i++] = toString(item, ";")
//        }
//        return strings
//    }
//
//    fun toDouble(value: String?, scaler: Scaler?): Double {
//        if (value == null) return 0.0
//        try {
//            return if (scaler == null) java.lang.Double.valueOf(value) else scaler.scaleBack(
//                java.lang.Double.valueOf(
//                    value
//                )
//            )
//                .doubleValue()
//        } catch (e: NumberFormatException) {
//            return 0.0
//        }
//
//    }
//
//    fun toDoubles(values: Array<String>?, scaler: Scaler?): DoubleArray? {
//        if (values == null) return null
//        val doubles = DoubleArray(values.size)
//        var i = 0
//        for (value in values)
//            try {
//                if (scaler == null)
//                    doubles[i] = java.lang.Double.valueOf(value)
//                else
//                    doubles[i] = scaler.scaleBack(java.lang.Double.valueOf(value)).doubleValue()
//                i++
//            } catch (e: NumberFormatException) {
//                doubles[i++] = 0.0
//            }
//
//        return doubles
//    }
//
//    fun toFloats(values: Array<String>?, scaler: Scaler?): FloatArray? {
//        if (values == null) return null
//        val floats = FloatArray(values.size)
//        var i = 0
//        for (value in values)
//            try {
//                if (scaler == null)
//                    floats[i] = java.lang.Float.valueOf(value)
//                else
//                    floats[i] = scaler.scaleBack(java.lang.Float.valueOf(value)).floatValue()
//                i++
//            } catch (e: NumberFormatException) {
//                floats[i++] = 0f
//            }
//
//        return floats
//    }
//
//    fun toFloat(value: String?, scaler: Scaler?): Float {
//        if (value == null) return 0f
//        try {
//            return if (scaler == null) java.lang.Float.valueOf(value) else scaler.scaleBack(
//                java.lang.Float.valueOf(
//                    value
//                )
//            ).floatValue()
//        } catch (e: NumberFormatException) {
//            return 0f
//        }
//
//    }
//
//    fun toInt(value: String?, scaler: Scaler?): Int {
//        if (value == null) return 0
//        try {
//            return if (scaler == null)
//                Integer.valueOf(value)
//            else
//                scaler.scaleBack(Integer.valueOf(value)).intValue()
//        } catch (e: NumberFormatException) {
//            return 0
//        }
//
//    }
//
//    fun toInts(values: Array<String>?, scaler: Scaler?): IntArray? {
//        if (values == null) return null
//        val ints = IntArray(values.size)
//        var i = 0
//        for (value in values)
//            try {
//                if (scaler == null)
//                    ints[i] = Integer.valueOf(value)
//                else
//                    ints[i] = scaler.scaleBack(Integer.valueOf(value)).intValue()
//                i++
//            } catch (e: NumberFormatException) {
//                ints[i++] = 0
//            }
//
//        return ints
//    }
//
//    fun toLongs(values: Array<String>?, scaler: Scaler?): LongArray? {
//        if (values == null) return null
//        val longs = LongArray(values.size)
//        var i = 0
//        for (value in values)
//            try {
//                if (scaler == null)
//                    longs[i] = java.lang.Long.valueOf(value)
//                else
//                    longs[i] = scaler.scaleBack(java.lang.Long.valueOf(value)).longValue()
//                i++
//            } catch (e: NumberFormatException) {
//                longs[i++] = 0
//            }
//
//        return longs
//    }
//
//    fun toLong(value: String?, scaler: Scaler?): Long {
//        if (value == null) return 0
//        try {
//            return if (scaler == null) java.lang.Long.valueOf(value) else scaler.scaleBack(
//                java.lang.Long.valueOf(
//                    value
//                )
//            ).longValue()
//        } catch (e: NumberFormatException) {
//            LLog.e("NumberFormatException " + e.message)
//
//        }
//
//        return 0
//    }
//
//    fun toObject(values: Array<String>?, someClass: Class<*>, scaler: Scaler): Any? {
//        if (values == null || values.size == 0) return null
//        if (values.size == 1) {
//            return toObject(values[0], someClass, scaler)
//        } else if (someClass == Double::class.java) {
//            return toDoubles(values, scaler)
//        } else if (someClass == Float::class.java) {
//            return toFloats(values, scaler)
//        } else if (someClass == Int::class.java) {
//            return toInts(values, scaler)
//        } else if (someClass == Long::class.java) {
//            return toLongs(values, scaler)
//        } else if (someClass == String::class.java) {
//            return values
//        }
//        return null
//    }
//
//    fun toObject(value: String?, someClass: Class<*>, scaler: Scaler?): Any? {
//        if (value == null) return null
//        if (someClass == Double::class.java) {
//            try {
//                return if (scaler == null) java.lang.Double.parseDouble(value) else scaler.scaleBack(
//                    java.lang.Double.parseDouble(
//                        value
//                    )
//                )
//            } catch (e: NumberFormatException) {
//                return java.lang.Double.valueOf(0.0)
//            }
//
//        } else if (someClass == Float::class.java) {
//            try {
//                return if (scaler == null) java.lang.Float.valueOf(value) else scaler.scaleBack(
//                    java.lang.Float.valueOf(
//                        value
//                    )
//                )
//            } catch (e: NumberFormatException) {
//                return java.lang.Float.valueOf(0f)
//            }
//
//        } else if (someClass == Int::class.java) {
//            try {
//                return if (scaler == null) Integer.valueOf(value) else scaler.scaleBack(Integer.valueOf(value))
//            } catch (e: NumberFormatException) {
//                try {
//                    return if (scaler == null) java.lang.Double.valueOf(value).toInt() else scaler.scaleBack(
//                        java.lang.Double.valueOf(
//                            value
//                        )
//                    ).intValue()
//                } catch (e2: NumberFormatException) {
//                    return Integer.valueOf(0)
//                }
//
//            }
//
//        } else if (someClass == Long::class.java) {
//            try {
//                return if (scaler == null) java.lang.Long.valueOf(value) else scaler.scaleBack(
//                    java.lang.Long.valueOf(
//                        value
//                    )
//                )
//            } catch (e: NumberFormatException) {
//                try {
//                    return if (scaler == null) java.lang.Double.valueOf(value).toLong() else scaler.scaleBack(
//                        java.lang.Double.valueOf(
//                            value
//                        )
//                    ).longValue()
//                } catch (e2: NumberFormatException) {
//                    return java.lang.Long.valueOf(0)
//                }
//
//            }
//
//        } else if (someClass == String::class.java) {
//            return value
//        }
//        return null
//    }

    private fun announceLine(value: String): String {
        val str = "   *" + "                          ".substring(0, 26 - value.length / 2) +
                value + "                             "
        return str.substring(0, 56) + "*\n"
    }

    fun announce(title: String, msg: String) {

        LLog.i(
            "\n" +
                    "   ******************************************************\n" +
                    "   *                                                    *\n" +
                    "   *                                                    *\n" +
                    announceLine(title) +
                    announceLine(msg) +
                    "   *                                                    *\n" +
                    "   *                                                    *\n" +
                    "   ******************************************************"
        )

    }


}
