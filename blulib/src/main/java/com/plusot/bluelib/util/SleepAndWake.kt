package com.plusot.bluelib.util

import android.os.Handler
import android.os.Looper
import com.plusot.bluelib.log.LLog
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


object SleepAndWake {
    //    private val DEBUG = false
    private val KEEP_ALIVE_TIME = 10
    private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()
    private val MAX_THREADS = 1000
    private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
    private val workQueue = LinkedBlockingQueue<Runnable>()
    private val usageLock = Any()
    private val usage = mutableMapOf<String, EnumMap<ThreadType, Int>>()

    private val threadPool = ThreadPoolExecutor(
        NUMBER_OF_CORES, // Initial pool size
        MAX_THREADS, // Max pool size
        KEEP_ALIVE_TIME.toLong(),
        KEEP_ALIVE_TIME_UNIT,
        workQueue
    )

    private enum class ThreadType {
        MAIN,
        IN_THREAD,
        NEW_THREAD,
        POOL_THREAD
    }

    class Stopper(private val handler: Handler?, private val runnable: Runnable) {
        private val lock = Any()

        fun stop(): Boolean {
            if (handler != null)
                synchronized(lock) {
                    //LLog.d("Call back removed")
                    handler.removeCallbacks(runnable)
                    return true
                }
            return false
        }

        fun execAndStop(): Boolean {
            if (handler != null)
                synchronized(lock) {
                    handler.removeCallbacks(runnable)
                    handler.post(runnable)
                    return true
                }
            return false
        }
    }

    private fun incCount(caller: String, type: ThreadType) {
        var map: EnumMap<ThreadType, Int>?
        synchronized(usageLock) {
            map = usage[caller]
            if (map == null) {
                usage[caller] = EnumMap(ThreadType::class.java)
                map= usage[caller]
            }
            map?.let {
                val count: Int? = it[type]
                if (count == null)
                    it.put(type, 1)
                else
                    it.put(ThreadType.MAIN, count + 1)
            }
        }
    }

    fun runInMain(caller: String, delayInMillis: Long, block: () -> Unit): Stopper {
        incCount(caller, ThreadType.MAIN)
        val handler = Handler(Looper.getMainLooper())
        val realRunnable = Runnable { block.invoke() }
        handler.postDelayed(realRunnable, delayInMillis)
        return Stopper(handler, realRunnable)
    }

//    fun runInMain(caller: Class<*>, delayInMillis: Long, block: () -> Unit): Stopper {
//        incCount(caller.simpleName, ThreadType.MAIN)
//        val handler = Handler(Looper.getMainLooper())
//        val realRunnable = Runnable { block.invoke() }
//        handler.postDelayed(realRunnable, delayInMillis)
//        return Stopper(handler, realRunnable)
//    }

    fun runInMain(delayInMillis: Long, block: () -> Unit): Stopper {
        incCount("Anonymous", ThreadType.MAIN)
        val handler = Handler(Looper.getMainLooper())
        val realRunnable = Runnable { block.invoke() }
        handler.postDelayed(realRunnable, delayInMillis)
        return Stopper(handler, realRunnable)
    }

//    fun runInMain(caller: String, block: () -> Unit) {
//        incCount(caller, ThreadType.MAIN)
//        Handler(Looper.getMainLooper()).post(block)
//    }

    fun runInMain(block: () -> Unit) {
        incCount("Anonymous", ThreadType.MAIN)
        Handler(Looper.getMainLooper()).post(block)
    }

    fun runInNewThread(block: () -> Unit) {
        try {
            incCount("Anonymous", ThreadType.POOL_THREAD)
            threadPool.execute(block)
        } catch (e: Exception) {
            LLog.e("Exception while creating thread", e)
        }

    }

//    fun runInNewThread(caller: String, block: () -> Unit) {
//        try {
//            incCount(caller, ThreadType.POOL_THREAD)
//            threadPool.execute(block)
//        } catch (e: Exception) {
//            LLog.e("Exception while creating thread", e)
//        }
//
//    }

//    fun logPoolSize() {
//        //        LLog.i("Number of cores = " + NUMBER_OF_CORES + ", number of threads in pool = " + threadPool.getPoolSize());
//        val stringer = Stringer("\n")
//        val callers: Set<String>
//        synchronized(usageLock) {
//            callers = usage.keys
//        }
//        for (caller in callers) {
//            synchronized(usageLock) {
//                val map = usage[caller]
//                if (map != null)
//                    for (type in map.keys) {
//                        val count = map[type]
//                        stringer.append(
//                            Globals.TAB + Format.format(
//                                caller,
//                                Globals.TAG_WIDTH
//                            ) + Format.format(type.toString(), 15) + " " + count
//                        )
//                    }
//            }
//        }
//        if (DEBUG) LLog.i("SleepAndWakeUsage:\n$stringer")
//    }

}

fun runInMain(block: () -> Unit) = SleepAndWake.runInMain(block)
fun runInMain(delay: Long, block: () -> Unit) = SleepAndWake.runInMain(delay, block)
