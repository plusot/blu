package com.plusot.bluelib.util

import android.annotation.SuppressLint
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog


import java.io.File
import java.io.FilenameFilter
import java.text.DateFormatSymbols
import java.util.Arrays
import java.util.HashMap
import java.util.Locale
import java.util.TreeSet

class FileExplorer(private val path: String, private val spec: String, private val ext: String, val tag: Int) {

    val years: Array<String>
        get() {
            val folder = File(path)
            val files = folder.list { _, filename ->
                filename.contains(spec + Globals.FILE_DETAIL_DELIM) && filename.contains(ext)
            }

            val years = TreeSet<String>()
            if (files != null)
                for (file in files) {
                    val shrtFile = file.replace(ext, "")
                    val parts =
                        shrtFile.split(Globals.FILE_DETAIL_DELIM.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (parts.size < 1) continue
                    years.add(parts[parts.size - 1].substring(0, 4))
                }
            return years.toTypedArray()
        }

    fun getMonths(year: String): Array<Int> {
        val folder = File(path)
        val files = folder.list { _, filename ->
            filename.contains(spec + Globals.FILE_DETAIL_DELIM + year) && filename.contains(ext)
        }

        val months = TreeSet<Int>()
        if (files != null)
            for (file in files) {
                val shrtFile = file.replace(ext, "")
                val parts =
                    shrtFile.split(Globals.FILE_DETAIL_DELIM.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (parts.size < 1) continue
                var intPart: String? = null
                try {
                    intPart = parts[parts.size - 1].substring(4, 6)
                    val month = Integer.valueOf(intPart)
                    months.add(month)
                } catch (e: NumberFormatException) {
                    LLog.e("getMonths: Could not convert: " + intPart!!)
                }

            }
        return months.toTypedArray()
    }

    fun getDays(year: String, month: Int): Array<Int> {
        val folder = File(path)
        val files = folder.list { _, filename ->
            filename.contains(
                spec + Globals.FILE_DETAIL_DELIM + year + String.format(
                    "%02d",
                    month
                )
            ) && filename.contains(ext)
        }

        val days = TreeSet<Int>()
        if (files != null)
            for (file in files) {
                var shrtFile = file.replace(ext, "")
                val parts =
                    shrtFile.split(Globals.FILE_DETAIL_DELIM.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (parts.size < 1) continue
                var intPart: String? = null
                try {
                    intPart = parts[parts.size - 1].substring(6, 8)
                    val day = Integer.valueOf(intPart)
                    days.add(day)
                } catch (e: NumberFormatException) {
                    LLog.i("getMonths: Could not convert: " + intPart!!)
                }

            }
        return days.toTypedArray()
    }

    fun getTimes(year: String, month: Int, day: Int): Array<String> {
        val folder = File(path)
        val files = folder.list { _, filename ->
            filename.contains(
                spec + Globals.FILE_DETAIL_DELIM + year + String.format(
                    "%02d",
                    month
                ) + String.format("%02d", day)
            ) && filename.contains(ext)
        }

        val times = TreeSet<String>()
        if (files != null) for (file in files) {
            val shrtFile = file.replace(ext, "")
            val parts = shrtFile.split(Globals.FILE_DETAIL_DELIM.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (parts.size < 1) continue
            var timePart: String

            timePart = parts[parts.size - 1].substring(9, 15)
            timePart = timePart.substring(0, 2) + ":" + timePart.substring(2, 4) + ":" + timePart.substring(4, 6)
            times.add(timePart)

        }
        return times.toTypedArray()
    }

    private fun getFileList(dateSpec: String?): Array<String>? {
        val folder = File(path)

        var fileList: Array<String>? = folder.list(FilenameFilter { _, filename ->
            if (dateSpec != null) {
                if (filename.contains(spec + Globals.FILE_DETAIL_DELIM + dateSpec) && filename.contains(ext))
                    return@FilenameFilter true
            } else {
                if (filename.contains(spec + Globals.FILE_DETAIL_DELIM) && filename.contains(ext))
                    return@FilenameFilter true
            }
            false
        })

        if (fileList == null || fileList.size == 0) return null

        if (dateSpec == null || dateSpec == "") {
            Arrays.sort(fileList, java.text.Collator.getInstance())

            val maxString = fileList[fileList.size - 1].substring(spec.length + 1, spec.length + 9)
            fileList = folder.list { _, filename ->
                filename.contains(spec + Globals.FILE_DETAIL_DELIM + maxString) && filename.contains(ext)
            }
        }
        return fileList
    }

    fun getFileList(time: Long): Array<String>? {
        return getFileList(TimeUtil.formatTime(time, "yyyyMMdd"))
    }

    fun getFileList(year: Int, month: Int, day: Int): Array<String>? {
        return getFileList(String.format("%04d%02d%02d", year, month, day))
    }

    fun getFileList(year: String, month: Int, day: Int, time: String): String? {
        val list = getFileList(
            year + String.format("%02d%02d", month, day) + "-" + time.substring(0, 2) + time.substring(
                3,
                5
            ) + time.substring(6, 8)
        )
        return if (list == null || list.size == 0) null else list[0]
    }

    fun getFileList(year: String, month: Int, day: Int): Array<String>? {
        return getFileList(year + String.format("%02d%02d", month, day))
    }

    fun getFileListWithPath(date: Long): Array<String>? {
        val list = getFileList(TimeUtil.formatDateShort(date))
        if (list == null || list.isEmpty()) return null
        val files = mutableListOf<String>()
        for (file in list) { files.add(path + file) }
        return files.toTypedArray()
    }

    class FileList(val items: Array<String>, val lookup: Map<String, String>)

    companion object {

        fun getMonths(year: String, months: Array<Int>): Array<String> {
            val sym = DateFormatSymbols(Locale.getDefault())
            val monthNames = sym.months
            val monthStrs = mutableListOf<String>()
            for (month in months) {
                monthStrs.add(monthNames[(month - 1) % 12] + " " + year)
            }
            return monthStrs.toTypedArray()
        }

//        fun getDays(year: String, month: String, days: Array<Int>): Array<String> {
//            val dayStrs = mutableListOf<String>()
//            for (day in days) {
//                dayStrs.add("$day $month")
//            }
//            return dayStrs.toTypedArray()
//        }

        fun getFileList(path: String, spec: String, exts: Array<String>, desc: Boolean): Array<String>? {
            val folder = File(path)

            val fileList = folder.list(FilenameFilter { _, filename ->
                for (ext in exts) {
                    if (filename.contains(spec) && filename.contains(ext)) return@FilenameFilter true
                }
                false
            })

            if (fileList == null || fileList.size == 0) return null

            Arrays.sort(fileList, java.text.Collator.getInstance())
            if (desc) {
                val l = fileList.size
                for (i in 0 until l / 2) {
                    val temp = fileList[i]
                    fileList[i] = fileList[l - i - 1]
                    fileList[l - i - 1] = temp
                }
            }
            return fileList
        }

        @SuppressLint("DefaultLocale")
        fun getFileList(paths: Array<String>, exts: Array<String>, desc: Boolean): FileList? {
            //List<String> list = new ArrayList<String>();
            val map = HashMap<String, String>()
            for (path in paths) {
                val folder = File(path)

                val fileList = folder.list(FilenameFilter { _, filename ->
                    for (ext in exts) {
                        if (filename.toLowerCase(Locale.US).contains(ext.toLowerCase(Locale.US)))
                            return@FilenameFilter true
                    }
                    false
                })
                if (fileList != null && fileList.size > 0)
                    for (item in fileList) {
                        map[item] = path + item
                        //list.addAll(Arrays.asList(fileList));
                    }
            }
            val fileList = map.keys.toTypedArray() //list.toArray(new String[0]);

            if (fileList.size == 0) return null

            Arrays.sort(fileList, java.text.Collator.getInstance())
            if (desc) {
                val l = fileList.size
                for (i in 0 until l / 2) {
                    val temp = fileList[i]
                    fileList[i] = fileList[l - i - 1]
                    fileList[l - i - 1] = temp
                }
            }
            return FileList(fileList, map)
        }
    }

}
