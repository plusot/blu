package com.plusot.bluelib.util

import android.location.Location

object Calc {

    fun bearing(loc1: Location, loc2: Location): Double {
        //http://www.movable-type.co.uk/scripts/latlong.html
        val lat1 = loc1.latitude * Math.PI / 180
        val lng1 = loc1.longitude * Math.PI / 180
        val lat2 = loc2.latitude * Math.PI / 180
        val lng2 = loc2.longitude * Math.PI / 180
        //double dLat = lat2 - lat1;
        val dLon = lng2 - lng1

        val y = Math.sin(dLon) * Math.cos(lat2)
        val x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon)
        val atan = Math.atan2(y, x)
        return if (java.lang.Double.isNaN(atan)) java.lang.Double.NaN else atan * 180 / Math.PI
    }

    /*public static double distance(Location loc1, Location loc2) {
		//Haversine formula
		double EARTH_RADIUS = 6367.45;
		double lat1 = loc1.getLatitude() * Math.PI / 180;
		double lng1 = loc1.getLongitude() * Math.PI / 180;
		double lat2 = loc2.getLatitude() * Math.PI / 180;
		double lng2 = loc2.getLongitude() * Math.PI / 180;
		double dLat = lat2 - lat1;
		double dLon = lng2 - lng1;
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return EARTH_RADIUS*c;
	}*/

    fun absChange(d1: Double, d2: Double): Double {
        val delta = Math.abs(d1 - d2)
        val mean = (Math.abs(d1) + Math.abs(d2)) / 2
        if (mean == 0.0 && delta > 0) return 1.0
        return if (mean == 0.0 && delta == 0.0) 0.0 else delta / mean
    }

    fun length(d: DoubleArray): Double {
        var sum = 0.0
        for (aD in d) {
            sum += aD * aD
        }
        return Math.sqrt(sum)
    }

    fun length(d: FloatArray): Float {
        var sum = 0f
        for (aD in d) {
            sum += aD * aD
        }
        return Math.sqrt(sum.toDouble()).toFloat()
    }

    fun unitVector(d: DoubleArray): DoubleArray {
        val v = DoubleArray(d.size)
        val length = length(d)
        for (i in d.indices) {
            v[i] = d[i] / length
        }
        return v
    }

    fun pow2(power: Int): Long {
        if (power == 0) return 1
        return 2 * pow2(power - 1)
    }

}
