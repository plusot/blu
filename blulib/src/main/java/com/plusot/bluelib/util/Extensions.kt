package com.plusot.bluelib.util

import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import androidx.annotation.ColorRes
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.plusot.bluelib.BuildConfig
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import java.util.*

/**
 * File: Extensions
 * Package: com.bronstenkate.diabetes24.util
 * Project: Diabetes24
 *
 * Created by Peter Bruinink on 12/12/17.
 * Copyright © 2017 Brons Ten Kate. All rights reserved.
 */


const val downArrow = '▾'

enum class Success {
    failed,
    success;
}
enum class OnOff {
    OFF,
    ON;
}

fun saveCall(block: () -> Unit) {
    try {
        LLog.d("Calling block from \"${LLog.getMethod(4)}\"")
        block()
    } catch (e: Exception) {
        LLog.e("Exception raised for \"${LLog.getMethod(4)}\": $e")
    }
}

fun Boolean.toInt() = if (this) 1 else 0
fun Boolean.toDouble() = if (this) 1.0 else 0.0
fun Int.toBool() = this > 0

fun Int.getString() : String {
    val context = Globals.appContext
    return context?.getString(this) ?: "Unknown String $this"
}

fun Int.getString(vararg formatArgs: Any) : String {
    val context = Globals.appContext
    return context?.getString(this, *formatArgs) ?: "Unknown String $this"
}


fun Int.getColor() : Int {
    val context = Globals.appContext
    return context?.resources?.getColored(this) ?: 0xFFAAAAAA.toInt()
}

fun Double.format(decimals: Int): String = Format.format(this, decimals)
fun Double.formatLocal(decimals: Int): String = Format.format(this, decimals, Locale.getDefault())

fun Float.format(decimals: Int): String = Format.format(this.toDouble(), decimals)
fun FloatArray.format(decimals: Int): String = this.fold( "", { acc, value -> "${if (acc == "") "" else "$acc, "} ${Format.format(value.toDouble(), decimals)}"} )

fun String.toIntOr(default: Int = 0): Int = this.toIntOrNull() ?: default


fun String.toIntOr(func: (Int) -> Unit) {
    val i = this.toIntOrNull()
    if (i != null) func(i)
}

fun String.toDoubleOr(default: Double = 0.0): Double = this.toDoubleOrNull() ?: default

fun String.sub(length: Int): String {
    val l = Math.min(length, this.length)
    return this.substring(0, l)
}

fun String.addIfNotEmpty(new: String) = if (this.isEmpty()) this else "$this$new"

fun String.toLongOr(default: Long = 0): Long = this.toLongOrNull() ?: default

fun String.capitalize():String {
    return if (this.length < 2) this.toUpperCase() else Character.toUpperCase(this[0]) + this.substring(1)
}

fun <E> MutableList<E>.addIfNew(element: E?): Boolean {
    if (element == null) return false
    return if (!this.contains(element)) {
        this.add(element)
        true
    } else
        false
}

fun <K, V> MutableMap<K, V>.addIfNew(key: K, value: V) {
    val v: V? = get(key)
    if (v == null) put(key, value)
}

fun <E> MutableList<E>.addIfNew(element: E, onIf: (E) -> Unit) {
    if (!this.contains(element)) {
        this.add(element)
        onIf(element)
    }
}

fun <T> MutableList<T>.next(current: T?): T? {
    if (current == null) return this.firstOrNull()
    var index = this.indexOf(current)
    if (index == -1) return this.firstOrNull()
    index++
    index %= this.size
    return this[index]
}

fun <T> List<T>.nextFromList(current: T?): T? {
    if (current == null) return this.firstOrNull()
    var index = this.indexOf(current)
    if (index == -1) return this.firstOrNull()
    index++
    return if (index >= this.size)
        null
    else
        this[index]
}

fun <T> MutableList<T>.nextElement(current: T): T {
    var index = this.indexOf(current)
    if (index == -1) return current
    index++
    index %= this.size
    return this[index] ?: current
}

fun <T> MutableList<T>.addMore(vararg new: T) {
    new.forEach { this.add(it) }
}

fun <T> MutableSet<T>.addOrNot(new: T?) {
    if (new != null) this.add(new)
}


inline fun <reified E : Enum<E>> enumSetOf(): EnumSet<E> = EnumSet.noneOf(E::class.java)

val Int.dp2Px: Int
    get() = Math.round(this * (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

//val Int.px2Dp: Int
//    get() = Math.round(this / (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

inline fun <reified T : Enum<T>> Int.toEnum() : T {
    return enumValues<T>()[this % enumValues<T>().size]
}

inline fun <reified T : Enum<T>> Boolean.toEnum() : T = if (this && enumValues<T>().size > 1)
    enumValues<T>()[1]
else
    enumValues<T>()[0]


inline fun <reified T : Enum<T>> String.toEnum() : T {
    enumValues<T>().forEach { if (it.toString().toLowerCase() == this.toLowerCase()) return it }
    return enumValues<T>()[0]
}

inline fun <reified T : Enum<T>> String.toEnum(func: (T) -> Unit)  {
    enumValues<T>().forEach { if (it.toString().toLowerCase() == this.toLowerCase()) func(it) }
}

fun Resources.getColored(@ColorRes color: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.getColor(color, null)
    } else {
        @Suppress("DEPRECATION")
        this.getColor(color)
    }
}

fun Context.getColored(@ColorRes color: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.resources.getColor(color, null)
    } else {
        @Suppress("DEPRECATION")
        this.getColor(color)
    }
}

fun randomInt(maxPlus1 : Int) = (1.0 * (maxPlus1) * Math.random()).toInt()

fun <T> filteredListOf(vararg elements: T?): List<T> {
    val filtered = mutableListOf<T>()
    elements.forEach { if (it != null) filtered.add(it)}
    return filtered
}

fun Dialog.showIt(): Dialog {
    this.show()
    return this
}

fun Context.getDefaultSharedPreferences() = getSharedPreferences(this.packageName + "_preferences", Context.MODE_PRIVATE)



inline fun <reified T> Gson.fromJson(string: String): T? = try {
    this.fromJson<T>(string, T::class.java)
} catch (e: JsonSyntaxException) {
    null
}

