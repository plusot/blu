package com.plusot.bluelib.util


import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class ShellExec {

    @Throws(IOException::class, InterruptedException::class)
    fun exec(cmds: Array<String>, tag: Int, callback: ShellCallback): Int {
        if (!Globals.runMode.isRun) return -1
        var exitVal = -1
        try {

            val pb = ProcessBuilder(*cmds)
            //pb.directory(fileBinDir);

            val cmdlog = StringBuilder()

            for (cmd in cmds) {
                if (cmdlog.isNotEmpty()) cmdlog.append(' ')
                cmdlog.append(cmd)
            }

            //LLog.v(Common"" + cmdlog.toString());

            //	pb.redirectErrorStream(true);
            val process = pb.start()

            // any error message?
            val errorGobbler = StreamGobbler(process.errorStream, "ERROR", callback)

            // any output?
            val outputGobbler = StreamGobbler(process.inputStream, "OUTPUT", callback)

            // kick them off
            errorGobbler.start()
            outputGobbler.start()

            exitVal = process.waitFor()

            callback.processComplete(tag, cmdlog.toString(), exitVal)
        } catch (e: Exception) {
            LLog.e("Could not execute${cmds.fold("", { acc, cmd -> "$acc $cmd" })}: ${e.message}")
        }

        return exitVal
    }

    interface ShellCallback {
        fun shellOut(shellLine: String)

        fun processComplete(tag: Int, commandLine: String, exitValue: Int)
    }

    internal inner class StreamGobbler(var inStream: InputStream, var type: String, var callback: ShellCallback?) : Thread() {

        init {
            name = "StreamGobler$id"
        }

        override fun run() {
            try {
                val isr = InputStreamReader(inStream)
                val br = BufferedReader(isr)
                var line = br.readLine()
                while (line != null) {
                    callback?.shellOut(line)
                    line = br.readLine()
                }

            } catch (ioe: IOException) {
                LLog.e("error reading shell slog", ioe)
            }

        }
    }
}