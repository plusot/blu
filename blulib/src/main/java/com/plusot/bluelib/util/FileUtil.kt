package com.plusot.bluelib.util

/**
 * Package: com.plusot.bluelib.util
 * Project: kaleido-app
 *
 *
 * Created by Peter Bruinink on 2019-07-02.
 * Copyright © 2019 Plusot. All rights reserved.
 */

import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.io.RandomAccessFile
import java.nio.channels.FileChannel
import java.util.zip.GZIPOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

object FileUtil {
    private val BUFFER = 2048


    fun concat(start: String, end: String, infile: File, outfile: File): Boolean {

        var inChan: FileChannel? = null
        var outChan: FileChannel? = null
        var fw: PrintWriter? = null
        try {
            fw = PrintWriter(BufferedWriter(FileWriter(outfile, false)))
            fw.write(start)
            fw.flush()
        } catch (e: FileNotFoundException) {
            LLog.e("concatFiles, start: File Not Found", e)
            return false
        } catch (e: IOException) {
            LLog.e("concatFiles, start: IO Exception", e)
            return false
        } finally {
            fw?.close()
        }

        var outStream: FileOutputStream? = null
        var inStream: RandomAccessFile? = null
        try {
            // create the channels appropriate for appending:
            outStream = FileOutputStream(outfile, true)
            outChan = outStream.channel
            inStream = RandomAccessFile(infile, "r")
            inChan = inStream.channel

            val startSize = outfile.length()
            val inFileSize = infile.length()
            var bytesWritten: Long = 0

            //set the position where we should start appending the data:
            outChan!!.position(startSize)
            var startByte = outChan.position()

            while (bytesWritten < inFileSize) {
                bytesWritten += outChan.transferFrom(inChan, startByte, inFileSize)
                startByte = bytesWritten + 1
            }


        } catch (e: FileNotFoundException) {
            LLog.e("concatFiles, work: File Not Found", e)
            return false
        } catch (e: IOException) {
            LLog.e("concatFiles, work: IO Exception", e)
            return false
        } finally {
            try {
                if (inChan != null) inStream!!.close()
                if (outChan != null) outStream!!.close()
            } catch (e: IOException) {
                LLog.i("Exception closing streams", e)
            }

        }
        try {
            fw = PrintWriter(BufferedWriter(FileWriter(outfile, true)))
            fw.write(end)
            fw.flush()
            fw.close()
            return true
        } catch (e: FileNotFoundException) {
            LLog.e("concatFiles, end: File Not Found", e)
            return false
        } catch (e: IOException) {
            LLog.e("concatFiles, end: IO Exception", e)
            return false
        } finally {
            fw!!.close()
        }
    }

    fun toGZip(start: String?, end: String?, infile: File, outfile: File): Boolean {
        var outStream: GZIPOutputStream? = null
        var inStream: RandomAccessFile? = null
        if (!infile.exists()) return false
        var result = true
        try {
            // create the channels appropriate for appending:
            outStream = GZIPOutputStream(BufferedOutputStream(FileOutputStream(outfile)))
            if (start != null) outStream.write(start.toByteArray())
            inStream = RandomAccessFile(infile, "r")

            val bytes = ByteArray(BUFFER)
            var bytesRead: Int = inStream.read(bytes)

            while (bytesRead != -1) {
                outStream.write(bytes, 0, bytesRead)
                bytesRead = inStream.read(bytes)
            }
            if (end != null) outStream.write(end.toByteArray())
        } catch (e: FileNotFoundException) {
            LLog.e("concatFilesToGZip, work: File Not Found", e)
            return false
        } catch (e: IOException) {
            LLog.e("concatFilesToGZip, work: IO Exception", e)
            return false
        } finally {
            try {
                inStream?.close()
                outStream?.close()
            } catch (e: IOException) {
                LLog.e("concatFilesToGZip, work: IO Exception while closing files", e)
                result = false
            }

        }
        return result
    }

    //	public static boolean toGZip(File infile, File outfile) {
    //		return toGZip(null, null, infile, outfile);
    //	}


    fun toZip(files: Array<String>, zipFile: String) {
        try {
            var origin: BufferedInputStream
            val dest = FileOutputStream(zipFile)

            val out = ZipOutputStream(BufferedOutputStream(dest))

            val data = ByteArray(BUFFER)

            for (file in files) {
                LLog.v("toZip adding: $file")
                val fi = FileInputStream(file)
                origin = BufferedInputStream(fi, BUFFER)
                val entry = ZipEntry(file.substring(file.lastIndexOf("/") + 1))
                out.putNextEntry(entry)
                var count: Int = origin.read(data, 0, BUFFER)
                while (count != -1) {
                    out.write(data, 0, count)
                    Thread.yield()
                    count = origin.read(data, 0, BUFFER)
                }
                origin.close()
                Thread.sleep(5)
            }

            out.close()
        } catch (e: Exception) {
            LLog.e("toZip exception: " + e.message)
        }

    }

    private fun systemLogToFile(pid: Int, cmd: Array<String>, endCmd: Array<String>) {
        try {
            val proc = Runtime.getRuntime().exec(cmd)
            val reader = BufferedReader(InputStreamReader(proc.inputStream), 1024)
            var line: String? = reader.readLine()
            if (line != null) {
                var file = File(Globals.logPath)
                if (!file.isDirectory()) file.mkdirs()
                val fileName = Globals.logPath +
                        Globals.SYSLOG_SPEC +
                        Globals.FILE_DETAIL_DELIM + TimeUtil.fileNameDate() +
                        Globals.FILE_DETAIL_DELIM + pid +
                        Globals.LOG_EXT
                file = File(fileName)

                file.createNewFile()
                val os = PrintWriter(BufferedWriter(FileWriter(file, true)))
                do {
                    os.println(line)
                    Thread.yield()
                    line = reader.readLine()
                } while (line != null)
                os.close()
            }
            reader.close()
            Runtime.getRuntime().exec(endCmd)
        } catch (e: IOException) {
            LLog.e("IOException " + e.message)
        }

    }

    fun systemLogToFile() {
        val pid = android.os.Process.myPid()
        val previousPid = PreferenceKey.PREVIOUS_PID.int

        if (pid != previousPid && previousPid != 0)
            systemLogToFile(
                previousPid,
                arrayOf("logcat", "--pid=$previousPid", "-d"),
                arrayOf("logcat", "--pid=$previousPid", "-c")
            )
        PreferenceKey.PREVIOUS_PID.int = pid
        systemLogToFile(
            pid,
            arrayOf("logcat", "--pid=$pid", "-d"),
            arrayOf("logcat", "--pid=$pid", "-c")
        )
    }

}