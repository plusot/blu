package com.plusot.bluelib.util

import java.util.Locale

object Format {

    fun format(value: Double, decimals: Int, locale: Locale = Locale.US): String {
        if (decimals < 0) return format(value)
        /*if (Math.abs(value) < 1.0 && decimals == 0) decimals = 1;
		if (Math.abs(value) < 0.1 && decimals <= 1) decimals = 2;
		if (Math.abs(value) < 0.01 && decimals <= 2) decimals = 3;
		 */
        val format = "%1." + decimals + "f"
        return String.format(locale, format, value).trim { it <= ' ' }
    }

    fun format(value: Float, decimals: Int, locale: Locale = Locale.US) = format(value.toDouble(), decimals, locale)

    fun format(value: Double, pre: Int, decimals: Int): String {
        if (decimals < 0) return format(value)
        /*if (Math.abs(value) < 1.0 && decimals == 0) decimals = 1;
		if (Math.abs(value) < 0.1 && decimals <= 1) decimals = 2;
		if (Math.abs(value) < 0.01 && decimals <= 2) decimals = 3;
		 */
        val format = "%" + (pre + 1 + decimals) + "." + decimals + "f"
        return String.format(Locale.US, format, value)
    }

    fun format(value: Int, pre: Int): String {
        /*if (Math.abs(value) < 1.0 && decimals == 0) decimals = 1;
		if (Math.abs(value) < 0.1 && decimals <= 1) decimals = 2;
		if (Math.abs(value) < 0.01 && decimals <= 2) decimals = 3;
		 */
        val format = "%" + pre + "d"
        return String.format(Locale.US, format, value)
    }

    fun format(value: Double): String {
        val decimals: Int
        val power = Math.log10(Math.abs(value))
        if (power > 1.5)
            decimals = 0
        else
            decimals = 2 - Math.round(power).toInt()

        val format = "%1." + decimals + "f"
        return String.format(Locale.US, format, value)
    }


    fun format(value: Float) = format(value.toDouble())

    fun format4(value: Double): String {
        return String.format(Locale.US, "%1.4f", value)
    }

    fun format2(value: Double): String {
        return String.format(Locale.US, "%1.2f", value)
    }

    fun format(string: String, length: Int): String {
        val format = "%" + length + "s"
        return (string + String.format(format, " ")).substring(0, length)
    }


}
