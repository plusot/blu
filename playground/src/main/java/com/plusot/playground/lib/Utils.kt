package com.plusot.playground.lib

import java.util.*

/**
 * Package: com.plusot.playground
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-05-28.
 * Copyright © 2019 Plusot. All rights reserved.
 */

fun isLeapYear(year: Long): Boolean {
    return year % 4 == 0L && (year % 400 == 0L || year % 100 != 0L)
}

fun daysUpToYear(year: Long): Long {
    return if (year == 0L) 0 else (year - 1) / 4 + year * 365 - (year - 1) / 100 + (year - 1) / 400
}

fun daysUpTo(unixTime: Long = System.currentTimeMillis()): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = unixTime
    return daysUpToYear(calendar.get(Calendar.YEAR).toLong()) + dayInYear(
        unixTime
    )
}

fun dayInYear(unixTime: Long): Int {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = unixTime
    return calendar.get(Calendar.DAY_OF_YEAR)
}

fun pow2(power: Int): Long {
    if (power == 0) return 1
    return 2 * pow2(power - 1)
}

fun toSecondsSinceYear0(unixTime: Long = System.currentTimeMillis()): Long {
    val days1970 = daysUpToYear(1970)
    val seconds = unixTime / 1000
    val totalSeconds = seconds + days1970 * 86400
    println("Days until 1970 = $days1970, seconds since 1970 = $seconds: Seconds since year 0 = $totalSeconds ${pow2(
        38
    )}" )
    return totalSeconds
}

fun fromSecondsSinceYear0(zeroTime: Long): Long {
    val seconds1970 = daysUpToYear(1970) * 86400
    if (zeroTime < seconds1970) return 0L
    val time1970 = (zeroTime - seconds1970) * 1000L
    println("Time calculated ${Date(time1970)}" )
    return time1970
}

fun Int.toBin(limit: Int = 32): String {
    val l = Math.min(limit - 1, 31)
    val char = if ((this shr l) and 0x1 == 0x1) '1' else '0'
    return if (l > 0) {
        "$char${this.toBin(l)}"
    } else
        char.toString()
}

fun Long.toBin(limit: Int = 64): String {
    val l = Math.min(limit - 1, 63)
    val char = if ((this shr l) and 0x1L == 0x1L) '1' else '0'
    return if (l > 0) {
        "$char${this.toBin(l)}"
    } else
        char.toString()
}

fun longMask(bitLength: Int, shift: Int = 0): Long {
    var mask = 0L
    repeat(bitLength) {
        mask = mask shl 1
        mask = mask or 0x1
    }
    return mask shl shift
}

fun ByteArray.getString(i: Int, length: Int): String {
    if (i >= this.size) return ""
    return try {
        String(this.copyOfRange(i, i + Math.min(length, this.size - i)))
    } catch (_: Exception) {
        ""
    }
}

fun Int.unsignedToSigned(size: Int): Int {
    var unsigned = this
    if (unsigned and (1 shl size - 1) != 0) {
        unsigned = -1 * ((1 shl size - 1) - (unsigned and (1 shl size - 1) - 1))
    }
    return unsigned
}

fun Int.toSigned(size: Int): Int =
    if (this and (1 shl size - 1) != 0)
        -1 * ((1 shl size - 1) - (this and (1 shl size - 1) - 1))
    else
        this

fun Byte.toUnsigned(): Int =
    0xFF and this.toInt()

fun Byte.checkBitOnIndex(index: Int): Boolean {
    val bit = 1 shl index
    return this.toInt() and bit == bit
}

fun Int.toHex() = String.format("0x%X", this)

fun Long.toHex() = String.format("0x%X", this)

fun Byte.toHex() = String.format("0x%X", this)

fun String.addIfNotEmpty(new: String) = if (this.isEmpty()) this else "$this$new"

fun String.toIntOr(default: Int = 0): Int = this.toIntOrNull() ?: default

fun ByteArray.toDelimitedString(): String = this.fold("") { acc, value -> "${acc.addIfNotEmpty("|")}$value"}
fun ByteArray.toIntString() = this.fold("") { acc, value -> "${acc.addIfNotEmpty(" ")}$value"}


fun String?.toByteArray(): ByteArray {
    if (this == null) return byteArrayOf()
    val parts = this.split('|')
    val array = ByteArray(parts.size)
    parts.forEachIndexed { index, s -> array[index] = s.toIntOr(0).toByte() }
    return array
}

fun Int.toZeroLeading(zeros: Int): String {
    val formatStr = "%0${zeros}d"
    return String.format(formatStr, this)
}





