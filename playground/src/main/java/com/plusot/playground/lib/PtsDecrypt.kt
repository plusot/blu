package com.plusot.playground.lib

/**
 * Package: com.plusot.playground.lib
 * Project: blu
 *
 * Created by Peter Bruinink on 2019-11-26.
 * Copyright © 2019 Plusot. All rights reserved.
 */

const val newLine = '\n'

enum class DecryptResult {
    SUCCESS,
    DECRYPTION_ERROR,
    LENGTH_ERROR,
    FINAL_CHECK_ERROR
}

class DockResult {
    var testtype = ""
    var result = ""
    var units = ""
    var lot = ""
    var serialnum = ""
    var QCCode = ""
    var testID = ""
    var testsRemaining = ""
    var testTime = ""


    override fun toString(): String = "TestType: $testtype$newLine" +
            "Result: $result$newLine" +
            "Units: $units$newLine" +
            "Lot: $lot$newLine" +
            "SerialNum: $serialnum$newLine" +
            "QCCode: $QCCode$newLine" +
            "testID: $testID$newLine" +
            "tests Remaining: $testsRemaining$newLine" +
            "Timestamp: ${testTime}"


}
object PtsDecrypt {

    //Long used for UInt32 types!!!!
//        val ACK_PACKET_TYPE = 2
    private val customerKey = longArrayOf(0x718fa332, 0xF3C2F892, 0x9b578a23, 0xB7C8FE01)
    private const val customerToMeter = 0x98D4CF97L
    private const val meterToCustomer = 0x88D4CF97L
    private const val uint32Mask = 0xFFFFFFFFL

    private fun ByteArray.getPacketType(): Long {
        if (this.size < 8) return -1   // invalid packet
        return this.wordFromFourBytes(0)
    }

    fun ackTestResult(serialNumber: Long, testID: Long, resultsToAck: Long): ByteArray {
        val data = ByteArray(40)  // 16 for payload, plus 24 for data
        data.fourBytesFromWord(2, 0)//packet type 2
        data.fourBytesFromWord(1, 4) // Source is always 1 from this host.
        data.fourBytesFromWord(testID, 8)
        data.fourBytesFromWord(resultsToAck, 12)
        return data.protocolEncryptionEncrypt(customerToMeter, serialNumber, customerKey)
    }

    private fun ByteArray.twoWordsFromEightBytes(offset: Int): LongArray =
        if (offset < this.size - 7)
            longArrayOf(
                this.getUInt32(offset),
                this.getUInt32(offset + 4)
            )
        else
            longArrayOf(0L, 0L)

    private fun ByteArray.wordFromFourBytes(offset: Int) = this.getUInt32(offset)


    private fun ByteArray.wordFromTwoBytes(offset: Int) = this.getUInt16(offset)

    private fun ByteArray.eightBytesFromTwoWords(words: LongArray, offset: Int) {
        this[offset] = ((words[0] shr 0) and 0xFF).toByte()
        this[offset + 1] = ((words[0] shr 8) and 0xFF).toByte()
        this[offset + 2] = ((words[0] shr 16) and 0xFF).toByte()
        this[offset + 3] = ((words[0] shr 24) and 0xFF).toByte()
        this[offset + 4] = ((words[1] shr 0) and 0xFF).toByte()
        this[offset + 5] = ((words[1] shr 8) and 0xFF).toByte()
        this[offset + 6] = ((words[1] shr 16) and 0xFF).toByte()
        this[offset + 7] = ((words[1] shr 24) and 0xFF).toByte()
    }

    private fun ByteArray.fourBytesFromWord(word: Long, offset: Int) {
        this[offset] = ((word shr 0) and 0xFF).toByte()
        this[offset + 1] = ((word shr 8) and 0xFF).toByte()
        this[offset + 2] = ((word shr 16) and 0xFF).toByte()
        this[offset + 3] = ((word shr 24) and 0xFF).toByte()
    }

//    private fun ByteArray.twoBytesFromWord(word: Int, offset: Int) {
//        this[offset] = ((word shr 0) and 0xFF).toByte()
//        this[offset + 1] = ((word shr 8) and 0xFF).toByte()
//    }

    private fun ByteArray.protocolEncryptionDecrypt(
        length: Int,
        identifier: Long,
        serialNumber: Long,
        key: LongArray
    ): Pair<DecryptResult, ByteArray?> {
        //var temp8Bytes = ByteArray(8)

        if (length < 32) return DecryptResult.LENGTH_ERROR to null

        //Copy the first block to a dummy area


        //for (i = 8; i < 16; ++i)
        //{
        //    temp8bytes[i - 8] = data[i];
        //}

        var decryptTemp = this.twoWordsFromEightBytes(8)
        val encryptionChain = this.twoWordsFromEightBytes(0)

        //Decrypt that block in the dummy area.

        decryptTemp.decipher(64, key)

        for (i in 0 until 2) {
            decryptTemp[i] = (decryptTemp[i] xor encryptionChain[i]) and uint32Mask
        }

        //Now check to see if the first 4 bytes match the magic number.
        if (decryptTemp[0] != identifier) return DecryptResult.DECRYPTION_ERROR to null

        // First identifier matches.  Get length. 16 bit little endian
        val embeddedLength = (decryptTemp[1] and 0xFFFF).toInt()


        // - 24 to get rid of the beginning and end encryption wrappers)
        if (embeddedLength > 240 || (length - 24 > 240)) return DecryptResult.LENGTH_ERROR to null

        // Length looks OK.  Copy all of the data back 16 bytes and decrypt it.

        val tempTwo32s = this.twoWordsFromEightBytes(8)
        encryptionChain[0] = (tempTwo32s[0] xor decryptTemp[0]) and uint32Mask
        encryptionChain[1] = (tempTwo32s[1] xor decryptTemp[1]) and uint32Mask

        for (i in 16 until length) {
            this[i - 16] = this[i]
        }

        for (j in 0 until length - 16 step 8) {

            decryptTemp = this.twoWordsFromEightBytes(j)

            decryptTemp.decipher(64, key)

            for (i in 0 until 2) {
                decryptTemp[i] = (decryptTemp[i] xor encryptionChain[i]) and uint32Mask
            }

            val tempTwo32s = this.twoWordsFromEightBytes(j)
            encryptionChain[0] = (tempTwo32s[0] xor decryptTemp[0]) and uint32Mask
            encryptionChain[1] = (tempTwo32s[1] xor decryptTemp[1]) and uint32Mask
            this.eightBytesFromTwoWords(decryptTemp, j)
        }

        val match = this.getUInt32(this.size - 24)
        return (if (match != identifier) DecryptResult.FINAL_CHECK_ERROR else DecryptResult.SUCCESS) to this.copyOf(embeddedLength)
    }

    private fun LongArray.decipher(numRounds: Int, key: LongArray) {
        if (this.size >= 2 && key.size >= 4) {
            var v0 = this[0]
            var v1 = this[1]
            val delta = 0x9E3779B9
            var sum = (delta * numRounds) and uint32Mask
            for (i in 0 until numRounds) {
                v1 -= (((v0 shl 4) xor (v0 shr 5)) + v0) xor (sum + key[((sum shr 11) and 3).toInt()]) and uint32Mask
                v1 = v1 and uint32Mask
                sum -= delta and uint32Mask
                v0 -= (((v1 shl 4) xor (v1 shr 5)) + v1) xor (sum + key[(sum and 3).toInt()]) and uint32Mask
                v0 = v0 and uint32Mask

            }
            this[0] = v0
            this[1] = v1
        }
    }

    private fun ByteArray.protocolEncryptionEncrypt(
        identifier: Long,
        serialNumber: Long,
        key: LongArray
    ): ByteArray {
        val originalLength = this.size
        //var encryptionChain = longArrayOf(0L, 0L)

        var lengthOut = originalLength;
        // Length should be a multiple of 8 bytes.  If not, extend the packet to make it so by padding with zeros.
        while ((lengthOut and 0x07) != 0) ++lengthOut

        // move the data forward by 16 bytes to make room for the serial number and encryption wrapper front.
        var i = originalLength;

        lengthOut += 24; // Add space for protocol

        val data = ByteArray(lengthOut)

        while (i > 0) {
            --i
            data[i + 16] = this[i]
        }

        // Now fill in serial number and fill unused bytes with 0
        data[0] = 0xB1.toByte() // Magic start of packet marker
        data[1] = 0xD3.toByte()   // Magic start of packet marker
        data[2] = (lengthOut and 0xFF).toByte()  // lsb of length of packet  Following
        data[3] = (lengthOut shr 8).toByte()  // MSB of length of packet Following
        data[4] = (serialNumber and 0xFF).toByte()
        data[5] = ((serialNumber shr 8) and 0xFF).toByte()
        data[6] = ((serialNumber shr 16) and 0xFF).toByte()
        data[7] = ((serialNumber shr 24) and 0xFF).toByte()

        //Now fill in the encryption wrapper front
        // Move identifier into data (little endian)
        // Do a byte by byte copy in case data is not word aligned
        data[8] = (identifier and 0xFF).toByte()
        data[9] = ((identifier shr 8) and 0xFF).toByte()
        data[10] = ((identifier shr 16) and 0xFF).toByte()
        data[11] = ((identifier shr 24) and 0xFF).toByte()

        //Now fill in the packet length (little endian)
        data[12] = (originalLength and 0xFF).toByte()
        data[13] = ((originalLength shr 8) and 0xFF).toByte()

        // Fill in unused bytes with zero.
        data[14] = 0.toByte()
        data[15] = 0.toByte()

        //Now fill in the encryption wrapper end
        // Move identifier into data (little endian)
        // Do a byte by byte copy in case data is not word aligned
        data[0 + data.size - 8] = (identifier and 0xFF).toByte()
        data[1 + data.size - 8] = ((identifier shr 8) and 0xFF).toByte()
        data[2 + data.size - 8] = ((identifier shr 16) and 0xFF).toByte()
        data[3 + data.size - 8] = ((identifier shr 24) and 0xFF).toByte()

        // Fill in unused bytes with zero.
        data[4 + data.size - 8] = 0.toByte()
        data[5 + data.size - 8] = 0.toByte()
        data[6 + data.size - 8] = 0.toByte()
        data[7 + data.size - 8] = 0.toByte()

        // Initialize encryption chain with first 8 bytes.
        var encryptionChain = data.twoWordsFromEightBytes(0)

        // Encrypt the packet in place here.
        for (j in 8 until data.size step 8) {
            val originalWords =
                longArrayOf(data.wordFromFourBytes(j), data.wordFromFourBytes(j + 4))
            val dataWords =
                longArrayOf(data.wordFromFourBytes(j), data.wordFromFourBytes(j + 4))
            dataWords[0] = (dataWords[0] xor encryptionChain[0]) and uint32Mask
            dataWords[1] = (dataWords[1] xor encryptionChain[1]) and uint32Mask
            dataWords.encipher(64, key)

            encryptionChain[0] = (dataWords[0] xor originalWords[0]) and uint32Mask
            encryptionChain[1] = (dataWords[1] xor originalWords[1]) and uint32Mask

            data.eightBytesFromTwoWords(dataWords, j)
        }
        return data
    }

    private fun LongArray.encipher(numRounds: Int, key: LongArray) {
        if (this.size >= 2 && key.size >= 4) {
            var v0 = this[0]
            var v1 = this[1]
            var sum = 0L
            val delta = 0x9E3779B9L
            for (i in 0 until numRounds) {
                v0 += ((((v1 shl 4) xor (v1 shr 5)) + v1) xor (sum + key[(sum and 3).toInt()])) and uint32Mask
                v0 = v0 and uint32Mask
                sum += delta
                sum = sum and uint32Mask
                v1 += ((((v0 shl 4) xor (v0 shr 5)) + v0) xor (sum + key[((sum shr 11) and 3).toInt()])) and uint32Mask
                v1 = v1 and uint32Mask
            }
            this[0] = v0
            this[1] = v1
        }
    }

    fun processPacket(encryptedData: ByteArray): ByteArray? {
        var resultString = ""
        var testResultToSend: ByteArray? = null

        try {
            val (success, data) = encryptedData.protocolEncryptionDecrypt(
                encryptedData.size,
                meterToCustomer,
                0,
                customerKey
            )
            if (data == null) {
                resultString = "Decryption error: $success"
            } else {
                val result = DockResult()
                val packetType = data.getPacketType().toInt()

                when (packetType) {
                    -1 -> {
                        resultString = "Invalid packet type"
                    }
                    11 -> {
                        // Audible Ack
                        var toneQueueStatus = data.wordFromTwoBytes(8)
                        resultString = "Audible ACK: "
                        if (toneQueueStatus == 0) {
                            resultString += "BUSY";
                        } else {
                            resultString += "EMPTY";
                        }
                    }
                    15 -> {  // Serial Number response

                        var serialNum = data.getUInt32(4)
                        var deviceDescription = data.getString(8, 31)
                        var revisionNumber = data.getString(40, 15)
                        var radioVersion = data.getString(56, 15)
                        var macAddr = data.getString(72, 20)

                        // print the payload
                        resultString = "Serial #: $serialNum$newLine" +
                                "Device Description: $deviceDescription$newLine" +
                                "Revision #: $revisionNumber$newLine" +
                                "Radio Version: $radioVersion$newLine" +
                                "MAC: $macAddr"
                    }

                    9 -> {
                        val statusType = data.wordFromFourBytes(8)
                        val payLoadLength = data.wordFromTwoBytes(12)
                        var status: String
                        when (statusType) {
                            2L -> {   // RTC Status
                                if (payLoadLength == 7) {
                                    val year = (data[15].toInt() shl 8) or data[14].toInt()
                                    val month = data[16];
                                    val day = data[17];
                                    val hour = data[18];
                                    val minute = data[19];
                                    val second = data[20];
                                    resultString = "$year-$month$day $hour:$minute$second"
                                }
                            }
//                                0L ->
                            else -> {   // Voltage Status
                                resultString = data.getString(14, payLoadLength);
                                val serialNum = data.wordFromFourBytes(4)
                            }
                        }

                    }

                    14 -> {
                        val serialNum = data.wordFromFourBytes(4);
                        result.serialnum = data.getString(4, 16)
                        result.testID = data.getString(20, 6)
                        result.testtype = data.getString(26, 12)
                        result.result = data.getString(38, 8)
                        result.units = data.getString(46, 15)
                        result.QCCode = data.getString(61, 10)
                        result.testsRemaining = data.getString(71, 6)
                        result.lot = data.getString(77, 16)
                        val year = (data[94].toInt() shl 8) or data[93].toInt()
                        val month = data[95];
                        val day = data[96];
                        val hour = data[97];
                        val minute = data[98];
                        val second = data[99];
                        result.testTime = "$year-$month$day $hour:$minute$second"
                        resultString = result.toString()
                        // TODO  Fix serialNum
                        testResultToSend = ackTestResult(0, 1, 1)

                    }

                    17 -> {
                        val notificationNumber = data.wordFromFourBytes(8);
                        when (notificationNumber.toInt()) {
                            0 -> resultString = "Notification:  Device shutting down.";
                            1 -> resultString = "Notification:  Printer Timeout.";
                            2 -> resultString = "Notification:  Printer Jam.";
                            3 -> resultString = "Notification:  Printer Out of Paper.";
                            4 -> resultString = "Notification:  Printer Error.";
                            5 -> resultString = "Notification:  No Result Received";
                            6 -> resultString = "Notification:  Printout Complete";
                            'F'.toInt() -> resultString = "Notification:  5V High Diagnostic Fault";
                            'L'.toInt() -> resultString = "Notification:  5V Low Diagnostic Fault";
                            'R'.toInt() -> resultString = "Notification:  Radio Diagnostic Fault";
                            'Y'.toInt() -> resultString =
                                "Notification:  System Clock Diagnostic Fault";
                            'C'.toInt() -> resultString =
                                "Notification:  Real Time Clock Diagnostic Fault";
                            'D'.toInt() -> resultString = "Notification:  Date / Time not set";
                            'B'.toInt() -> resultString =
                                "Notification:  Battery Out of Operating Range";
                            'M'.toInt() -> resultString = "Notification:  NVM Diagnostic Fault";
                            'S'.toInt() -> resultString = "Notification:  NVM Settings Lost";
                            else -> resultString =
                                "Notification:  Unknown Notification $notificationNumber"
                        }

                    }
                    else -> {
                        resultString = "Unknown packet: ${data.toHex()}"
                    }

                }
            }
        } catch (e: Exception) {
            println("Exception $e")
        }
        println("Packet processed to $resultString")
        return testResultToSend
    }

    fun decrypt(data: ByteArray) {

        val magic = data.getUInt16(0)
        println("Magic: ${magic.toHex()}")
        if (magic != 0xD3B1) return

        val length = data.getUInt16(2)
        println("Length: $length")
        if (length == 0) return

        val serial = data.getUInt32(4)
        println("Serial: ${serial.toHex()}")

        val packetType = data.getUInt32(8)
        println("Packet type: ${packetType.toHex()}")

        val payloadLength = data.getUInt16(12)
        println("Payload length: ${payloadLength.toHex()}")

        val futureUse = data.getUInt16(14)
        println("Future use: ${futureUse.toHex()}")

        val payload = data.copyOfRange(16, data.size - 8)
        if (data.size - 8 > 16) println("Payload: ${payload.print()}")

        val packetTypeCheck = data.getUInt32(data.size - 8)
        println("Packet type check: ${packetTypeCheck.toHex()}")

        val reserved = data.getUInt32(data.size - 4)
        println("Reserved: ${reserved.toHex()}")

        //processPacket(payload)

    }
}
