package com.plusot.playground.lib;

/**
 * Package: com.plusot.playground.lib
 * Project: blu
 * <p>
 * Created by Peter Bruinink on 2019-11-26.
 * Copyright © 2019 Plusot. All rights reserved.
 */
public interface CryptoHandler {
    public void setKey(byte[] key);
    public void setBoxes(byte[][][] keybox);
    public byte[] encrypt(byte[] block);
    public byte[] decrypt(byte[] block);
    public int getBlockSize();
    public int getKeySize();
    public int[] getBoxSizes();/* { Number of Boxes, Rows, Columns } */
}