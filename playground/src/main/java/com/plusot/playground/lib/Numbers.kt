package com.plusot.playground.lib

/**
 * Package: com.plusot.playground.lib
 * Project: blu
 *
 * Created by Peter Bruinink on 2019-11-26.
 * Copyright © 2019 Plusot. All rights reserved.
 */

fun ByteArray.toHex(len: Int = Integer.MAX_VALUE, longFormat: Boolean = false): String {
    if (null == this) {
        return "no deviceData"
    }

    val hexString = StringBuilder()
    for (i in 0 until Math.min(this.size, len)) {
        if (longFormat) hexString.append("[")
        hexString.append(String.format("%02X", this[i].toInt() and 0xFF))
        if (longFormat)
            hexString.append("]")
        else
            hexString.append(" ")
    }
    return hexString.toString().trim { it <= ' ' }
}

fun ByteArray.getUInt8(i: Int): Int {
    return if (i >= this.size)
        0
    else
        (0xff and this[i].toInt())
}

//Little endian
fun ByteArray.getUInt16(i: Int): Int {
    return if (i >= this.size - 1)
        0
    else
        (       (0xff and this[i].toInt()) +
                (0xff and this[i + 1].toInt() shl 8)
                )
}

fun ByteArray.getInt16(i: Int): Int = this.getUInt16(i).toSigned(16)


fun ByteArray.getUInt32(i: Int): Long {
    return if (i >= this.size - 3)
        0
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24)
                ).toLong()
}

fun ByteArray.getUInt40(i: Int): Long {
    return if (i >= this.size - 4)
        0
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24) +
                (0xffL and this[i + 4].toLong() shl 32)
                ).toLong()
}

fun ByteArray.getUInt40BE(i: Int): Long { // BE = Big Endian
    return if (i >= this.size - 4)
        0
    else
        (       (0xffL and this[i + 4].toLong()) +
                (0xffL and this[i + 3].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 1].toLong() shl 24) +
                (0xffL and this[i].toLong() shl 32)
                ).toLong()
}

fun ByteArray.getNibble(i: Int, rShift: Int = 0): Int {
    return if (i >= this.size)
        0
    else
        (0x0f and (this[i].toInt() shr rShift))
}

fun ByteArray.isZero(): Boolean {
    this.forEach { if (it.toInt() != 0) return false }
    return true
}

fun ByteArray.equalsOther(other: ByteArray?) : Boolean {
    if (other == null) return false
    if (this.size != other.size) return false
    this.forEachIndexed { index, byte -> if (other[index] != byte) return false }
    return true
}

fun ByteArray.getUInt64(i: Int): Long {
    return if (i >= this.size - 7)
        0L
    else
        (       (0xffL and this[i].toLong()) +
                (0xffL and this[i + 1].toLong() shl 8) +
                (0xffL and this[i + 2].toLong() shl 16) +
                (0xffL and this[i + 3].toLong() shl 24) +
                (0xffL and this[i + 4].toLong() shl 32) +
                (0xffL and this[i + 5].toLong() shl 40) +
                (0xffL and this[i + 6].toLong() shl 48) +
                (0xffL and this[i + 7].toLong() shl 56)
                )
}

fun ByteArray.print() = this.fold(""){ acc, value -> if (acc.isEmpty()) value.toHex() else "$acc ${value.toHex()}"}

fun byteArrayOfInts(vararg elements: Int): ByteArray = elements.map { it.toByte() }.toByteArray()


inline fun ByteArray.copyFrom(fromIndex: Int) = if (fromIndex < this.size) this.copyOfRange(fromIndex, this.size) else null
