package com.plusot.playground


import com.plusot.playground.lib.*

/**
 * Package: com.plusot.playground
 * Project: kaleido
 *
 * Created by Peter Bruinink on 28-05-19.
 * Copyright © 2019 Plusot. All rights reserved.
 */

fun test() {
    val days = daysUpTo()
    println("Days = $days")
    val timeZero = toSecondsSinceYear0()
    fromSecondsSinceYear0(timeZero)

    val text = "Peter is gek"
    val bytes = text.toByteArray(Charsets.UTF_8)
    println("Text part = ${bytes.getString(1, 6)}")

    val intValue = 0xFFFB
    println("Unsigned: $intValue to signed: ${intValue.toSigned(16)} ${-0x10000 + intValue}")
    val mask = longMask(5, 3)
    println("Bin ${mask.toBin()}")

    val byteArray = byteArrayOf(0xFF.toByte(), 0xFE.toByte())
    println("Get uint8 = ${byteArray[0]} ${byteArray.getUInt8(0)} ${byteArray.getUInt8(1)}")

    val byte = 0x80.toByte()
    println("Byte ${byte.toUnsigned()} ${0x8000 shr 15}")

    val test: ByteArray = byteArrayOf(0x32, 0x0, 0x9)
    test.forEach {
        for (i in 0..7) {
            println("Check ${it.toHex()} on bit $i = ${it.checkBitOnIndex(i)}")
        }
    }

    val bites = byteArrayOf(1,2,3,4,5,6,7,8,9,10,11,12,
        24,23,22,21,20,19,19,17,16,15,14,13)
    val delimited = bites.toDelimitedString()
    println("Bytes $delimited - ${delimited.toByteArray().toIntString()}")

    val zeroBytes = byteArrayOf('\u0000'.toByte())
    println("Zero bytes = ${zeroBytes.toIntString()}")

    val pumpId = 0x8000
    println("PumpId = $pumpId, ${pumpId shr 15}")

    println("Int = ${34.toZeroLeading(4)}")
}

fun testPts() {
    val bytes = byteArrayOfInts(
        0xB1, 0xD3, 0x24, 0x00, 0x01, 0x00,
        0x00, 0x00, 0x8A, 0xE0, 0x79, 0xD9,
        0x1D, 0x4B, 0x2A, 0x94, 0x65, 0xB1,
        0xF8, 0x28, 0xF3, 0xC3, 0xF4, 0x21,
        0xD6, 0x85, 0x5C, 0xC1, 0xD6, 0xAA,
        0x2F, 0x09, 0x27, 0x08, 0xFB, 0x27,
        0x1B, 0x81, 0x82, 0xFA
    )
    println("Bytes: ${bytes.print()}")
    PtsDecrypt.decrypt(bytes)
    PtsDecrypt.processPacket(bytes)
    val ack = PtsDecrypt.ackTestResult(0, 1, 1); // TODO  Fix serialNum
    println("Bytes: ${ack.toHex()}")

    println("CRC16 = ${bytes.crcCcitt16(0xDACD).toHex()} = ${bytes.crcCcittTable(0xDACD).toHex()}")

//    println("Math.ceil ${Math.ceil(2.4)}")


}

fun Int.toHex(digits: Int): String {
    val formatStr = if (digits <= 0) "0x%X" else "0x%0${digits}X"
    return String.format(formatStr, this)
}


fun fromBytes(bytes: ByteArray, st: Int, len: Int): Int {
    var result = 0
    if (st + len - 1 >= bytes.size) return 0
    for (i in len - 1 downTo 0) {
        result = result shl 8
        result += bytes[i + st].toInt() and 0xFF
    }
    return result
}

fun main(args: Array<String>) {
    println("Start of program")
    println()

//    testPts()
//    println("From bytes ${fromBytes(byteArrayOf(0xFE.toByte(), 0xAE.toByte(), 0x9A.toByte()),0,2)}")
//    test()
//    println("To hex: ${0x1ABCD.toHex(0)}")

    val a: String? = null
    a?.also {
       println("a is not null.")
    } ?: run {
        println("a is null, but run runs!")
    }

    println()
    println("End of program")
}

